﻿CREATE PROCEDURE [dbo].[Procedure_tour_names_by_country_and_departure_city]
	@lang NVARCHAR(10),
	@countryName NVARCHAR(80),
	@departureCityName NVARCHAR(80)
AS
	IF (@lang = 'en')
	BEGIN
		SELECT DISTINCT Name FROM View_all_tours_names WHERE HotelId IN ( SELECT Id
																 FROM Hotel 
																 WHERE ResortId IN ( SELECT Id
																					 FROM Resort
																					 WHERE CountryId = ( SELECT Id
																										 FROM Country
																										 WHERE Name = @countryName ) ) )
													AND DepartureCityId = ( SELECT Id FROM DepartureCity WHERE Name = @departureCityName )
	END
	ELSE
	BEGIN
		SELECT DISTINCT NameRUS FROM View_all_tours_names WHERE HotelId IN ( SELECT Id
																    FROM Hotel 
																	WHERE ResortId IN ( SELECT Id
																						FROM Resort
																						WHERE CountryId = ( SELECT Id
																											FROM Country
																											WHERE NameRUS = @countryName ) ) )
													   AND DepartureCityId = ( SELECT Id FROM DepartureCity WHERE NameRUS = @departureCityName )
	END
