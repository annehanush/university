﻿CREATE PROCEDURE [dbo].[Procedure_tour_suitable_category_diet]
	@lang NVARCHAR(10),
	@dateFrom DATE,
	@dateTo DATE,
	@countryName NVARCHAR(80),
	@category INT,
	@departureCityName NVARCHAR(80),
	@nightsFrom INT,
	@nightsTo INT,
	@people INT,
	@dietName NVARCHAR(80)
AS
	IF (@lang = 'en')
	BEGIN
		SELECT DepartureDate, TourName, NumberOfNights, HotelName, HotelRating, DietName, NumberOfPersons, Cost, TransportName, TransportClassName, TourId FROM View__suitable_tours
		WHERE ( DepartureDate BETWEEN @dateFrom AND @dateTo ) AND 
				HotelId IN ( SELECT Id FROM Hotel WHERE Rating = @category AND ResortId IN ( SELECT Id FROM Resort WHERE CountryId IN ( SELECT Id FROM Country WHERE Name = @countryName ) ) ) AND
				DepartureCityId = ( SELECT Id FROM DepartureCity WHERE Name = @departureCityName ) AND
				( NumberOfNights BETWEEN @nightsFrom AND @nightsTo ) AND
				NumberOfPersons = @people AND DietName = @dietName
	END
	ELSE
	BEGIN
		SELECT DepartureDate, TourName, NumberOfNights, HotelName, HotelRating, DietName, NumberOfPersons, Cost, TransportName, TransportClassName, TourId FROM View_suitable_tours_RUS
		WHERE ( DepartureDate BETWEEN @dateFrom AND @dateTo ) AND 
				HotelId IN ( SELECT Id FROM Hotel WHERE Rating = @category AND ResortId IN ( SELECT Id FROM Resort WHERE CountryId IN ( SELECT Id FROM Country WHERE NameRUS = @countryName ) ) ) AND
				DepartureCityId = ( SELECT Id FROM DepartureCity WHERE NameRUS = @departureCityName ) AND
				( NumberOfNights BETWEEN @nightsFrom AND @nightsTo ) AND
				NumberOfPersons = @people AND DietName = @dietName
	END

