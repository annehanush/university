﻿CREATE PROCEDURE [dbo].[Procedure_top_people_amount]
	@lang NVARCHAR(10),
	@countryName NVARCHAR(80),
	@departureCityName NVARCHAR(80)
AS
IF (@lang = 'en')
	BEGIN
		SELECT TOP 1 NumberOfPersons FROM Tour WHERE HotelId IN ( SELECT Id
																 FROM Hotel 
																 WHERE ResortId IN ( SELECT Id
																					 FROM Resort
																					 WHERE CountryId = ( SELECT Id
																										 FROM Country
																										 WHERE Name = @countryName ) ) )
													AND DepartureCityId = ( SELECT Id FROM DepartureCity WHERE Name = @departureCityName )
	END
	ELSE
	BEGIN
		SELECT TOP 1 NumberOfPersons FROM Tour WHERE HotelId IN ( SELECT Id
																    FROM Hotel 
																	WHERE ResortId IN ( SELECT Id
																						FROM Resort
																						WHERE CountryId = ( SELECT Id
																											FROM Country
																											WHERE NameRUS = @countryName ) ) )
													   AND DepartureCityId = ( SELECT Id FROM DepartureCity WHERE NameRUS = @departureCityName )
	END
