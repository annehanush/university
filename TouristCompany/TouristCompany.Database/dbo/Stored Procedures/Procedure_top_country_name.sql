﻿CREATE PROCEDURE [dbo].[Procedure_top_country_name]
	@lang NVARCHAR(10)
AS
IF (@lang = 'en')
	BEGIN
		SELECT TOP 1 Name FROM Country
	END
	ELSE
	BEGIN
		SELECT TOP 1 NameRUS FROM Country
	END
