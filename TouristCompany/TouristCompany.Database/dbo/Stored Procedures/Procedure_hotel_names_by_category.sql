﻿CREATE PROCEDURE [dbo].[Procedure_hotel_names_by_category]
	@lang NVARCHAR(10),
	@rating INT,
	@countryName NVARCHAR(80)
AS
IF (@lang = 'en')
	BEGIN
		SELECT Name, Rating FROM View_all_hotels_names WHERE Rating = @rating AND ResortId IN ( SELECT Id 
																							FROM Resort 
																							WHERE CountryId = ( SELECT Id FROM Country WHERE Name = @countryName ) )
	END
	ELSE
	BEGIN
		SELECT Name, Rating FROM View_all_hotels_names WHERE Rating = @rating AND ResortId IN ( SELECT Id 
																							FROM Resort 
																							WHERE CountryId = ( SELECT Id FROM Country WHERE NameRUS = @countryName ) )
	END
	
