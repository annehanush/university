﻿CREATE PROCEDURE [dbo].[Procedure_insert_services_for_request]
	@lang NVARCHAR(10),
	@serviceName NVARCHAR(80)
AS
	DECLARE @requestId INT, @serviceId INT

	SELECT @requestId = MAX(Id) FROM Request
	
	IF (@lang = 'en')
	BEGIN
		SELECT @serviceId = Id FROM Service WHERE Name = @serviceName
	END
	ELSE
	BEGIN
		SELECT @serviceId = Id FROM Service WHERE NameRUS = @serviceName
	END

	INSERT INTO RequestWithService (RequestId, ServiceId)
	VALUES (@requestId, @serviceId)