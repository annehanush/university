﻿CREATE PROCEDURE [dbo].[Procedure_insert_request]
	@date DATE,
	@tourId INT,
	@clientId INT 
AS
	INSERT INTO Request ( DateOfCreation, StatusId, TourId, ClientId, EmployeeId )
	VALUES ( @date, 1, @tourId, @clientId, NULL )