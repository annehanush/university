﻿CREATE PROCEDURE [dbo].[Procedure_update_user_phone_number]
	@userId INT,
	@phone NVARCHAR(80)
AS
	UPDATE [dbo].[User] SET PhoneNumber = @phone
	WHERE [dbo].[User].[Id] = @userId
