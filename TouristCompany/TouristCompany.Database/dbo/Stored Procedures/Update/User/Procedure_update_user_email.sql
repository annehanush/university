﻿CREATE PROCEDURE [dbo].[Procedure_update_user_email]
	@userId INT,
	@email NVARCHAR(256)
AS
	UPDATE [dbo].[User] SET Email = @email
	WHERE [dbo].[User].[Id] = @userId

