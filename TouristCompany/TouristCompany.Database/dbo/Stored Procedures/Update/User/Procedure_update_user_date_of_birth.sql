﻿CREATE PROCEDURE [dbo].[Procedure_update_user_date_of_birth]
	@userId INT,
	@date DATE
AS
	UPDATE [dbo].[User] SET DateOfBirth = @date
	WHERE [dbo].[User].[Id] = @userId

