﻿CREATE PROCEDURE [dbo].[Procedure_update_user_password]
	@userId INT,
	@password NVARCHAR(50)
AS
	UPDATE [dbo].[User] SET [dbo].[User].[Password] = @password
	WHERE [dbo].[User].[Id] = @userId
