﻿CREATE PROCEDURE [dbo].[Procedure_update_user_full_name]
	@userId INT,
	@newFullName NVARCHAR(80)
AS
	UPDATE [dbo].[User] SET FullName = @newFullName
	WHERE [dbo].[User].[Id] = @userId

