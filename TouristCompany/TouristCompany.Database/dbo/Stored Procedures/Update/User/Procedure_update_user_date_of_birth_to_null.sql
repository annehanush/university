﻿CREATE PROCEDURE [dbo].[Procedure_update_user_date_of_birth_to_null]
	@userId INT
AS
	UPDATE [dbo].[User] SET DateOfBirth = NULL
	WHERE [dbo].[User].[Id] = @userId

