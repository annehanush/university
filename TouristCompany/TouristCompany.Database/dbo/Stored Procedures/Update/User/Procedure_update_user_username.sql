﻿CREATE PROCEDURE [dbo].[Procedure_update_user_username]
	@userId INT,
	@newUserName NVARCHAR(80)
AS
	UPDATE [dbo].[User] SET UserName = @newUserName
	WHERE [dbo].[User].[Id] = @userId
