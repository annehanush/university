﻿CREATE PROCEDURE [dbo].[Procedure_resort_names_by_country]
	@countryName NVARCHAR(80),
	@lang NVARCHAR(10)
AS
	IF (@lang = 'en')
	BEGIN
		SELECT Name FROM View_all_reosrts_names WHERE CountryId IN ( SELECT Id FROM Country WHERE Name = @countryName )
	END
	ELSE
	BEGIN
		SELECT NameRUS FROM View_all_reosrts_names WHERE CountryId IN ( SELECT Id FROM Country WHERE NameRUS = @countryName )
	END
