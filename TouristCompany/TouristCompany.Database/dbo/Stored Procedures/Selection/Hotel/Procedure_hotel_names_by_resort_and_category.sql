﻿CREATE PROCEDURE [dbo].[Procedure_hotel_names_by_resort_and_category]
	@lang NVARCHAR(10),
	@rating INT,
	@resortName NVARCHAR(80)
AS
IF (@lang = 'en')
	BEGIN
		SELECT Name, Rating FROM View_all_hotels_names WHERE Rating = @rating AND ResortId IN ( SELECT Id 
																								FROM Resort 
																								WHERE Name = @resortName )
	END
	ELSE
	BEGIN
		SELECT Name, Rating FROM View_all_hotels_names WHERE Rating = @rating AND ResortId IN ( SELECT Id 
																								FROM Resort 
																								WHERE NameRUS = @resortName )
	END
