﻿CREATE PROCEDURE [dbo].[Procedure_all_countries_names]
	@lang NVARCHAR(10)
AS
	IF (@lang = 'en')
	BEGIN
		SELECT View_all_countries_names.Name FROM View_all_countries_names
	END
	ELSE
	BEGIN
		SELECT View_all_countries_names.NameRUS FROM View_all_countries_names
	END