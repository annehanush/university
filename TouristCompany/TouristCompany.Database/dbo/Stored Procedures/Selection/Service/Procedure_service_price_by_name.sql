﻿CREATE PROCEDURE [dbo].[Procedure_service_price_by_name]
	@lang NVARCHAR(10),
	@serviceName NVARCHAR(80)
AS
	IF (@lang = 'en')
	BEGIN
		SELECT Cost FROM Service WHERE Name = @serviceName
	END
	ELSE
	BEGIN
		SELECT Cost FROM Service WHERE NameRUS = @serviceName
	END
