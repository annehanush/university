﻿CREATE PROCEDURE [dbo].[Procedure_service_names_all]
	@lang NVARCHAR(10)
AS
	IF (@lang = 'en')
	BEGIN
		SELECT Name FROM View_all_services_names
	END
	ELSE
	BEGIN
		SELECT NameRUS FROM View_all_services_names
	END
