﻿CREATE PROCEDURE [dbo].[Procedure_tour_suitable_hotel]
	@lang NVARCHAR(10),
	@dateFrom DATE,
	@dateTo DATE,
	@hotelName NVARCHAR(80),
	@departureCityName NVARCHAR(80),
	@nightsFrom INT,
	@nightsTo INT,
	@people INT
AS
	IF (@lang = 'en')
	BEGIN
		SELECT DepartureDate, TourName, NumberOfNights, HotelName, HotelRating, DietName, NumberOfPersons, Cost, TransportName, TransportClassName, TourId FROM View__suitable_tours
		WHERE ( DepartureDate BETWEEN @dateFrom AND @dateTo ) AND 
				HotelId IN ( SELECT Id FROM Hotel WHERE Name = @hotelName ) AND
				DepartureCityId = ( SELECT Id FROM DepartureCity WHERE Name = @departureCityName ) AND
				( NumberOfNights BETWEEN @nightsFrom AND @nightsTo ) AND
				NumberOfPersons = @people
	END
	ELSE
	BEGIN
		SELECT DepartureDate, TourName, NumberOfNights, HotelName, HotelRating, DietName, NumberOfPersons, Cost, TransportName, TransportClassName, TourId FROM View_suitable_tours_RUS
		WHERE ( DepartureDate BETWEEN @dateFrom AND @dateTo ) AND 
				HotelId IN ( SELECT Id FROM Hotel WHERE Name = @hotelName ) AND
				DepartureCityId = ( SELECT Id FROM DepartureCity WHERE NameRUS = @departureCityName ) AND
				( NumberOfNights BETWEEN @nightsFrom AND @nightsTo ) AND
				NumberOfPersons = @people
	END
