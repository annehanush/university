﻿CREATE PROCEDURE [dbo].[Procedure_tour_for_request]
	@lang NVARCHAR(10),
	@tourId INT
AS
	IF (@lang = 'en')
	BEGIN
		SELECT * FROM View_tour_for_request WHERE TourId = @tourId
	END
	ELSE
	BEGIN
		SELECT * FROM View_tour_for_request_RUS WHERE TourId = @tourId
	END
