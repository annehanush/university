﻿CREATE PROCEDURE [dbo].[Procedure_user_role_name_by_user_id]
	@userId INT
AS
	SELECT Name 
	FROM [dbo].[Role], [dbo].[User] 
	WHERE [dbo].[User].[Id] = @userId AND [dbo].[User].[RoleId] = [dbo].[Role].[Id]
