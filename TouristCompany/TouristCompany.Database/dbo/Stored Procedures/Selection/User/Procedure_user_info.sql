﻿CREATE PROCEDURE [dbo].[Procedure_user_info]
	@userId INT
AS
	SELECT View_user_info.UserName, View_user_info.FullName, View_user_info.DateOfBirth, View_user_info.Email,
	View_user_info.PhoneNumber 
	FROM View_user_info 
	WHERE View_user_info.Id = @userId
