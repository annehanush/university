﻿CREATE PROCEDURE [dbo].[Procedure_top_departure_city_name]
	@lang NVARCHAR(10)
AS
IF (@lang = 'en')
	BEGIN
		SELECT TOP 1 Name FROM DepartureCity
	END
	ELSE
	BEGIN
		SELECT TOP 1 NameRUS FROM DepartureCity
	END
