﻿CREATE PROCEDURE [dbo].[Procedure_departure_city_names]
	@lang NVARCHAR(10)
AS
	IF (@lang = 'en')
	BEGIN
		SELECT Name FROM DepartureCity
	END
	ELSE
	BEGIN
		SELECT NameRUS FROM DepartureCity
	END
