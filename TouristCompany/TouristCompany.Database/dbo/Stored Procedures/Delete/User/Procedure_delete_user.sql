﻿CREATE PROCEDURE [dbo].[Procedure_delete_user]
	@userId INT
AS
	DELETE FROM [dbo].[User] WHERE [dbo].[User].[Id] = @userId
