﻿CREATE PROCEDURE [dbo].[Procedure_hotel_names_by_resort]
	@lang NVARCHAR(10),
	@resortName NVARCHAR(80)
AS
	IF (@lang = 'en')
	BEGIN
		SELECT Name, Rating FROM View_all_hotels_names WHERE ResortId IN ( SELECT Id 
																		   FROM Resort 
																		   WHERE Name = @resortName )
	END
	ELSE
	BEGIN
		SELECT Name, Rating FROM View_all_hotels_names WHERE ResortId IN ( SELECT Id 
																		   FROM Resort 
																		   WHERE NameRUS = @resortName )
	END
