﻿CREATE TABLE [dbo].[Transport]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(80) NOT NULL, 
    [ClassId] INT NOT NULL, 
    [NameRUS] NVARCHAR(80) NOT NULL, 
    CONSTRAINT [FK_Transport_TransportClass] FOREIGN KEY ([ClassId]) REFERENCES [dbo].[TransportClass]([Id])
)
