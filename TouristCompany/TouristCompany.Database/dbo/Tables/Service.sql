﻿CREATE TABLE [dbo].[Service]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(80) NOT NULL, 
    [ProviderId] INT NOT NULL, 
    [NameRUS] NVARCHAR(80) NOT NULL, 
    [Cost] MONEY NOT NULL, 
    CONSTRAINT [FK_Service_Provider] FOREIGN KEY ([ProviderId]) REFERENCES [dbo].[Provider]([Id])
)
