﻿CREATE TABLE [dbo].[Client]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Address] NVARCHAR(180) NULL, 
    [PassportNumber] NVARCHAR(50) NOT NULL, 
    [UserId] INT NOT NULL, 
    CONSTRAINT [FK_Client_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User]([Id])
)
