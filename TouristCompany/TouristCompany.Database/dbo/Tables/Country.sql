﻿CREATE TABLE [dbo].[Country]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(80) NOT NULL, 
    [VisaCost] MONEY NULL, 
    [Currency] NVARCHAR(80) NULL, 
    [Language] NVARCHAR(80) NOT NULL, 
    [NameRUS] NVARCHAR(80) NOT NULL
)
