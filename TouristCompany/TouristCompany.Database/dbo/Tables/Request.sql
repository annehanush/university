﻿CREATE TABLE [dbo].[Request]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [DateOfCreation] DATE NOT NULL, 
    [StatusId] INT NOT NULL, 
    [TourId] INT NOT NULL, 
    [ClientId] INT NOT NULL, 
    [EmployeeId] INT NULL, 
    CONSTRAINT [FK_Request_Tour] FOREIGN KEY ([TourId]) REFERENCES [dbo].[Tour]([Id]), 
    CONSTRAINT [FK_Request_UserClient] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_Request_UserEmployee] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[User] ([Id]), 
    CONSTRAINT [FK_Request_Status] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status]([Id])
)
