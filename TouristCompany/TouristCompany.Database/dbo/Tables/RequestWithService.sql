﻿CREATE TABLE [dbo].[RequestWithService]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [RequestId] INT NOT NULL, 
    [ServiceId] INT NOT NULL, 
    CONSTRAINT [FK_RequestWithService_Request] FOREIGN KEY ([RequestId]) REFERENCES [dbo].[Request]([Id]), 
    CONSTRAINT [FK_RequestWithService_Service] FOREIGN KEY ([ServiceId]) REFERENCES [dbo].[Service]([Id])
)
