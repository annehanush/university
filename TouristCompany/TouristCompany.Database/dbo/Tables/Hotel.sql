﻿CREATE TABLE [dbo].[Hotel]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(80) NOT NULL, 
    [Rating] INT NOT NULL, 
    [Description] NVARCHAR(MAX) NULL, 
    [ResortId] INT NOT NULL, 
    [TypeOfDietId] INT NOT NULL, 
    CONSTRAINT [FK_Hotel_Resort] FOREIGN KEY ([ResortId]) REFERENCES [dbo].[Resort]([Id]), 
    CONSTRAINT [FK_Hotel_TypeOfDiet] FOREIGN KEY ([TypeOfDietId]) REFERENCES [dbo].[TypeOfDiet]([Id])
)
