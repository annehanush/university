﻿CREATE TABLE [dbo].[Tour]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(80) NOT NULL, 
    [Cost] MONEY NOT NULL, 
    [DepartureDate] DATE NOT NULL, 
    [DateOfAppearance] DATE NULL, 
    [DateOfDisappearance] DATE NULL, 
    [DepartureCityId] INT NOT NULL, 
    [HotelId] INT NOT NULL, 
    [TransportId] INT NOT NULL, 
    [NumberOfPersons] INT NOT NULL, 
    [NumberOfNights] INT NOT NULL, 
    [NameRUS] NVARCHAR(80) NOT NULL, 
    CONSTRAINT [FK_Tour_DepartureCity] FOREIGN KEY ([DepartureCityId]) REFERENCES [dbo].[DepartureCity]([Id]), 
    CONSTRAINT [FK_Tour_Hotel] FOREIGN KEY ([HotelId]) REFERENCES [dbo].[Hotel]([Id]), 
    CONSTRAINT [FK_Tour_Transport] FOREIGN KEY ([TransportId]) REFERENCES [dbo].[Transport]([Id])
)
