﻿CREATE TABLE [dbo].[Resort]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(80) NOT NULL, 
    [Airport] NVARCHAR(80) NOT NULL, 
    [Description] NVARCHAR(MAX) NULL, 
    [CountryId] INT NOT NULL, 
    [NameRUS] NVARCHAR(80) NOT NULL, 
    [AirportRUS] NVARCHAR(80) NOT NULL, 
    [DescriprionRUS] NVARCHAR(MAX) NULL, 
    CONSTRAINT [FK_Resort_Country] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country]([Id])
)
