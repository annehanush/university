﻿CREATE TABLE [dbo].[User]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [UserName] NVARCHAR(80) NOT NULL, 
    [FullName] NVARCHAR(180) NULL, 
    [DateOfBirth] DATE NULL, 
    [Email] NVARCHAR(256) NOT NULL, 
    [PhoneNumber] NVARCHAR(80) NULL, 
    [Password] NVARCHAR(50) NOT NULL, 
    [RoleId] INT NOT NULL, 
    CONSTRAINT [FK_User_Role] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Role]([Id])
)
