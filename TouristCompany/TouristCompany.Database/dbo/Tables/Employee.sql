﻿CREATE TABLE [dbo].[Employee]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Post] NVARCHAR(80) NOT NULL, 
    [UserId] INT NOT NULL, 
    CONSTRAINT [FK_Employee_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User]([Id])
)
