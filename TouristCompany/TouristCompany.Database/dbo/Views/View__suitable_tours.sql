﻿CREATE VIEW [dbo].[View__suitable_tours]
AS
	SELECT DepartureDate, Tour.Name AS TourName, NumberOfNights, Hotel.Name AS HotelName, Hotel.Rating AS HotelRating, TypeOfDiet.Name AS DietName, NumberOfPersons, Cost, Transport.Name AS TransportName, TransportClass.Name AS TransportClassName, Tour.Id AS TourId, DepartureCityId, HotelId  
	FROM Tour, Hotel, TypeOfDiet, Transport, TransportClass
	WHERE Tour.HotelId = Hotel.Id AND Hotel.TypeOfDietId = TypeOfDiet.Id AND Tour.TransportId = Transport.Id AND Transport.ClassId = TransportClass.Id
