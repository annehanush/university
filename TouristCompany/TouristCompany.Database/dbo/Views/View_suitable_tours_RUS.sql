﻿CREATE VIEW [dbo].[View_suitable_tours_RUS]
AS
	SELECT DepartureDate, Tour.NameRUS AS TourName, NumberOfNights, Hotel.Name AS HotelName, Hotel.Rating AS HotelRating, TypeOfDiet.Name AS DietName, NumberOfPersons, Cost, Transport.NameRUS AS TransportName, TransportClass.NameRUS AS TransportClassName, Tour.Id AS TourId, DepartureCityId, HotelId  
	FROM Tour, Hotel, TypeOfDiet, Transport, TransportClass
	WHERE Tour.HotelId = Hotel.Id AND Hotel.TypeOfDietId = TypeOfDiet.Id AND Tour.TransportId = Transport.Id AND Transport.ClassId = TransportClass.Id
