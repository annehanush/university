﻿CREATE VIEW [dbo].[View_tour_for_request_RUS]
AS 
	SELECT Tour.NameRUS AS Tour, Tour.DepartureDate AS Departure, Resort.NameRUS AS Resort, Country.NameRUS AS Country, 
	Hotel.Name AS Hotel, Hotel.Rating AS Rating, Transport.NameRUS AS TransportName, TransportClass.NameRUS AS TransportClass, 
	Tour.NumberOfPersons, Tour.NumberOfNights, Tour.Cost, Tour.Id AS TourId
	FROM Tour, Hotel, Resort, Country, Transport, TransportClass
	WHERE HotelId = Hotel.Id AND ResortId = Resort.Id AND CountryId = Country.Id AND TransportId = Transport.Id AND
		  ClassId = TransportClass.Id
