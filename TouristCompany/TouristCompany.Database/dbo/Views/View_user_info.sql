﻿CREATE VIEW [dbo].[View_user_info]
AS 
	SELECT [dbo].[User].[UserName], [dbo].[User].[FullName], [dbo].[User].[DateOfBirth], 
	[dbo].[User].[Email], [dbo].[User].[PhoneNumber], [dbo].[User].[Id] FROM [dbo].[User]
