﻿CREATE VIEW [dbo].[View_tour_for_request]
AS 
	SELECT Tour.Name AS Tour, Tour.DepartureDate AS Departure, Resort.Name AS Resort, Country.Name AS Country, 
	Hotel.Name AS Hotel, Hotel.Rating AS Rating, Transport.Name AS TransportName, TransportClass.Name AS TransportClass, 
	Tour.NumberOfPersons, Tour.NumberOfNights, Tour.Cost, Tour.Id AS TourId
	FROM Tour, Hotel, Resort, Country, Transport, TransportClass
	WHERE HotelId = Hotel.Id AND ResortId = Resort.Id AND CountryId = Country.Id AND TransportId = Transport.Id AND
		  ClassId = TransportClass.Id
