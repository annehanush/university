namespace TouristCompany.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Tour")]
    public partial class Tour
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Tour()
        {
            Requests = new HashSet<Request>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(80)]
        public string Name { get; set; }

        [Required]
        [StringLength(10)]
        public string Cost { get; set; }

        [Column(TypeName = "date")]
        public DateTime DepartureDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateOfAppearance { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateOfDisappearance { get; set; }

        public int DepartureCityId { get; set; }

        public int HotelId { get; set; }

        public int TransportId { get; set; }

        public int NumberOfPersons { get; set; }

        public int NumberOfNights { get; set; }

        public virtual DepartureCity DepartureCity { get; set; }

        public virtual Hotel Hotel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Request> Requests { get; set; }

        public virtual Transport Transport { get; set; }
    }
}
