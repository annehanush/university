namespace TouristCompany.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Request")]
    public partial class Request
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Request()
        {
            RequestWithServices = new HashSet<RequestWithService>();
        }

        public int Id { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateOfCreation { get; set; }

        public bool Insurance { get; set; }

        public int Status { get; set; }

        public int TourId { get; set; }

        public int ClientId { get; set; }

        public int EmployeeId { get; set; }

        public virtual Client Client { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Tour Tour { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RequestWithService> RequestWithServices { get; set; }
    }
}
