namespace TouristCompany.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RequestWithService")]
    public partial class RequestWithService
    {
        public int Id { get; set; }

        public int RequestId { get; set; }

        public int ServiceId { get; set; }

        [Column(TypeName = "money")]
        public decimal Cost { get; set; }

        public virtual Request Request { get; set; }

        public virtual Service Service { get; set; }
    }
}
