namespace TouristCompany.Data.Entities
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class TouristCompanyDbContext : DbContext
    {
        public TouristCompanyDbContext()
            : base("name=TouristCompanyDbContext")
        {
        }

        public virtual DbSet<C__RefactorLog> C__RefactorLog { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<DepartureCity> DepartureCities { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Hotel> Hotels { get; set; }
        public virtual DbSet<Provider> Providers { get; set; }
        public virtual DbSet<Request> Requests { get; set; }
        public virtual DbSet<RequestWithService> RequestWithServices { get; set; }
        public virtual DbSet<Resort> Resorts { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<Tour> Tours { get; set; }
        public virtual DbSet<Transport> Transports { get; set; }
        public virtual DbSet<TransportClass> TransportClasses { get; set; }
        public virtual DbSet<TypeOfDiet> TypeOfDiets { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>()
                .HasMany(e => e.Requests)
                .WithRequired(e => e.Client)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.VisaCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.Resorts)
                .WithRequired(e => e.Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DepartureCity>()
                .HasMany(e => e.Tours)
                .WithRequired(e => e.DepartureCity)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Requests)
                .WithRequired(e => e.Employee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Hotel>()
                .HasMany(e => e.Tours)
                .WithRequired(e => e.Hotel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Provider>()
                .HasMany(e => e.Services)
                .WithRequired(e => e.Provider)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Request>()
                .HasMany(e => e.RequestWithServices)
                .WithRequired(e => e.Request)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RequestWithService>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Resort>()
                .HasMany(e => e.Hotels)
                .WithRequired(e => e.Resort)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.Role)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Service>()
                .HasMany(e => e.RequestWithServices)
                .WithRequired(e => e.Service)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Tour>()
                .Property(e => e.Cost)
                .IsFixedLength();

            modelBuilder.Entity<Tour>()
                .HasMany(e => e.Requests)
                .WithRequired(e => e.Tour)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Transport>()
                .HasMany(e => e.Tours)
                .WithRequired(e => e.Transport)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TransportClass>()
                .HasMany(e => e.Transports)
                .WithRequired(e => e.TransportClass)
                .HasForeignKey(e => e.ClassId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TypeOfDiet>()
                .HasMany(e => e.Hotels)
                .WithRequired(e => e.TypeOfDiet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Clients)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Employees)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);
        }
    }
}
