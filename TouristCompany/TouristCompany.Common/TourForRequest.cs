﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TouristCompany.Common
{
    public class TourForRequest
    {
        public string Tour { get; set; }
        public string DepartureDate { get; set; }
        public string Resort { get; set; }
        public string Hotel { get; set; }
        public string Transport { get; set; }
        public int People { get; set; }
        public int Nights { get; set; }
        public string Price { get; set; }
    }
}
