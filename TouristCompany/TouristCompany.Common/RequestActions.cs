﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TouristCompany.Common
{
    public static class RequestActions
    {
        public static string _userEmail = "";
        public static TourForRequest _tourInfo = new TourForRequest();
        public static decimal _totalCost = 0;

        public static List<string> _services = new List<string>();

        public static List<string> _selectedServices = new List<string>();

        /// <summary>
        /// Get current user email.
        /// </summary>
        public static void GetUserEmail()
        {
            _userEmail = RequestSQLHelpers.UserEmailByUserId(UserActions._userId);
        }

        /// <summary>
        /// Fill services from DB.
        /// </summary>
        public static void FillServices()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;
            _services = RequestSQLHelpers.AllServicesNames(selectedLanguage);
        }

        /// <summary>
        /// Fill tour info from DB.
        /// </summary>
        public static void FillTourInfo()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;
            _tourInfo = RequestSQLHelpers.TourInfoById(selectedLanguage, TourSelectionActions._requestTourId);

            _totalCost = decimal.Parse(_tourInfo.Price);
        }

        /// <summary>
        /// Work with total cost.
        /// </summary>
        public static void TotalCostChange()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;
            decimal sumPrice = 0;

            foreach (var service in _selectedServices)
            {
                decimal price = decimal.Parse(RequestSQLHelpers.ServicePriceByName(selectedLanguage, service));
                sumPrice += price;
            }

            _totalCost = decimal.Parse(_tourInfo.Price) + sumPrice;
        }

        /// <summary>
        /// Making of request.
        /// </summary>
        public static void MakeRequest()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;

            RequestSQLHelpers.InsertRequest(DateTime.Now, TourSelectionActions._requestTourId, UserActions._userId);
            foreach (var service in _selectedServices)
            {
                RequestSQLHelpers.InsertServicesForRequest(selectedLanguage, service);
            }
        }

    }
}
