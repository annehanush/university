﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TouristCompany.Common
{
    public static class TourSelectionSQLHelpers
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["TouristCompanyDbContext"].ConnectionString;

        /// <summary>
        /// Get names of all the countries from DB.
        /// </summary>
        /// <returns></returns>
        public static List<string> AllCountriesNames(string lang)
        {
            List<string> _countries = new List<string>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_all_countries_names", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string name = dr.GetString(0);
                        _countries.Add(name);
                    }
                }
            }
            return _countries;
        }

        /// <summary>
        /// Get name of the one top country from DB.
        /// </summary>
        /// <returns></returns>
        public static string TopCountry(string lang)
        {
            string _country = "";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_top_country_name", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string name = dr.GetString(0);
                        _country = name;
                    }
                }
            }

            return _country;
        }

        /// <summary>
        /// Get names of all the departure cities from DB.
        /// </summary>
        /// <returns></returns>
        public static List<string> AllDepartureCitiesNames(string lang)
        {
            List<string> _cities = new List<string>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_departure_city_names", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string name = dr.GetString(0);
                        _cities.Add(name);
                    }
                }
            }
            return _cities;
        }

        /// <summary>
        /// Get name of the one top departure city from DB.
        /// </summary>
        /// <returns></returns>
        public static string TopDepartureCity(string lang)
        {
            string _city = "";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_top_departure_city_name", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string name = dr.GetString(0);
                        _city = name;
                    }
                }
            }

            return _city;
        }

        /// <summary>
        /// Get names of all the types of diets from DB.
        /// </summary>
        /// <returns></returns>
        public static List<string> AllTypeOfDietsNames()
        {
            List<string> _diets = new List<string>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_type_of_diet_names", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string name = dr.GetString(0);
                        _diets.Add(name);
                    }
                }
            }
            return _diets;
        }

        /// <summary>
        /// Get names of all the resorts od a certain country from DB.
        /// </summary>
        /// <returns></returns>
        public static List<string> AllResortsNamesOfACertainCountry(string countryName, string lang)
        {
            List<string> _resorts = new List<string>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_resort_names_by_country", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@countryName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = countryName;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string name = dr.GetString(0);
                        _resorts.Add(name);
                    }
                }
            }
            return _resorts;
        }

        /// <summary>
        /// Get names and ratings of all the hotels of a certain country from DB.
        /// </summary>
        /// <returns></returns>
        public static List<Tuple<string, int>> AllHotelsNamesWithRatingOfACertainCountry(string countryName, string lang)
        {
            List<Tuple<string, int>> _hotels = new List<Tuple<string, int>>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_hotel_names_by_country", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@countryName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = countryName;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string name = dr.GetString(0);
                        int rating = dr.GetInt32(1);

                        var currentTuple = new Tuple<string, int>(name, rating);

                        _hotels.Add(currentTuple);
                    }
                }
            }

            return _hotels;
        }

        /// <summary>
        /// Get names of all the tours of a certain country and departure city from DB.
        /// </summary>
        /// <returns></returns>
        public static List<string> AllToursNamesOfACertainCountryAndDepartureCity(string lang, string countryName, string departureCityName)
        {
            List<string> _tours = new List<string>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_tour_names_by_country_and_departure_city", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@countryName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = countryName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@departureCityName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = departureCityName;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string name = dr.GetString(0);
                        _tours.Add(name);
                    }
                }
            }
            return _tours;
        }

        /// <summary>
        /// Get name of the one top tour of country and departure city from DB.
        /// </summary>
        /// <returns></returns>
        public static string TopTourName(string lang, string countryName, string departureCityName)
        {
            string _tour = "";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_top_tour_name", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@countryName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = countryName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@departureCityName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = departureCityName;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string name = dr.GetString(0);
                        _tour = name;
                    }
                }
            }

            return _tour;
        }

        /// <summary>
        /// Get nights amounts of all the tours of a certain country and departure city from DB.
        /// </summary>
        /// <returns></returns>
        public static List<int> AllToursNightsOfACertainCountryAndDepartureCity(string lang, string countryName, string departureCityName)
        {
            List<int> _nights = new List<int>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_tour_nights_by_country_and_departure_city", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@countryName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = countryName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@departureCityName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = departureCityName;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        int amount = dr.GetInt32(0);
                        _nights.Add(amount);
                    }
                }
            }
            return _nights;
        }

        /// <summary>
        /// Get nights of the one top tour of country and departure city from DB.
        /// </summary>
        /// <returns></returns>
        public static int TopNightsAmount(string lang, string countryName, string departureCityName)
        {
            int _nights = -1;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_top_nights_amount", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@countryName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = countryName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@departureCityName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = departureCityName;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        int amount = dr.GetInt32(0);
                        _nights = amount;
                    }
                }
            }

            return _nights;
        }

        /// <summary>
        /// Get people amounts of all the tours of a certain country and departure city from DB.
        /// </summary>
        /// <returns></returns>
        public static List<int> AllToursPeopleOfACertainCountryAndDepartureCity(string lang, string countryName, string departureCityName)
        {
            List<int> _people = new List<int>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_tour_people_by_country_and_departure_city", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@countryName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = countryName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@departureCityName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = departureCityName;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        int amount = dr.GetInt32(0);
                        _people.Add(amount);
                    }
                }
            }
            return _people;
        }

        /// <summary>
        /// Get people amount of the one top tour of country and departure city from DB.
        /// </summary>
        /// <returns></returns>
        public static int TopPeopleAmount(string lang, string countryName, string departureCityName)
        {
            int _people = -1;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_top_people_amount", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@countryName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = countryName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@departureCityName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = departureCityName;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        int amount = dr.GetInt32(0);
                        _people = amount;
                    }
                }
            }

            return _people;
        }

        /// <summary>
        /// Get names and ratings of all the hotels of a certain resort from DB.
        /// </summary>
        /// <returns></returns>
        public static List<Tuple<string, int>> AllHotelsNamesWithRatingOfACertainResort(string resortName, string lang)
        {
            List<Tuple<string, int>> _hotels = new List<Tuple<string, int>>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_hotel_names_by_resort", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@resortName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = resortName;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string name = dr.GetString(0);
                        int rating = dr.GetInt32(1);

                        var currentTuple = new Tuple<string, int>(name, rating);

                        _hotels.Add(currentTuple);
                    }
                }
            }

            return _hotels;
        }

        /// <summary>
        /// Get names and ratings of all the hotels of a certain rating from DB.
        /// </summary>
        /// <returns></returns>
        public static List<Tuple<string, int>> AllHotelsNamesWithRatingOfACertainCategory(string lang, int category, string countryName)
        {
            List<Tuple<string, int>> _hotels = new List<Tuple<string, int>>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_hotel_names_by_category", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@rating";
                param.SqlDbType = SqlDbType.Int;
                param.Value = category;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@countryName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = countryName;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string name = dr.GetString(0);
                        int rating = dr.GetInt32(1);

                        var currentTuple = new Tuple<string, int>(name, rating);

                        _hotels.Add(currentTuple);
                    }
                }
            }

            return _hotels;
        }

        /// <summary>
        /// Get names and ratings of all the hotels of a certain rating and resort from DB.
        /// </summary>
        /// <returns></returns>
        public static List<Tuple<string, int>> AllHotelsNamesWithRatingOfACertainCategoryAndResort(string lang, int category, string resortName)
        {
            List<Tuple<string, int>> _hotels = new List<Tuple<string, int>>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_hotel_names_by_resort_and_category", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@rating";
                param.SqlDbType = SqlDbType.Int;
                param.Value = category;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@resortName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = resortName;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string name = dr.GetString(0);
                        int rating = dr.GetInt32(1);

                        var currentTuple = new Tuple<string, int>(name, rating);

                        _hotels.Add(currentTuple);
                    }
                }
            }

            return _hotels;
        }

        /// <summary>
        /// Get suitable tours by COUNTRY from DB.
        /// </summary>
        /// <returns></returns>
        public static List<SuitableTour> SuitableToursCountry(string lang, DateTime dateFrom, DateTime dateTo, string countryName, string departureCityName, int nightsFrom, int nightsTo, int people)
        {
            List<SuitableTour> _tours = new List<SuitableTour>(); 

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_tour_suitable_country", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateFrom";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateTo";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@countryName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = countryName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@departureCityName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = departureCityName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsFrom";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsTo";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@people";
                param.SqlDbType = SqlDbType.Int;
                param.Value = people;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        SuitableTour tour = new SuitableTour();
                        tour.DepartureDate = dr.GetDateTime(0).ToShortDateString();
                        tour.TourName = dr.GetString(1);
                        tour.Nights = dr.GetInt32(2);
                        tour.Hotel = dr.GetString(3).ToUpper() + " " + dr.GetInt32(4).ToString() + "*";
                        tour.Diet = dr.GetString(5);
                        tour.People = dr.GetInt32(6);
                        if (TourSelectionActions._selectedCurrency == "USD")
                        {
                            tour.Price = dr.GetSqlMoney(7).ToString() + " USD";
                        }
                        else
                        {
                            tour.Price = (dr.GetSqlMoney(7) * 2).ToString() + " BYN";
                        }
                        tour.Transport = dr.GetString(8) + " -- " + dr.GetString(9);
                        tour.TourId = dr.GetInt32(10);

                        _tours.Add(tour);
                    }
                }
            }

            return _tours;
        }

        /// <summary>
        /// Get suitable tours by COUNTRY + DIET from DB.
        /// </summary>
        /// <returns></returns>
        public static List<SuitableTour> SuitableToursCountryDiet(string lang, DateTime dateFrom, DateTime dateTo, string countryName, string departureCityName, int nightsFrom, int nightsTo, int people, string diet)
        {
            List<SuitableTour> _tours = new List<SuitableTour>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_tour_suitable_country_diet", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateFrom";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateTo";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@countryName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = countryName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@departureCityName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = departureCityName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsFrom";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsTo";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@people";
                param.SqlDbType = SqlDbType.Int;
                param.Value = people;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dietName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = diet;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        SuitableTour tour = new SuitableTour();
                        tour.DepartureDate = dr.GetDateTime(0).ToShortDateString();
                        tour.TourName = dr.GetString(1);
                        tour.Nights = dr.GetInt32(2);
                        tour.Hotel = dr.GetString(3).ToUpper() + " " + dr.GetInt32(4).ToString() + "*";
                        tour.Diet = dr.GetString(5);
                        tour.People = dr.GetInt32(6);
                        if (TourSelectionActions._selectedCurrency == "USD")
                        {
                            tour.Price = dr.GetSqlMoney(7).ToString() + " USD";
                        }
                        else
                        {
                            tour.Price = (dr.GetSqlMoney(7) * 2).ToString() + " BYN";
                        }
                        tour.Transport = dr.GetString(8) + " -- " + dr.GetString(9);
                        tour.TourId = dr.GetInt32(10);

                        _tours.Add(tour);
                    }
                }
            }

            return _tours;
        }

        /// <summary>
        /// Get suitable tours by RESORT from DB.
        /// </summary>
        /// <returns></returns>
        public static List<SuitableTour> SuitableToursResort(string lang, DateTime dateFrom, DateTime dateTo, string resortName, string departureCityName, int nightsFrom, int nightsTo, int people)
        {
            List<SuitableTour> _tours = new List<SuitableTour>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_tour_suitable_resort", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateFrom";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateTo";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@resortName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = resortName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@departureCityName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = departureCityName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsFrom";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsTo";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@people";
                param.SqlDbType = SqlDbType.Int;
                param.Value = people;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        SuitableTour tour = new SuitableTour();
                        tour.DepartureDate = dr.GetDateTime(0).ToShortDateString();
                        tour.TourName = dr.GetString(1);
                        tour.Nights = dr.GetInt32(2);
                        tour.Hotel = dr.GetString(3).ToUpper() + " " + dr.GetInt32(4).ToString() + "*";
                        tour.Diet = dr.GetString(5);
                        tour.People = dr.GetInt32(6);
                        if (TourSelectionActions._selectedCurrency == "USD")
                        {
                            tour.Price = dr.GetSqlMoney(7).ToString() + " USD";
                        }
                        else
                        {
                            tour.Price = (dr.GetSqlMoney(7) * 2).ToString() + " BYN";
                        }
                        tour.Transport = dr.GetString(8) + " -- " + dr.GetString(9);
                        tour.TourId = dr.GetInt32(10);

                        _tours.Add(tour);
                    }
                }
            }

            return _tours;
        }

        /// <summary>
        /// Get suitable tours by RESORT + DIET from DB.
        /// </summary>
        /// <returns></returns>
        public static List<SuitableTour> SuitableToursResortDiet(string lang, DateTime dateFrom, DateTime dateTo, string resortName, string departureCityName, int nightsFrom, int nightsTo, int people, string diet)
        {
            List<SuitableTour> _tours = new List<SuitableTour>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_tour_suitable_resort_diet", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateFrom";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateTo";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@resortName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = resortName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@departureCityName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = departureCityName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsFrom";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsTo";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@people";
                param.SqlDbType = SqlDbType.Int;
                param.Value = people;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dietName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = diet;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        SuitableTour tour = new SuitableTour();
                        tour.DepartureDate = dr.GetDateTime(0).ToShortDateString();
                        tour.TourName = dr.GetString(1);
                        tour.Nights = dr.GetInt32(2);
                        tour.Hotel = dr.GetString(3).ToUpper() + " " + dr.GetInt32(4).ToString() + "*";
                        tour.Diet = dr.GetString(5);
                        tour.People = dr.GetInt32(6);
                        if (TourSelectionActions._selectedCurrency == "USD")
                        {
                            tour.Price = dr.GetSqlMoney(7).ToString() + " USD";
                        }
                        else
                        {
                            tour.Price = (dr.GetSqlMoney(7) * 2).ToString() + " BYN";
                        }
                        tour.Transport = dr.GetString(8) + " -- " + dr.GetString(9);
                        tour.TourId = dr.GetInt32(10);

                        _tours.Add(tour);
                    }
                }
            }

            return _tours;
        }

        /// <summary>
        /// Get suitable tours by CATEGORY from DB.
        /// </summary>
        /// <returns></returns>
        public static List<SuitableTour> SuitableToursCategory(string lang, DateTime dateFrom, DateTime dateTo, string countryName, int category, string departureCityName, int nightsFrom, int nightsTo, int people)
        {
            List<SuitableTour> _tours = new List<SuitableTour>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_tour_suitable_category", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateFrom";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateTo";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@countryName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = countryName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@category";
                param.SqlDbType = SqlDbType.Int;
                param.Value = category;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@departureCityName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = departureCityName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsFrom";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsTo";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@people";
                param.SqlDbType = SqlDbType.Int;
                param.Value = people;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        SuitableTour tour = new SuitableTour();
                        tour.DepartureDate = dr.GetDateTime(0).ToShortDateString();
                        tour.TourName = dr.GetString(1);
                        tour.Nights = dr.GetInt32(2);
                        tour.Hotel = dr.GetString(3).ToUpper() + " " + dr.GetInt32(4).ToString() + "*";
                        tour.Diet = dr.GetString(5);
                        tour.People = dr.GetInt32(6);
                        if (TourSelectionActions._selectedCurrency == "USD")
                        {
                            tour.Price = dr.GetSqlMoney(7).ToString() + " USD";
                        }
                        else
                        {
                            tour.Price = (dr.GetSqlMoney(7) * 2).ToString() + " BYN";
                        }
                        tour.Transport = dr.GetString(8) + " -- " + dr.GetString(9);
                        tour.TourId = dr.GetInt32(10);

                        _tours.Add(tour);
                    }
                }
            }

            return _tours;
        }

        /// <summary>
        /// Get suitable tours by CATEGORY + DIET from DB.
        /// </summary>
        /// <returns></returns>
        public static List<SuitableTour> SuitableToursCategoryDiet(string lang, DateTime dateFrom, DateTime dateTo, string countryName, int category, string departureCityName, int nightsFrom, int nightsTo, int people, string diet)
        {
            List<SuitableTour> _tours = new List<SuitableTour>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_tour_suitable_category_diet", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateFrom";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateTo";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@countryName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = countryName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@category";
                param.SqlDbType = SqlDbType.Int;
                param.Value = category;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@departureCityName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = departureCityName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsFrom";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsTo";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@people";
                param.SqlDbType = SqlDbType.Int;
                param.Value = people;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dietName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = diet;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        SuitableTour tour = new SuitableTour();
                        tour.DepartureDate = dr.GetDateTime(0).ToShortDateString();
                        tour.TourName = dr.GetString(1);
                        tour.Nights = dr.GetInt32(2);
                        tour.Hotel = dr.GetString(3).ToUpper() + " " + dr.GetInt32(4).ToString() + "*";
                        tour.Diet = dr.GetString(5);
                        tour.People = dr.GetInt32(6);
                        if (TourSelectionActions._selectedCurrency == "USD")
                        {
                            tour.Price = dr.GetSqlMoney(7).ToString() + " USD";
                        }
                        else
                        {
                            tour.Price = (dr.GetSqlMoney(7) * 2).ToString() + " BYN";
                        }
                        tour.Transport = dr.GetString(8) + " -- " + dr.GetString(9);
                        tour.TourId = dr.GetInt32(10);

                        _tours.Add(tour);
                    }
                }
            }

            return _tours;
        }

        /// <summary>
        /// Get suitable tours by RESORT + CATEGORY from DB.
        /// </summary>
        /// <returns></returns>
        public static List<SuitableTour> SuitableToursResortCategory(string lang, DateTime dateFrom, DateTime dateTo, string resortName, int category, string departureCityName, int nightsFrom, int nightsTo, int people)
        {
            List<SuitableTour> _tours = new List<SuitableTour>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_tour_suitable_resort_category", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateFrom";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateTo";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@resortName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = resortName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@category";
                param.SqlDbType = SqlDbType.Int;
                param.Value = category;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@departureCityName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = departureCityName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsFrom";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsTo";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@people";
                param.SqlDbType = SqlDbType.Int;
                param.Value = people;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        SuitableTour tour = new SuitableTour();
                        tour.DepartureDate = dr.GetDateTime(0).ToShortDateString();
                        tour.TourName = dr.GetString(1);
                        tour.Nights = dr.GetInt32(2);
                        tour.Hotel = dr.GetString(3).ToUpper() + " " + dr.GetInt32(4).ToString() + "*";
                        tour.Diet = dr.GetString(5);
                        tour.People = dr.GetInt32(6);
                        if (TourSelectionActions._selectedCurrency == "USD")
                        {
                            tour.Price = dr.GetSqlMoney(7).ToString() + " USD";
                        }
                        else
                        {
                            tour.Price = (dr.GetSqlMoney(7) * 2).ToString() + " BYN";
                        }
                        tour.Transport = dr.GetString(8) + " -- " + dr.GetString(9);
                        tour.TourId = dr.GetInt32(10);

                        _tours.Add(tour);
                    }
                }
            }

            return _tours;
        }

        /// <summary>
        /// Get suitable tours by RESORT + CATEGORY + DIET from DB.
        /// </summary>
        /// <returns></returns>
        public static List<SuitableTour> SuitableToursResortCategoryDiet(string lang, DateTime dateFrom, DateTime dateTo, string resortName, int category, string departureCityName, int nightsFrom, int nightsTo, int people, string diet)
        {
            List<SuitableTour> _tours = new List<SuitableTour>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_tour_suitable_resort_category_diet", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateFrom";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateTo";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@resortName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = resortName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@category";
                param.SqlDbType = SqlDbType.Int;
                param.Value = category;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@departureCityName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = departureCityName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsFrom";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsTo";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@people";
                param.SqlDbType = SqlDbType.Int;
                param.Value = people;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dietName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = diet;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        SuitableTour tour = new SuitableTour();
                        tour.DepartureDate = dr.GetDateTime(0).ToShortDateString();
                        tour.TourName = dr.GetString(1);
                        tour.Nights = dr.GetInt32(2);
                        tour.Hotel = dr.GetString(3).ToUpper() + " " + dr.GetInt32(4).ToString() + "*";
                        tour.Diet = dr.GetString(5);
                        tour.People = dr.GetInt32(6);
                        if (TourSelectionActions._selectedCurrency == "USD")
                        {
                            tour.Price = dr.GetSqlMoney(7).ToString() + " USD";
                        }
                        else
                        {
                            tour.Price = (dr.GetSqlMoney(7) * 2).ToString() + " BYN";
                        }
                        tour.Transport = dr.GetString(8) + " -- " + dr.GetString(9);
                        tour.TourId = dr.GetInt32(10);

                        _tours.Add(tour);
                    }
                }
            }

            return _tours;
        }

        /// <summary>
        /// Get suitable tours by HOTEL from DB.
        /// </summary>
        /// <returns></returns>
        public static List<SuitableTour> SuitableToursHotel(string lang, DateTime dateFrom, DateTime dateTo, string hotelName, string departureCityName, int nightsFrom, int nightsTo, int people)
        {
            List<SuitableTour> _tours = new List<SuitableTour>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_tour_suitable_hotel", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateFrom";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateTo";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@hotelName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = hotelName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@departureCityName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = departureCityName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsFrom";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsTo";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@people";
                param.SqlDbType = SqlDbType.Int;
                param.Value = people;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        SuitableTour tour = new SuitableTour();
                        tour.DepartureDate = dr.GetDateTime(0).ToShortDateString();
                        tour.TourName = dr.GetString(1);
                        tour.Nights = dr.GetInt32(2);
                        tour.Hotel = dr.GetString(3).ToUpper() + " " + dr.GetInt32(4).ToString() + "*";
                        tour.Diet = dr.GetString(5);
                        tour.People = dr.GetInt32(6);
                        if (TourSelectionActions._selectedCurrency == "USD")
                        {
                            tour.Price = dr.GetSqlMoney(7).ToString() + " USD";
                        }
                        else
                        {
                            tour.Price = (dr.GetSqlMoney(7) * 2).ToString() + " BYN";
                        }
                        tour.Transport = dr.GetString(8) + " -- " + dr.GetString(9);
                        tour.TourId = dr.GetInt32(10);

                        _tours.Add(tour);
                    }
                }
            }

            return _tours;
        }

        /// <summary>
        /// Get suitable tours by HOTEL + DIET from DB.
        /// </summary>
        /// <returns></returns>
        public static List<SuitableTour> SuitableToursHotelDiet(string lang, DateTime dateFrom, DateTime dateTo, string hotelName, string departureCityName, int nightsFrom, int nightsTo, int people, string diet)
        {
            List<SuitableTour> _tours = new List<SuitableTour>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_tour_suitable_hotel_diet", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateFrom";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dateTo";
                param.SqlDbType = SqlDbType.Date;
                param.Value = dateTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@hotelName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = hotelName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@departureCityName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = departureCityName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsFrom";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsFrom;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@nightsTo";
                param.SqlDbType = SqlDbType.Int;
                param.Value = nightsTo;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@people";
                param.SqlDbType = SqlDbType.Int;
                param.Value = people;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@dietName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = diet;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        SuitableTour tour = new SuitableTour();
                        tour.DepartureDate = dr.GetDateTime(0).ToShortDateString();
                        tour.TourName = dr.GetString(1);
                        tour.Nights = dr.GetInt32(2);
                        tour.Hotel = dr.GetString(3).ToUpper() + " " + dr.GetInt32(4).ToString() + "*";
                        tour.Diet = dr.GetString(5);
                        tour.People = dr.GetInt32(6);
                        if (TourSelectionActions._selectedCurrency == "USD")
                        {
                            tour.Price = dr.GetSqlMoney(7).ToString() + " USD";
                        }
                        else
                        {
                            tour.Price = (dr.GetSqlMoney(7) * 2).ToString() + " BYN";
                        }
                        tour.Transport = dr.GetString(8) + " -- " + dr.GetString(9);
                        tour.TourId = dr.GetInt32(10);

                        _tours.Add(tour);
                    }
                }
            }

            return _tours;
        }

    }
}
