﻿using System;

namespace TouristCompany.Common
{
    public class SuitableTour
    {
        public string DepartureDate { get; set; }
        public string TourName { get; set; }
        public int Nights { get; set; }
        public string Hotel { get; set; }
        public string Diet { get; set; }
        public int People { get; set; }
        public string Price { get; set; }
        public string Transport { get; set; }
        public int TourId { get; set; }
    }
}
