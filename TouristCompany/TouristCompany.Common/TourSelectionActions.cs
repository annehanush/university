﻿using System;
using System.Collections.Generic;

namespace TouristCompany.Common
{
    public static class TourSelectionActions
    {
        public static int _requestTourId = -1;

        public static string _selectedCountry = "";
        public static string _selectedDepartureCity = "";
        public static string _selectedTourName = "";
        public static int _selectedNightsFrom = -1;
        public static int _selectedNightsTo = -1;
        public static int _selectedPeopleAmount = -1;
        public static DateTime _selectedDepartureFromDate = DateTime.Now;
        public static DateTime _selectedDepartureToDate = DateTime.Now;
        public static string _selectedCurrency = "USD";
        public static List<string> _selectedResorts = new List<string>();
        public static List<int> _selectedCategory = new List<int>();
        public static List<string> _selectedTypesOfDiets = new List<string>();
        public static List<string> _selectedHotels = new List<string>();

        public static List<SuitableTour> _suitableTours = new List<SuitableTour>();

        public static List<string> _countries = new List<string>();
        public static List<string> _departureCities = new List<string>();
        public static List<string> _typesOfDiets = new List<string>();
        public static List<string> _reosrts = new List<string>();
        public static List<Tuple<string, int>> _hotels = new List<Tuple<string, int>>();
        public static List<string> _toursNames = new List<string>();
        public static List<int> _nightsFrom = new List<int>();
        public static List<int> _nightsTo = new List<int>();
        public static List<int> _peopleAmount = new List<int>();

        /// <summary>
        /// Countries filling using SQL query.
        /// </summary>
        public static void FillCountries()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;

            _countries = TourSelectionSQLHelpers.AllCountriesNames(selectedLanguage);
            _selectedCountry = TourSelectionSQLHelpers.TopCountry(selectedLanguage);
        }

        /// <summary>
        /// Departure filling using SQL query.
        /// </summary>
        public static void FillDepartureCities()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;

            _departureCities = TourSelectionSQLHelpers.AllDepartureCitiesNames(selectedLanguage);
            _selectedDepartureCity = TourSelectionSQLHelpers.TopDepartureCity(selectedLanguage);
        }

        public static void FillTypesOfDiets()
        {
            _typesOfDiets = TourSelectionSQLHelpers.AllTypeOfDietsNames();
        }

        public static void FillResorts()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;

            _reosrts = TourSelectionSQLHelpers.AllResortsNamesOfACertainCountry(_selectedCountry, selectedLanguage);
        }

        public static void FillHotels()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;

            _hotels = TourSelectionSQLHelpers.AllHotelsNamesWithRatingOfACertainCountry(_selectedCountry, selectedLanguage);
        }

        public static void FillTours()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;

            _toursNames = TourSelectionSQLHelpers.AllToursNamesOfACertainCountryAndDepartureCity(selectedLanguage, _selectedCountry, _selectedDepartureCity);
            _selectedTourName = " ---";
        }

        public static void FillNights()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;

            _nightsFrom = TourSelectionSQLHelpers.AllToursNightsOfACertainCountryAndDepartureCity(selectedLanguage, _selectedCountry, _selectedDepartureCity);
            _nightsTo = TourSelectionSQLHelpers.AllToursNightsOfACertainCountryAndDepartureCity(selectedLanguage, _selectedCountry, _selectedDepartureCity);
            _selectedNightsFrom = TourSelectionSQLHelpers.TopNightsAmount(selectedLanguage, _selectedCountry, _selectedDepartureCity);
            _selectedNightsTo = TourSelectionSQLHelpers.TopNightsAmount(selectedLanguage, _selectedCountry, _selectedDepartureCity);
        }

        public static void FillPeople()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;

            _peopleAmount = TourSelectionSQLHelpers.AllToursPeopleOfACertainCountryAndDepartureCity(selectedLanguage, _selectedCountry, _selectedDepartureCity);
            _selectedPeopleAmount = TourSelectionSQLHelpers.TopPeopleAmount(selectedLanguage, _selectedCountry, _selectedDepartureCity);
        }

        public static void FillHotelsByResorts()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;
            _hotels.Clear();

            foreach (var _resort in _selectedResorts)
            {
                List<Tuple<string, int>> hotelsOfResort = new List<Tuple<string, int>>();
                List<Tuple<string, int>> hotels = TourSelectionSQLHelpers.AllHotelsNamesWithRatingOfACertainResort(_resort, selectedLanguage);
                List<Tuple<string, int>> toRemove = new List<Tuple<string, int>>();

                foreach (var hotel in hotels)
                {
                    if (!hotelsOfResort.Contains(hotel))
                    {
                        hotelsOfResort.Add(hotel);
                    }
                }

                foreach (var _hotel in hotelsOfResort)
                {
                    if (!hotels.Contains(_hotel))
                    {
                        toRemove.Add(_hotel);
                    }
                }

                foreach (var item in toRemove)
                {
                    hotelsOfResort.Remove(item);
                }

                _hotels.AddRange(hotelsOfResort);
            }
        }

        public static void FillHotelsByCategory()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;
            _hotels.Clear();

            foreach (var _category in _selectedCategory)
            {
                List<Tuple<string, int>> hotelsOfCategory = new List<Tuple<string, int>>();
                List<Tuple<string, int>> hotels = TourSelectionSQLHelpers.AllHotelsNamesWithRatingOfACertainCategory(selectedLanguage, _category, _selectedCountry);
                List<Tuple<string, int>> toRemove = new List<Tuple<string, int>>();

                foreach (var hotel in hotels)
                {
                    if (!hotelsOfCategory.Contains(hotel))
                    {
                        hotelsOfCategory.Add(hotel);
                    }
                }

                foreach (var _hotel in hotelsOfCategory)
                {
                    if (!hotels.Contains(_hotel))
                    {
                        toRemove.Add(_hotel);
                    }
                }

                foreach (var item in toRemove)
                {
                    hotelsOfCategory.Remove(item);
                }

                _hotels.AddRange(hotelsOfCategory);
            }
        }

        public static void FillHotelsByCategoryAndResort()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;
            _hotels.Clear();

            foreach (var _resort in _selectedResorts)
            {
                foreach (var _category in _selectedCategory)
                {
                    List<Tuple<string, int>> hotelsOfCategoryAndResort = new List<Tuple<string, int>>();
                    List<Tuple<string, int>> hotels = TourSelectionSQLHelpers.AllHotelsNamesWithRatingOfACertainCategoryAndResort(selectedLanguage, _category, _resort);
                    List<Tuple<string, int>> toRemove = new List<Tuple<string, int>>();

                    foreach (var hotel in hotels)
                    {
                        if (!hotelsOfCategoryAndResort.Contains(hotel))
                        {
                            hotelsOfCategoryAndResort.Add(hotel);
                        }
                    }

                    foreach (var _hotel in hotelsOfCategoryAndResort)
                    {
                        if (!hotels.Contains(_hotel))
                        {
                            toRemove.Add(_hotel);
                        }
                    }

                    foreach (var item in toRemove)
                    {
                        hotelsOfCategoryAndResort.Remove(item);
                    }

                    _hotels.AddRange(hotelsOfCategoryAndResort);
                }
            }
        }

        public static void FillSuitableToursCountry()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;
            _suitableTours.Clear();

            _suitableTours = TourSelectionSQLHelpers.SuitableToursCountry(selectedLanguage, _selectedDepartureFromDate, _selectedDepartureToDate, _selectedCountry, _selectedDepartureCity, _selectedNightsFrom, _selectedNightsTo, _selectedPeopleAmount);
        }

        public static void FillSuitableToursCountryDiet()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;
            _suitableTours.Clear();

            foreach (var diet in _selectedTypesOfDiets)
            {
                List<SuitableTour> tmp = TourSelectionSQLHelpers.SuitableToursCountryDiet(selectedLanguage, _selectedDepartureFromDate, _selectedDepartureToDate, _selectedCountry, _selectedDepartureCity, _selectedNightsFrom, _selectedNightsTo, _selectedPeopleAmount, diet);

                foreach (var item in tmp)
                {
                    if (!_suitableTours.Contains(item))
                    {
                        _suitableTours.Add(item);
                    }
                }
            }
        }

        public static void FillSuitableToursResort()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;
            _suitableTours.Clear();

            foreach (var resort in _selectedResorts)
            {
                List<SuitableTour> tmp = TourSelectionSQLHelpers.SuitableToursResort(selectedLanguage, _selectedDepartureFromDate, _selectedDepartureToDate, resort, _selectedDepartureCity, _selectedNightsFrom, _selectedNightsTo, _selectedPeopleAmount);

                foreach (var item in tmp)
                {
                    if (!_suitableTours.Contains(item))
                    {
                        _suitableTours.Add(item);
                    }
                }
            }
        }

        public static void FillSuitableToursResortDiet()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;
            _suitableTours.Clear();

            foreach (var resort in _selectedResorts)
            {
                foreach (var diet in _selectedTypesOfDiets)
                {
                    List<SuitableTour> tmp = TourSelectionSQLHelpers.SuitableToursResortDiet(selectedLanguage, _selectedDepartureFromDate, _selectedDepartureToDate, resort, _selectedDepartureCity, _selectedNightsFrom, _selectedNightsTo, _selectedPeopleAmount, diet);

                    foreach (var item in tmp)
                    {
                        if (!_suitableTours.Contains(item))
                        {
                            _suitableTours.Add(item);
                        }
                    }
                }
            }
        }

        public static void FillSuitableToursCategory()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;
            _suitableTours.Clear();

            foreach (var category in _selectedCategory)
            {
                List<SuitableTour> tmp = TourSelectionSQLHelpers.SuitableToursCategory(selectedLanguage, _selectedDepartureFromDate, _selectedDepartureToDate, _selectedCountry, category, _selectedDepartureCity, _selectedNightsFrom, _selectedNightsTo, _selectedPeopleAmount);

                foreach (var item in tmp)
                {
                    if (!_suitableTours.Contains(item))
                    {
                        _suitableTours.Add(item);
                    }
                }
            }
        }

        public static void FillSuitableToursCategoryDiet()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;
            _suitableTours.Clear();

            foreach (var category in _selectedCategory)
            {
                foreach (var diet in _selectedTypesOfDiets)
                {
                    List<SuitableTour> tmp = TourSelectionSQLHelpers.SuitableToursCategoryDiet(selectedLanguage, _selectedDepartureFromDate, _selectedDepartureToDate, _selectedCountry, category, _selectedDepartureCity, _selectedNightsFrom, _selectedNightsTo, _selectedPeopleAmount, diet);

                    foreach (var item in tmp)
                    {
                        if (!_suitableTours.Contains(item))
                        {
                            _suitableTours.Add(item);
                        }
                    }
                }
            }
        }

        public static void FillSuitableToursResortCategory()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;
            _suitableTours.Clear();

            foreach (var resort in _selectedResorts)
            {
                foreach (var category in _selectedCategory)
                {
                    List<SuitableTour> tmp = TourSelectionSQLHelpers.SuitableToursResortCategory(selectedLanguage, _selectedDepartureFromDate, _selectedDepartureToDate, resort, category, _selectedDepartureCity, _selectedNightsFrom, _selectedNightsTo, _selectedPeopleAmount);

                    foreach (var item in tmp)
                    {
                        if (!_suitableTours.Contains(item))
                        {
                            _suitableTours.Add(item);
                        }
                    }
                }
            }
        }

        public static void FillSuitableToursResortCategoryDiet()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;
            _suitableTours.Clear();

            foreach (var resort in _selectedResorts)
            {
                foreach (var category in _selectedCategory)
                {
                    foreach (var diet in _selectedTypesOfDiets)
                    {
                        List<SuitableTour> tmp = TourSelectionSQLHelpers.SuitableToursResortCategoryDiet(selectedLanguage, _selectedDepartureFromDate, _selectedDepartureToDate, resort, category, _selectedDepartureCity, _selectedNightsFrom, _selectedNightsTo, _selectedPeopleAmount, diet);

                        foreach (var item in tmp)
                        {
                            if (!_suitableTours.Contains(item))
                            {
                                _suitableTours.Add(item);
                            }
                        }
                    }
                }
            }
        }

        public static void FillSuitableToursHotel()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;
            _suitableTours.Clear();

            foreach (var hotel in _selectedHotels)
            {
                List<SuitableTour> tmp = TourSelectionSQLHelpers.SuitableToursHotel(selectedLanguage, _selectedDepartureFromDate, _selectedDepartureToDate, hotel, _selectedDepartureCity, _selectedNightsFrom, _selectedNightsTo, _selectedPeopleAmount);

                foreach (var item in tmp)
                {
                    if (!_suitableTours.Contains(item))
                    {
                        _suitableTours.Add(item);
                    }
                }
            }
        }

        public static void FillSuitableToursHotelDiet()
        {
            string selectedLanguage = ApplicationParameters.selectedLanguage;
            _suitableTours.Clear();

            foreach (var hotel in _selectedHotels)
            {
                foreach (var diet in _selectedTypesOfDiets)
                {
                    List<SuitableTour> tmp = TourSelectionSQLHelpers.SuitableToursHotelDiet(selectedLanguage, _selectedDepartureFromDate, _selectedDepartureToDate, hotel, _selectedDepartureCity, _selectedNightsFrom, _selectedNightsTo, _selectedPeopleAmount, diet);

                    foreach (var item in tmp)
                    {
                        if (!_suitableTours.Contains(item))
                        {
                            _suitableTours.Add(item);
                        }
                    }
                }
            }
        }
    }
}
