﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TouristCompany.Common
{
    public static class RequestSQLHelpers
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["TouristCompanyDbContext"].ConnectionString;

        /// <summary>
        /// Get user email by his ID.
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public static string UserEmailByUserId(int userId)
        {
            string email = "";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_user_email_by_user_id", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@userId";
                param.SqlDbType = SqlDbType.Int;
                param.Value = userId;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        email = dr.GetString(0);
                    }
                }
            }

            return email;
        }

        /// <summary>
        /// Get names of all the services from DB.
        /// </summary>
        /// <returns></returns>
        public static List<string> AllServicesNames(string lang)
        {
            List<string> _services = new List<string>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_service_names_all", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string name = dr.GetString(0);
                        _services.Add(name);
                    }
                }
            }
            return _services;
        }

        /// <summary>
        /// Get info about tour by id from DB.
        /// </summary>
        /// <returns></returns>
        public static TourForRequest TourInfoById(string lang, int tourId)
        {
            TourForRequest _tour = new TourForRequest();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_tour_for_request", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@tourId";
                param.SqlDbType = SqlDbType.Int;
                param.Value = tourId;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        _tour.Tour = dr.GetString(0);
                        _tour.DepartureDate = dr.GetDateTime(1).ToShortDateString();
                        _tour.Resort = dr.GetString(2) + " -- " + dr.GetString(3);
                        _tour.Hotel = dr.GetString(4).ToUpper() + " " + dr.GetInt32(5).ToString() + "*";
                        _tour.Transport = dr.GetString(6) + " -- " + dr.GetString(7);
                        _tour.People = dr.GetInt32(8);
                        _tour.Nights = dr.GetInt32(9);
                        _tour.Price = dr.GetSqlMoney(10).ToString();
                    }
                }
            }

            return _tour;
        }

        /// <summary>
        /// Get service price by name from DB.
        /// </summary>
        /// <returns></returns>
        public static string ServicePriceByName(string lang, string name)
        {
            string price = "";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_service_price_by_name", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@serviceName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = name;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        price = dr.GetSqlMoney(0).ToString();
                    }
                }
            }

            return price;
        }


        /// <summary>
        /// Insert new request to DB.
        /// </summary>
        /// <returns></returns>
        public static void InsertRequest(DateTime date, int tourId, int clientId)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_insert_request", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@date";
                param.SqlDbType = SqlDbType.Date;
                param.Value = date;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@tourId";
                param.SqlDbType = SqlDbType.Int;
                param.Value = tourId;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@clientId";
                param.SqlDbType = SqlDbType.Int;
                param.Value = clientId;
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();

                connection.Close();
            }
        }

        /// <summary>
        /// Insert services for the new request to DB.
        /// </summary>
        /// <returns></returns>
        public static void InsertServicesForRequest(string lang, string serviceName)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_insert_services_for_request", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 10;
                param.Value = lang;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@serviceName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = serviceName;
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();

                connection.Close();
            }
        }
    }
}
