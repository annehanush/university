﻿using System;

namespace TouristCompany.Common
{
    public static class UserActions
    {
        public static string _username = "";
        public static int _userId = -1;
        public static string _userRole = "";

        public static string _enteredUsername = "";
        public static string _enteredFullName = "";
        public static string _enteredEmail = "";
        public static string _enteredPhone = "";
        public static string _enteredPassword = "";

        public static string _loginEnteredUsername = "";
        public static string _loginEnteredPassword = "";

        /// <summary>
        /// Clear all sign up data fileds.
        /// </summary>
        public static void ClearSignUpData()
        {
            _enteredUsername = "";
            _enteredFullName = "";
            _enteredEmail = "";
            _enteredPhone = "";
            _enteredPassword = "";
        }

        /// <summary>
        /// Check if there the same user name exists in system.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static bool CheckUsernameExistance(string userName)
        {
            bool isFree = true;

            foreach (var name in UserSQLHelpers.AllExistedUsernames())
            {
                if (userName == name)
                {
                    isFree = false;
                    break;
                }
            }

            return isFree;
        }

        /// <summary>
        /// Check if all the parameters were entered correctly.
        /// </summary>
        /// <returns></returns>
        public static bool CheckEnteredParameters()
        {
            bool isReadyForRegistration = false;

            if (_enteredUsername != "" && _enteredPassword != "" && _enteredEmail != "")
            {
                isReadyForRegistration = true;
            }

            return isReadyForRegistration;
        }

        /// <summary>
        /// New user registration.
        /// </summary>
        public static void Register()
        {
            UserSQLHelpers.Registration(_enteredUsername, _enteredFullName, _enteredEmail, _enteredPhone, _enteredPassword);
        }

        /// <summary>
        /// Log in system.
        /// </summary>
        /// <returns></returns>
        public static bool Login()
        {
            bool isName = false;  
            bool login = false;

            foreach (var name in UserSQLHelpers.AllExistedUsernames())
            {
                if (_loginEnteredUsername == name)
                {
                    isName = true;
                    break;
                }
            }

            if (isName)
            {
                string password = UserSQLHelpers.PasswordByUsername(_loginEnteredUsername);
                if (_loginEnteredPassword == password)
                {
                    login = true;
                    _username = _loginEnteredUsername;
                    _userId = UserSQLHelpers.UserIdByUsername(_username);
                    GetUserRole();
                }
            }

            return login;
        }

        /// <summary>
        /// Get role of current user.
        /// </summary>
        public static void GetUserRole()
        {
            if (_userId != -1)
            {
                _userRole = UserSQLHelpers.UserRoleByUserId(_userId);
            }
        }

        /// <summary>
        /// Check if role of current user is "User".
        /// </summary>
        /// <returns></returns>
        public static bool CheckIfTheUserIsInRoleUser()
        {
            bool isUser = false;

            if (_userRole == "user")
            {
                isUser = true;
            }

            return isUser;
        }

        /// <summary>
        /// Get info about current user.
        /// </summary>
        /// <returns></returns>
        public static CurrentUser GetCurrentUserInfo()
        {
            CurrentUser _user = UserSQLHelpers.UserInfoByUserId(_userId);
            return _user;
        }

        /// <summary>
        /// Change current user Username.
        /// </summary>
        public static bool UpdateUsername(string newName)
        {
            bool correct = false;

            bool sameName = false;
            foreach (var name in UserSQLHelpers.AllExistedUsernames())
            {
                if (newName == name)
                {
                    sameName = true;
                    break;
                }
            }

            if (!sameName)
            {
                UserSQLHelpers.UpdateUserUsername(_userId, newName);
                _username = newName;
                correct = true;
            }

            return correct;
        }

        /// <summary>
        /// Change current user Full name.
        /// </summary>
        public static void UpdateFullName(string newName)
        {
            UserSQLHelpers.UpdateUserFullName(_userId, newName);
        }

        /// <summary>
        /// Change current user Date of birth.
        /// </summary>
        public static void UpdateDateOfBirth(DateTime date)
        {
            UserSQLHelpers.UpdateUserDateOfBirth(_userId, date);
        }

        /// <summary>
        /// Change current user Date of birth to NULL.
        /// </summary>
        public static void UpdateDateOfBirthToNull()
        {
            UserSQLHelpers.UpdateUserDateOfBirthToNull(_userId);
        }

        /// <summary>
        /// Change current user Email.
        /// </summary>
        public static void UpdateEmail(string email)
        {
            UserSQLHelpers.UpdateUserEmail(_userId, email);
        }

        /// <summary>
        /// Change current user Phone number.
        /// </summary>
        public static void UpdatePhoneNumber(string phone)
        {
            UserSQLHelpers.UpdateUserPhoneNumber(_userId, phone);
        }

        /// <summary>
        /// Change current user Password.
        /// </summary>
        public static void UpdatePassword(string password)
        {
            UserSQLHelpers.UpdateUserPassword(_userId, password);
        }

        /// <summary>
        /// Delete current user.
        /// </summary>
        public static void DeleteUser()
        {
            UserSQLHelpers.DeleteUser(_userId);
        }
    }
}
