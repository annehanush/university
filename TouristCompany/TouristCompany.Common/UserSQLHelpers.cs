﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace TouristCompany.Common
{
    public static class UserSQLHelpers
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["TouristCompanyDbContext"].ConnectionString;

        /// <summary>
        /// Get all existed usernames.
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public static List<string> AllExistedUsernames()
        {
            List<string> _names = new List<string>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_user_all_usernames", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string name = dr.GetString(0);
                        _names.Add(name);
                    }
                }
            }
            return _names;
        }

        /// <summary>
        /// Register.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="fullName"></param>
        /// <param name="email"></param>
        /// <param name="phone"></param>
        /// <param name="password"></param>
        public static void Registration(string username, string fullName, string email, string phone, string password)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_insert_reigistration", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@username";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = username;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@fullName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 180;
                if (fullName != "")
                {
                    param.Value = fullName;
                }
                else
                {
                    param.Value = DBNull.Value;
                }
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@email";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 256;
                param.Value = email;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@phone";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                if (phone != "")
                {
                    param.Value = phone;
                }
                else
                {
                    param.Value = DBNull.Value;
                }
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@password";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = password;
                sqlCmd.Parameters.Add(param);


                sqlCmd.ExecuteNonQuery();

                connection.Close();
            }
        }

        /// <summary>
        /// Get username password.
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public static string PasswordByUsername(string username)
        {
            string password = "";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_user_password_by_username", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@username";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = username;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        password = dr.GetString(0);
                    }
                }
            }

            return password;
        }

        /// <summary>
        /// Get username Id.
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public static int UserIdByUsername(string username)
        {
            int userId = -1;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_user_id_by_username", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@username";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = username;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        userId = dr.GetInt32(0);
                    }
                }
            }

            return userId;
        }

        /// <summary>
        /// Get user role by his ID.
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public static string UserRoleByUserId(int userId)
        {
            string role = "";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_user_role_name_by_user_id", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@userId";
                param.SqlDbType = SqlDbType.Int;
                param.Value = userId;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        role = dr.GetString(0);
                    }
                }
            }

            return role;
        }

        /// <summary>
        /// Get user info by his ID.
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public static CurrentUser UserInfoByUserId(int userId)
        {
            CurrentUser _user = new CurrentUser();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_user_info", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@userId";
                param.SqlDbType = SqlDbType.Int;
                param.Value = userId;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        _user.Username = dr.GetString(0);
                        try
                        {
                            _user.FullName = dr.GetString(1);
                        }
                        catch (SqlNullValueException)
                        {
                            _user.FullName = "";
                        }
                        try
                        {
                            _user.DateOfBirth = dr.GetDateTime(2).ToShortDateString();
                        }
                        catch (SqlNullValueException)
                        {
                            _user.DateOfBirth = "";
                        }
                        _user.Email = dr.GetString(3);
                        try
                        {
                            _user.PhoneNumber = dr.GetString(4);
                        }
                        catch (SqlNullValueException)
                        {
                            _user.PhoneNumber = "";
                        }
                    }
                }
            }

            return _user;
        }

        /// <summary>
        /// Update current user Username.
        /// </summary>
        /// <returns></returns>
        public static void UpdateUserUsername(int userId, string newUsername)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_update_user_username", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@userId";
                param.SqlDbType = SqlDbType.Int;
                param.Value = userId;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@newUserName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = newUsername;
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();

                connection.Close();
            }
        }

        /// <summary>
        /// Update current user Full name.
        /// </summary>
        /// <returns></returns>
        public static void UpdateUserFullName(int userId, string newFullName)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_update_user_full_name", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@userId";
                param.SqlDbType = SqlDbType.Int;
                param.Value = userId;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@newFullName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = newFullName;
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();

                connection.Close();
            }
        }

        /// <summary>
        /// Update current user Date of birth.
        /// </summary>
        /// <returns></returns>
        public static void UpdateUserDateOfBirth(int userId, DateTime date)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_update_user_date_of_birth", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@userId";
                param.SqlDbType = SqlDbType.Int;
                param.Value = userId;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@date";
                param.SqlDbType = SqlDbType.Date;
                param.Value = date;
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();

                connection.Close();
            }
        }

        /// <summary>
        /// Update current user Date of birth to NULL.
        /// </summary>
        /// <returns></returns>
        public static void UpdateUserDateOfBirthToNull(int userId)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_update_user_date_of_birth_to_null", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@userId";
                param.SqlDbType = SqlDbType.Int;
                param.Value = userId;
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();

                connection.Close();
            }
        }

        /// <summary>
        /// Update current user Email.
        /// </summary>
        /// <returns></returns>
        public static void UpdateUserEmail(int userId, string email)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_update_user_email", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@userId";
                param.SqlDbType = SqlDbType.Int;
                param.Value = userId;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@email";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 256;
                param.Value = email;
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();

                connection.Close();
            }
        }

        /// <summary>
        /// Update current user Phone number.
        /// </summary>
        /// <returns></returns>
        public static void UpdateUserPhoneNumber(int userId, string phone)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_update_user_phone_number", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@userId";
                param.SqlDbType = SqlDbType.Int;
                param.Value = userId;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@phone";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 80;
                param.Value = phone;
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();

                connection.Close();
            }
        }

        /// <summary>
        /// Update current user Password.
        /// </summary>
        /// <returns></returns>
        public static void UpdateUserPassword(int userId, string password)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_update_user_password", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@userId";
                param.SqlDbType = SqlDbType.Int;
                param.Value = userId;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@password";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = password;
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();

                connection.Close();
            }
        }

        /// <summary>
        /// Delete current user.
        /// </summary>
        /// <returns></returns>
        public static void DeleteUser(int userId)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_delete_user", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@userId";
                param.SqlDbType = SqlDbType.Int;
                param.Value = userId;
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();

                connection.Close();
            }
        }
    }
}
