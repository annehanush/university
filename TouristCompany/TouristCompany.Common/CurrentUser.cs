﻿namespace TouristCompany.Common
{
    public class CurrentUser
    {
        public string Username { get; set; }
        public string FullName { get; set; }
        public string DateOfBirth { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
