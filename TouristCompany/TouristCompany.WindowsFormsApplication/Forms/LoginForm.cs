﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TouristCompany.Common;

namespace TouristCompany.WindowsFormsApplication.Forms
{
    public partial class LoginForm : Form
    {
        // language of the programm
        public string selectedLanguage = ApplicationParameters.selectedLanguage;

        public LoginForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Check of what language of the program was chose.
        /// </summary>
        private void CheckLanguage()
        {
            if (selectedLanguage == "ru")
            {
                countriesToolStripMenuItem.Text = "Страны";
                tourSelectionToolStripMenuItem.Text = "Подбор тура";
                contactToolStripMenuItem.Text = "Контакты";
                fAQToolStripMenuItem.Text = "Ч.З.В";

                captionLabel.Text = "ВХОД";
                usernameLabel.Text = "Имя пользователя";
                usernameLabel.Location = new Point(304, 130);
                passwordLabel.Text = "Пароль";
                passwordLabel.Location = new Point(347, 198);
                loginButton.Text = "Войти";
                registrationButton.Text = "Регистрация";
            }

        }

        /// <summary>
        /// Form resize operations.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoginForm_Resize(object sender, EventArgs e)
        {
            if (this.Width > 781)
            {
                countriesToolStripMenuItem.Font = new Font("Segoe UI", 13);
                tourSelectionToolStripMenuItem.Font = new Font("Segoe UI", 13);
                contactToolStripMenuItem.Font = new Font("Segoe UI", 13);
                fAQToolStripMenuItem.Font = new Font("Segoe UI", 13);

                paradiseToolStripMenuItem.Font = new Font("Segoe UI", 22, FontStyle.Bold);
                countriesToolStripMenuItem.Margin = new Padding(790, 0, 0, 0);

                loginButton.Size = new Size(82, 27);
                registrationButton.Size = new Size(117, 27);
            }
            else
            {
                countriesToolStripMenuItem.Font = new Font("Segoe UI", 10);
                tourSelectionToolStripMenuItem.Font = new Font("Segoe UI", 10);
                contactToolStripMenuItem.Font = new Font("Segoe UI", 10);
                fAQToolStripMenuItem.Font = new Font("Segoe UI", 10);

                paradiseToolStripMenuItem.Font = new Font("Segoe UI", 14, FontStyle.Bold);
                countriesToolStripMenuItem.Margin = new Padding(330, 0, 0, 0);

                loginButton.Size = new Size(82, 27);
                registrationButton.Size = new Size(117, 27);
            }
        }

        /// <summary>
        /// Countries menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void countriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
            CountriesForm _countriesForm = new CountriesForm();
            _countriesForm.Show();
        }

        /// <summary>
        /// Tour selection menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tourSelectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
            TourSelectionForm _tourSelectionForm = new TourSelectionForm();
            _tourSelectionForm.Show();
        }

        /// <summary>
        /// Paradise menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void paradiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
        }

        /// <summary>
        /// Registration button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void registrationButton_Click(object sender, EventArgs e)
        {
            this.Close();
            bool opened = false;
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name == "SignUpForm")
                {
                    opened = true;
                    MessageBox.Show("Sign up window is already opened.");
                }
            }

            if (!opened)
            {
                SignUpForm _signUpForm = new SignUpForm();
                _signUpForm.Show();
            }
        }

        /// <summary>
        /// Form load.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoginForm_Load(object sender, EventArgs e)
        {
            CheckLanguage();
            passwordTextBox.PasswordChar = '*';
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            bool readyToLogin = false;

            if (usernameTextBox.Text != "")
            {
                UserActions._loginEnteredUsername = usernameTextBox.Text;
                readyToLogin = true;
            }
            else
            {
                readyToLogin = false;
                if (selectedLanguage == "en")
                {
                    MessageBox.Show("You must enter username!", "Login process", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Введите имя пользователя!", "Вход", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if (passwordTextBox.Text != "")
            {
                readyToLogin = true;
                UserActions._loginEnteredPassword = passwordTextBox.Text;
            }
            else
            {
                readyToLogin = false;
                if (selectedLanguage == "en")
                {
                    MessageBox.Show("You must enter password!", "Login process", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Введите пароль!", "Вход", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if (readyToLogin)
            {
                bool login = UserActions.Login();
                if (login)
                {
                    if (selectedLanguage == "en")
                    {
                        MessageBox.Show("You were logged in system.\nYou will be redirected to the main page.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Вы вошли в систему.\nВы будете перенаправлены на главную страницу приложения.", "Вход", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
                    {
                        if (Application.OpenForms[i].Name == "MainForm")
                        {
                            var mainForm = Application.OpenForms.OfType<MainForm>().Single();
                            mainForm.ChangeMenu();
                        }
                        else
                        {
                            Application.OpenForms[i].Close();
                        }
                    }
                }
                else
                {
                    if (selectedLanguage == "en")
                    {
                        MessageBox.Show("Incorrect input data.", "Login process", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Вы ввели неверные данные.", "Вход", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
