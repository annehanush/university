﻿using System;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using TouristCompany.Common;

namespace TouristCompany.WindowsFormsApplication.Forms
{
    public partial class TourSelectionForm : Form
    {
        // language of the programm
        public string selectedLanguage = ApplicationParameters.selectedLanguage;
        public static bool isSuit = false;

        public TourSelectionForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Menu changing if user is in system.
        /// </summary>
        public void ChangeMenu()
        {
            if (UserActions._username != "" && UserActions._userId != -1)
            {
                signUpToolStripMenuItem.Visible = false;
                logInToolStripMenuItem.Visible = false;
                profileToolStripMenuItem.Visible = true;
                profileToolStripMenuItem.Margin = new Padding(100, 0, 0, 0);
            }

        }

        /// <summary>
        /// Check of the language of the programm.
        /// </summary>
        private void CheckLanguage()
        {
            if (selectedLanguage == "ru")
            {
                countriesToolStripMenuItem.Text = "Страны";
                tourSelectionToolStripMenuItem.Text = "Подбор тура";
                contactToolStripMenuItem.Text = "Контакты";
                fAQToolStripMenuItem.Text = "Ч.З.В";

                profileToolStripMenuItem.Text = "Профиль";
                profileInfoToolStripMenuItem.Text = "О профиле";
                logOutToolStripMenuItem.Text = "Выйти";

                signUpToolStripMenuItem.Text = "Регистрация";
                if (this.Width > 781)
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 11);
                }
                else
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 8);
                }
                logInToolStripMenuItem.Text = "Вход";

                captionLabel.Text = "ПОДБОР ТУРА";

                departureCityLabel.Text = "Город\nотъезда";
                countryLabel.Text = "Страна";
                tourLabel.Text = "Тур";
                departureFromLabel.Text = "Отъезд\nс";
                departureToLabel.Text = "до";
                nightsFromLabel.Text = "Ночей\nот";
                nightsToLabel.Text = "до";
                currencyLabel.Text = "Валюта";
                peopleLabel.Text = "Человек";
                resortLabel.Text = "Курорты";
                hotelLabel.Text = "Отели";
                categoryLabel.Text = "Категории";
                dietLabel.Text = "Питание";

                findButton.Text = "Поиск";
            }
        }

        /// <summary>
        /// Form resize.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TourSelectionForm_Resize(object sender, EventArgs e)
        {
            /*if (this.Width > 781)
            {
                countriesToolStripMenuItem.Font = new Font("Segoe UI", 13);
                tourSelectionToolStripMenuItem.Font = new Font("Segoe UI", 13);
                contactToolStripMenuItem.Font = new Font("Segoe UI", 13);
                fAQToolStripMenuItem.Font = new Font("Segoe UI", 13);
                if (selectedLanguage == "ru")
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 11);
                }
                else
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 13);
                }
                logInToolStripMenuItem.Font = new Font("Segoe UI", 13);

                paradiseToolStripMenuItem.Font = new Font("Segoe UI", 22, FontStyle.Bold);
                countriesToolStripMenuItem.Margin = new Padding(500, 0, 0, 0);
                signUpToolStripMenuItem.Margin = new Padding(95, 0, 0, 0);

                departureCityLabel.Location = new Point(331, 132);
                departureCityComboBox.Location = new Point(407, 132);
                tourLabel.Location = new Point(720, 132);
                tourComboBox.Location = new Point(796, 132);
                countryLabel.Location = new Point(331, 179);
                countryComboBox.Location = new Point(407, 179);
                line1.Location = new Point(334, 218);
                departureFromLabel.Location = new Point(331, 237);
                departureFromDateTimePicker.Location = new Point(407, 237);
                nightsFromLabel.Location = new Point(605, 237);
                nightsFromComboBox.Location = new Point(660, 237);
                currencyLabel.Location = new Point(793, 237);
                currencyComboBox.Location = new Point(861, 237);
                departureToLabel.Location = new Point(331, 285);
                departureToDateTimePicker.Location = new Point(407, 285);
                nightsToLabel.Location = new Point(605, 285);
                nightsToComboBox.Location = new Point(660, 285);
                peopleLabel.Location = new Point(793, 285);
                peopleComboBox.Location = new Point(861, 285);
                line2.Location = new Point(335, 325);
                resortLabel.Location = new Point(332, 342);
                categoryLabel.Location = new Point(516, 342);
                hotelLabel.Location = new Point(617, 342);
                dietLabel.Location = new Point(869, 342);
                resortPanel.Location = new Point(335, 366);
                categoryPanel.Location = new Point(519, 366);
                hotelPanel.Location = new Point(620, 366);
                dietPanel.Location = new Point(872, 366);
                findButton.Location = new Point(924, 547);

                suitableTousLabel.Location = new Point(617, 592);
                suitableToursDataGridView.Location = new Point(334, 615);
            }
            else
            {
                countriesToolStripMenuItem.Font = new Font("Segoe UI", 10);
                tourSelectionToolStripMenuItem.Font = new Font("Segoe UI", 10);
                contactToolStripMenuItem.Font = new Font("Segoe UI", 10);
                fAQToolStripMenuItem.Font = new Font("Segoe UI", 10);
                if (selectedLanguage == "ru")
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 8);
                }
                else
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 10);
                }
                logInToolStripMenuItem.Font = new Font("Segoe UI", 10);

                paradiseToolStripMenuItem.Font = new Font("Segoe UI", 14, FontStyle.Bold);
                countriesToolStripMenuItem.Margin = new Padding(130, 0, 0, 0);
                signUpToolStripMenuItem.Margin = new Padding(35, 0, 0, 0);

                if (suitableTousLabel.Visible == true)
                {
                    departureCityLabel.Location = new Point(31, 132);
                    departureCityComboBox.Location = new Point(107, 132);
                    tourLabel.Location = new Point(420, 132);
                    tourComboBox.Location = new Point(496, 132);
                    countryLabel.Location = new Point(31, 179);
                    countryComboBox.Location = new Point(107, 179);
                    line1.Location = new Point(34, 218);
                    departureFromLabel.Location = new Point(31, 237);
                    departureFromDateTimePicker.Location = new Point(107, 237);
                    nightsFromLabel.Location = new Point(305, 237);
                    nightsFromComboBox.Location = new Point(360, 237);
                    currencyLabel.Location = new Point(493, 237);
                    currencyComboBox.Location = new Point(561, 237);
                    departureToLabel.Location = new Point(31, 285);
                    departureToDateTimePicker.Location = new Point(107, 285);
                    nightsToLabel.Location = new Point(305, 285);
                    nightsToComboBox.Location = new Point(360, 285);
                    peopleLabel.Location = new Point(493, 285);
                    peopleComboBox.Location = new Point(561, 285);
                    line2.Location = new Point(35, 325);
                    resortLabel.Location = new Point(32, 342);
                    categoryLabel.Location = new Point(216, 342);
                    hotelLabel.Location = new Point(317, 342);
                    dietLabel.Location = new Point(569, 342);
                    resortPanel.Location = new Point(35, 366);
                    categoryPanel.Location = new Point(219, 366);
                    hotelPanel.Location = new Point(320, 366);
                    dietPanel.Location = new Point(572, 366);
                    findButton.Location = new Point(624, 547);

                    suitableTousLabel.Location = new Point(317, 592);
                    suitableToursDataGridView.Location = new Point(34, 615);
                }
                else
                {
                    departureCityLabel.Location = new Point(31, 132);
                    departureCityComboBox.Location = new Point(107, 132);
                    tourLabel.Location = new Point(420, 132);
                    tourComboBox.Location = new Point(496, 132);
                    countryLabel.Location = new Point(31, 179);
                    countryComboBox.Location = new Point(107, 179);
                    line1.Location = new Point(34, 218);
                    departureFromLabel.Location = new Point(31, 237);
                    departureFromDateTimePicker.Location = new Point(107, 237);
                    nightsFromLabel.Location = new Point(305, 237);
                    nightsFromComboBox.Location = new Point(360, 237);
                    currencyLabel.Location = new Point(493, 237);
                    currencyComboBox.Location = new Point(561, 237);
                    departureToLabel.Location = new Point(31, 285);
                    departureToDateTimePicker.Location = new Point(107, 285);
                    nightsToLabel.Location = new Point(305, 285);
                    nightsToComboBox.Location = new Point(360, 285);
                    peopleLabel.Location = new Point(493, 285);
                    peopleComboBox.Location = new Point(561, 285);
                    line2.Location = new Point(35, 325);
                    resortLabel.Location = new Point(32, 342);
                    categoryLabel.Location = new Point(216, 342);
                    hotelLabel.Location = new Point(317, 342);
                    dietLabel.Location = new Point(569, 342);
                    resortPanel.Location = new Point(35, 366);
                    categoryPanel.Location = new Point(219, 366);
                    hotelPanel.Location = new Point(320, 366);
                    dietPanel.Location = new Point(572, 366);
                    findButton.Location = new Point(624, 547);

                    suitableTousLabel.Location = new Point(317, 405);
                    suitableToursDataGridView.Location = new Point(34, 430);
                }
            }*/
        }

        /// <summary>
        /// Paradise menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void paradiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
        }

        /// <summary>
        /// Countries menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void countriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
            CountriesForm _countriesForm = new CountriesForm();
            _countriesForm.Show();
        }
     
        /// <summary>
        /// Tour selection menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tourSelectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
            TourSelectionForm _tourSelectionForm = new TourSelectionForm();
            _tourSelectionForm.Show();
        }

        /// <summary>
        /// Country comboBox filling.
        /// </summary>
        private void FillCountries()
        {
            TourSelectionActions.FillCountries();
            countryComboBox.Text = TourSelectionActions._selectedCountry;
            foreach (var country in TourSelectionActions._countries)
            {
                countryComboBox.Items.Add(country);
            }
        }

        /// <summary>
        /// Departure city comboBox filling.
        /// </summary>
        private void FillDepartureCities()
        {
            TourSelectionActions.FillDepartureCities();
            departureCityComboBox.Text = TourSelectionActions._selectedDepartureCity;
            foreach (var departureCity in TourSelectionActions._departureCities)
            {
                departureCityComboBox.Items.Add(departureCity);
            }
        }

        /// <summary>
        /// Currency comboBox filling.
        /// </summary>
        private void FillCurrency()
        {
            currencyComboBox.Text = "USD";
            currencyComboBox.Items.Add("BYN");
            currencyComboBox.Items.Add("USD");
        }

        /// <summary>
        /// Category checkBox panel filling.
        /// </summary>
        private void FillCategory()
        {
            for (int i = 2; i < 6; i++)
            {
                CheckBox category = new CheckBox();
                category.CheckedChanged += category_CheckedChanged;
                category.Text = i + "*";
                category.Location = new Point(4, 23 * (i - 2) + 2);
                category.Size = new Size(50, 21);

                categoryPanel.Controls.Add(category);
            }
        }

        /// <summary>
        /// Diets checkBox panel filling.
        /// </summary>
        private void FillDiets()
        {
            TourSelectionActions.FillTypesOfDiets();
            int countDiets = 0;
            foreach (var _diet in TourSelectionActions._typesOfDiets)
            {
                CheckBox diet = new CheckBox();
                diet.CheckedChanged += diet_CheckedChanged;
                diet.Text = _diet;
                diet.Location = new Point(4, 23 * countDiets + 2);

                dietPanel.Controls.Add(diet);
                countDiets++;
            }
        }

        /// <summary>
        /// Resorts checkBox panel filling.
        /// </summary>
        private void FillResorts()
        {
            TourSelectionActions.FillResorts();
            int countResorts = 0;
            foreach (var _reosrt in TourSelectionActions._reosrts)
            {
                CheckBox resort = new CheckBox();
                resort.CheckedChanged += resort_CheckedChanged;
                resort.Text = _reosrt;
                resort.Location = new Point(4, 23 * countResorts + 2);
                resort.Size = new Size(140, 21);

                resortPanel.Controls.Add(resort);
                countResorts++;
            }
        }

        /// <summary>
        /// Hotels checkBox panel filling.
        /// </summary>
        private void FillHotels()
        {
            TourSelectionActions.FillHotels();
            int countHotels = 0;
            foreach (var _hotel in TourSelectionActions._hotels)
            {
                CheckBox hotel = new CheckBox();
                hotel.CheckedChanged += hotel_CheckedChanged;
                string label = _hotel.Item1.ToUpper() + " " + _hotel.Item2 + "*";
                hotel.Location = new Point(4, 23 * countHotels + 2);
                hotel.Text = label;
                hotel.Size = new Size(300, 21);

                hotelPanel.Controls.Add(hotel);
                countHotels++;
            }
        }

        /// <summary>
        /// Tour comboBox filling.
        /// </summary>
        private void FillToursNames()
        {
            TourSelectionActions.FillTours();
            tourComboBox.Text = TourSelectionActions._selectedTourName;
            foreach (var tour in TourSelectionActions._toursNames)
            {
                tourComboBox.Items.Add(tour);
            }
        } 

        /// <summary>
        /// Nights from and to comboBoxes filling.
        /// </summary>
        private void FillNights()
        {
            TourSelectionActions.FillNights();
            if (TourSelectionActions._selectedNightsFrom == -1)
            {
                nightsFromComboBox.Text = "";
            }
            else
            {
                nightsFromComboBox.Text = TourSelectionActions._selectedNightsFrom.ToString();
            }
            if (TourSelectionActions._selectedNightsTo == -1)
            {
                nightsToComboBox.Text = "";
            }
            else
            {
                nightsToComboBox.Text = TourSelectionActions._selectedNightsTo.ToString();
            }
            foreach (var amount in TourSelectionActions._nightsFrom)
            {
                nightsFromComboBox.Items.Add(amount);
            }
            foreach (var amount in TourSelectionActions._nightsTo)
            {
                nightsToComboBox.Items.Add(amount);
            }
        }

        /// <summary>
        /// People comboBox filling.
        /// </summary>
        private void FillPeople()
        {
            TourSelectionActions.FillPeople();
            if (TourSelectionActions._selectedPeopleAmount == -1)
            {
                peopleComboBox.Text = "";
            }
            else
            {
                peopleComboBox.Text = TourSelectionActions._selectedPeopleAmount.ToString();
            }
            foreach (var amount in TourSelectionActions._peopleAmount)
            {
                peopleComboBox.Items.Add(amount);
            }
        }

        /// <summary>
        /// Hotels checkBox panel filling by resorts.
        /// </summary>
        private void FillHotelsByResorts()
        {
            TourSelectionActions.FillHotelsByResorts();
            int countHotels = 0;
            foreach (var _hotel in TourSelectionActions._hotels)
            {
                CheckBox hotel = new CheckBox();
                hotel.CheckedChanged += hotel_CheckedChanged;
                string label = _hotel.Item1.ToUpper() + " " + _hotel.Item2 + "*";
                hotel.Location = new Point(4, 23 * countHotels + 2);
                hotel.Text = label;
                hotel.Size = new Size(300, 21);

                hotelPanel.Controls.Add(hotel);
                countHotels++;
            }
        }

        /// <summary>
        /// Hotels checkBox panel filling by category.
        /// </summary>
        private void FillHotelsByCategory()
        {
            TourSelectionActions.FillHotelsByCategory();
            int countHotels = 0;
            foreach (var _hotel in TourSelectionActions._hotels)
            {
                CheckBox hotel = new CheckBox();
                hotel.CheckedChanged += hotel_CheckedChanged;
                string label = _hotel.Item1.ToUpper() + " " + _hotel.Item2 + "*";
                hotel.Location = new Point(4, 23 * countHotels + 2);
                hotel.Text = label;
                hotel.Size = new Size(300, 21);

                hotelPanel.Controls.Add(hotel);
                countHotels++;
            }
        }

        /// <summary>
        /// Hotels checkBox panel filling by resorts and category.
        /// </summary>
        private void FillHotelsByResortsAndCategory()
        {
            TourSelectionActions.FillHotelsByCategoryAndResort();
            int countHotels = 0;
            foreach (var _hotel in TourSelectionActions._hotels)
            {
                CheckBox hotel = new CheckBox();
                hotel.CheckedChanged += hotel_CheckedChanged;
                string label = _hotel.Item1.ToUpper() + " " + _hotel.Item2 + "*";
                hotel.Location = new Point(4, 23 * countHotels + 2);
                hotel.Text = label;
                hotel.Size = new Size(300, 21);

                hotelPanel.Controls.Add(hotel);
                countHotels++;
            }
        }

        /// <summary>
        /// Form load.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TourSelectionForm_Load(object sender, EventArgs e)
        {
            this.Size = new Size(781, 425);
            CheckLanguage();
            ChangeMenu();

            FillCountries();
            FillDepartureCities();
            FillCurrency();
            FillCategory();
            FillDiets();
            FillResorts();
            FillHotels();
            FillToursNames();
            FillNights();
            FillPeople();

            TourSelectionActions._selectedDepartureFromDate = DateTime.Now;
            TourSelectionActions._selectedDepartureToDate = DateTime.Now;

            TourSelectionActions._suitableTours.Clear();

            TourSelectionActions._requestTourId = -1;
        }

        /// <summary>
        /// Log in menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void logInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool opened = false;
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name == "LoginForm")
                {
                    opened = true;
                    if (selectedLanguage == "en")
                    {
                        MessageBox.Show("Login window is already opened.");
                    }
                    else
                    {
                        MessageBox.Show("Окно входа уже открыто.");
                    }
                }
            }

            if (!opened)
            {
                LoginForm _loginForm = new LoginForm();
                _loginForm.Show();
            }
        }

        /// <summary>
        /// Sign up menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void signUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool opened = false;
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name == "SignUpForm")
                {
                    opened = true;
                    if (selectedLanguage == "en")
                    {
                        MessageBox.Show("Sign up window is already opened.");
                    }
                    else
                    {
                        MessageBox.Show("Окно регистрации уже открыто.");
                    }
                }
            }

            if (!opened)
            {
                SignUpForm _signUpForm = new SignUpForm();
                _signUpForm.Show();
            }
        }

        /// <summary>
        /// No editeble country comboBox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void countryComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// No editeble departure city comboBox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void departureCityComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// No editeble tour comboBox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tourComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// No editeble nights from comboBox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nightsFromComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// nights to
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nightsToComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// No editeble currency comboBox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void currencyComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// No editeble people comboBox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void peopleComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// Departure city selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void departureCityComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            TourSelectionActions._selectedDepartureCity = departureCityComboBox.Text;

            TourSelectionActions._selectedTourName = "";
            tourComboBox.Items.Clear();
            TourSelectionActions._selectedNightsFrom = -1;
            nightsFromComboBox.Items.Clear();
            TourSelectionActions._selectedNightsTo = -1;
            nightsToComboBox.Items.Clear();
            TourSelectionActions._selectedPeopleAmount = -1;
            peopleComboBox.Items.Clear();
            TourSelectionActions._selectedHotels.Clear();
            TourSelectionActions._selectedResorts.Clear();

            TourSelectionActions._requestTourId = -1;

            FillToursNames();
            FillNights();
            FillPeople();

            if (TourSelectionActions._toursNames.Count == 0)
            {
                if (selectedLanguage == "en")
                {
                    MessageBox.Show("Impossible to select a tour in a given direction.\nTry to choose another departure city.");
                }
                else
                {
                    MessageBox.Show("Невозможно подобрать тур по заданному направлению.\nПопробуйте выбрать другой город отъезда.");
                }
            }
        }

        /// <summary>
        /// Country selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void countryComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            TourSelectionActions._selectedCountry = countryComboBox.Text;

            TourSelectionActions._selectedTourName = "";
            tourComboBox.Items.Clear();
            TourSelectionActions._selectedNightsFrom = -1;
            nightsFromComboBox.Items.Clear();
            TourSelectionActions._selectedNightsTo = -1;
            nightsToComboBox.Items.Clear();
            TourSelectionActions._hotels.Clear();
            hotelPanel.Controls.Clear();
            TourSelectionActions._selectedHotels.Clear();
            TourSelectionActions._selectedResorts.Clear();
            TourSelectionActions._selectedPeopleAmount = -1;
            peopleComboBox.Items.Clear();
            resortPanel.Controls.Clear();
            hotelPanel.Controls.Clear();

            TourSelectionActions._requestTourId = -1;

            FillToursNames();
            FillNights();
            FillPeople();
            FillResorts();
            FillHotels();

            if (TourSelectionActions._toursNames.Count == 0)
            {
                if (selectedLanguage == "en")
                {
                    MessageBox.Show("Impossible to select a tour in a given direction.\nTry to choose another country.");
                }
                else
                {
                    MessageBox.Show("Невозможно подобрать тур по заданному направлению.\nПопробуйте выбрать другую страну.");
                }
            }
        }

        /// <summary>
        /// Departure from date selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void departureFromDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            TourSelectionActions._selectedDepartureFromDate = departureFromDateTimePicker.Value;

            if (TourSelectionActions._selectedDepartureToDate < TourSelectionActions._selectedDepartureFromDate)
            {
                TourSelectionActions._selectedDepartureToDate = TourSelectionActions._selectedDepartureFromDate;
                departureToDateTimePicker.Value = departureFromDateTimePicker.Value;
            }
        }

        /// <summary>
        /// Departure to date selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void departureToDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            TourSelectionActions._selectedDepartureToDate = departureToDateTimePicker.Value;

            if (TourSelectionActions._selectedDepartureToDate < TourSelectionActions._selectedDepartureFromDate)
            {
                TourSelectionActions._selectedDepartureFromDate = TourSelectionActions._selectedDepartureToDate;
                departureFromDateTimePicker.Value = departureToDateTimePicker.Value;
            }
        }

        /// <summary>
        /// Nights from amount selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nightsFromComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            TourSelectionActions._selectedNightsFrom = Int32.Parse(nightsFromComboBox.Text);

            if (TourSelectionActions._selectedNightsTo < TourSelectionActions._selectedNightsFrom)
            {
                nightsToComboBox.Items.Clear();
                TourSelectionActions._selectedNightsTo = TourSelectionActions._selectedNightsFrom;
                nightsToComboBox.Text = TourSelectionActions._selectedNightsTo.ToString();
                foreach (var item in TourSelectionActions._nightsTo)
                {
                    if (item >= TourSelectionActions._selectedNightsFrom)
                    {
                        nightsToComboBox.Items.Add(item);
                    }
                }
            }
            else
            {
                nightsToComboBox.Items.Clear();
                nightsToComboBox.Text = TourSelectionActions._selectedNightsTo.ToString();
                foreach (var item in TourSelectionActions._nightsTo)
                {
                    if (item >= TourSelectionActions._selectedNightsFrom)
                    {
                        nightsToComboBox.Items.Add(item);
                    }
                }
            }
        }

        /// <summary>
        /// Nights to amount selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nightsToComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            TourSelectionActions._selectedNightsTo = Int32.Parse(nightsToComboBox.Text);
        }

        /// <summary>
        /// People amount selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void peopleComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            TourSelectionActions._selectedPeopleAmount = Int32.Parse(peopleComboBox.Text);
        }

        /// <summary>
        /// Currency selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void currencyComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            TourSelectionActions._selectedCurrency = currencyComboBox.Text;
        }

        /// <summary>
        /// Resort selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void resort_CheckedChanged(object sender, EventArgs e)
        {
            foreach (CheckBox resort in resortPanel.Controls)
            {
                if (resort.Checked)
                {
                    if (!TourSelectionActions._selectedResorts.Contains(resort.Text))
                    {
                        TourSelectionActions._selectedResorts.Add(resort.Text);
                    }
                }
                else
                {
                    if (TourSelectionActions._selectedResorts.Contains(resort.Text))
                    {
                        TourSelectionActions._selectedResorts.Remove(resort.Text);
                    }
                }
            }

            if (TourSelectionActions._selectedCategory.Count == 0 && TourSelectionActions._selectedResorts.Count > 0)
            {
                hotelPanel.Controls.Clear();
                FillHotelsByResorts();
            }
            else if (TourSelectionActions._selectedCategory.Count > 0 && TourSelectionActions._selectedResorts.Count == 0)
            {
                hotelPanel.Controls.Clear();
                FillHotelsByCategory();
            }
            else if (TourSelectionActions._selectedCategory.Count > 0 && TourSelectionActions._selectedResorts.Count > 0)
            {
                hotelPanel.Controls.Clear();
                FillHotelsByResortsAndCategory();
            }
            else if (TourSelectionActions._selectedCategory.Count == 0 && TourSelectionActions._selectedResorts.Count == 0)
            {
                hotelPanel.Controls.Clear();
                FillHotels();
            }
        }

        /// <summary>
        /// Category selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void category_CheckedChanged(object sender, EventArgs e)
        {
            Regex regex = new Regex(@"\d");
            foreach (CheckBox category in categoryPanel.Controls)
            {
                if (category.Checked)
                {
                    MatchCollection matches = regex.Matches(category.Text);
                    if (matches.Count > 0)
                    {
                        foreach (Match match in matches)
                        {
                            int num = Int32.Parse(match.Value);
                            if (!TourSelectionActions._selectedCategory.Contains(num))
                            {
                                TourSelectionActions._selectedCategory.Add(num);
                            }
                        }
                    }
                }
                else
                {
                    MatchCollection matches = regex.Matches(category.Text);
                    if (matches.Count > 0)
                    {
                        foreach (Match match in matches)
                        {
                            int num = Int32.Parse(match.Value);
                            if (TourSelectionActions._selectedCategory.Contains(num))
                            {
                                TourSelectionActions._selectedCategory.Remove(num);
                            }
                        }
                    }
                    
                }
            }

            if (TourSelectionActions._selectedCategory.Count == 0 && TourSelectionActions._selectedResorts.Count > 0)
            {
                hotelPanel.Controls.Clear();
                FillHotelsByResorts();
            }
            else if (TourSelectionActions._selectedCategory.Count > 0 && TourSelectionActions._selectedResorts.Count == 0)
            {
                hotelPanel.Controls.Clear();
                FillHotelsByCategory();
            }
            else if (TourSelectionActions._selectedCategory.Count > 0 && TourSelectionActions._selectedResorts.Count > 0)
            {
                hotelPanel.Controls.Clear();
                FillHotelsByResortsAndCategory();
            }
            else if (TourSelectionActions._selectedCategory.Count == 0 && TourSelectionActions._selectedResorts.Count == 0)
            {
                hotelPanel.Controls.Clear();
                FillHotels();
            }
        }

        /// <summary>
        /// Diet selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void diet_CheckedChanged(object sender, EventArgs e)
        {
            foreach (CheckBox diet in dietPanel.Controls)
            {
                if (diet.Checked)
                {
                    if (!TourSelectionActions._selectedTypesOfDiets.Contains(diet.Text))
                    {
                        TourSelectionActions._selectedTypesOfDiets.Add(diet.Text);
                    }
                }
                else
                {
                    if (TourSelectionActions._selectedTypesOfDiets.Contains(diet.Text))
                    {
                        TourSelectionActions._selectedTypesOfDiets.Remove(diet.Text);
                    }
                }
            }
        }

        /// <summary>
        /// Hotel selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void hotel_CheckedChanged(object sender, EventArgs e)
        {
            Regex regex = new Regex(@"\D*");
            foreach (CheckBox hotel in hotelPanel.Controls)
            {
                if (hotel.Checked)
                {
                    MatchCollection matches = regex.Matches(hotel.Text);
                    if (matches.Count > 0)
                    {
                        int count = 0;
                        foreach (Match match in matches)
                        {
                            if (count < 1)
                            {
                                string str = match.Value.Trim();
                                if (!TourSelectionActions._selectedHotels.Contains(str))
                                {
                                    TourSelectionActions._selectedHotels.Add(str);
                                }
                                count++;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
                else
                {
                    MatchCollection matches = regex.Matches(hotel.Text);
                    if (matches.Count > 0)
                    {
                        int count = 0;
                        foreach (Match match in matches)
                        {
                            if (count < 1)
                            {
                                string str = match.Value.Trim();
                                if (TourSelectionActions._selectedHotels.Contains(str))
                                {
                                    TourSelectionActions._selectedHotels.Remove(str);
                                }
                                count++;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }

                }
            }
        }

        /// <summary>
        /// Filling of suitable tours.
        /// </summary>
        private void FillSuitableTours()
        {
            // country
            if (TourSelectionActions._selectedResorts.Count == 0 && TourSelectionActions._selectedCategory.Count == 0 && TourSelectionActions._selectedHotels.Count == 0 && TourSelectionActions._selectedTypesOfDiets.Count == 0)
            {
                TourSelectionActions.FillSuitableToursCountry();
            }
            // country + diet
            else if (TourSelectionActions._selectedResorts.Count == 0 && TourSelectionActions._selectedCategory.Count == 0 && TourSelectionActions._selectedHotels.Count == 0 && TourSelectionActions._selectedTypesOfDiets.Count > 0)
            {
                TourSelectionActions.FillSuitableToursCountryDiet();
            }
            // resort
            else if (TourSelectionActions._selectedResorts.Count > 0 && TourSelectionActions._selectedCategory.Count == 0 && TourSelectionActions._selectedHotels.Count == 0 && TourSelectionActions._selectedTypesOfDiets.Count == 0)
            {
                TourSelectionActions.FillSuitableToursResort();
            }
            // resort + diet
            else if (TourSelectionActions._selectedResorts.Count > 0 && TourSelectionActions._selectedCategory.Count == 0 && TourSelectionActions._selectedHotels.Count == 0 && TourSelectionActions._selectedTypesOfDiets.Count > 0)
            {
                TourSelectionActions.FillSuitableToursResortDiet();
            }
            // category
            else if (TourSelectionActions._selectedResorts.Count == 0 && TourSelectionActions._selectedCategory.Count > 0 && TourSelectionActions._selectedHotels.Count == 0 && TourSelectionActions._selectedTypesOfDiets.Count == 0)
            {
                TourSelectionActions.FillSuitableToursCategory();
            }
            // category + diet
            else if (TourSelectionActions._selectedResorts.Count == 0 && TourSelectionActions._selectedCategory.Count > 0 && TourSelectionActions._selectedHotels.Count == 0 && TourSelectionActions._selectedTypesOfDiets.Count > 0)
            {
                TourSelectionActions.FillSuitableToursCategoryDiet();
            }
            // resort + category
            else if (TourSelectionActions._selectedResorts.Count > 0 && TourSelectionActions._selectedCategory.Count > 0 && TourSelectionActions._selectedHotels.Count == 0 && TourSelectionActions._selectedTypesOfDiets.Count == 0)
            {
                TourSelectionActions.FillSuitableToursResortCategory();
            }
            // resort + category + diet
            else if (TourSelectionActions._selectedResorts.Count > 0 && TourSelectionActions._selectedCategory.Count > 0 && TourSelectionActions._selectedHotels.Count == 0 && TourSelectionActions._selectedTypesOfDiets.Count > 0)
            {
                TourSelectionActions.FillSuitableToursResortCategoryDiet();
            }
            // hotel
            else if (TourSelectionActions._selectedResorts.Count >= 0 && TourSelectionActions._selectedCategory.Count >= 0 && TourSelectionActions._selectedHotels.Count > 0 && TourSelectionActions._selectedTypesOfDiets.Count == 0)
            {
                TourSelectionActions.FillSuitableToursHotel();
            }
            // hotel + diet
            else if (TourSelectionActions._selectedResorts.Count >= 0 && TourSelectionActions._selectedCategory.Count >= 0 && TourSelectionActions._selectedHotels.Count > 0 && TourSelectionActions._selectedTypesOfDiets.Count > 0)
            {
                TourSelectionActions.FillSuitableToursHotelDiet();
            }

            if (TourSelectionActions._suitableTours.Count == 0)
            {
                suitableToursDataGridView.Visible = false;

                if (selectedLanguage == "en")
                {
                    MessageBox.Show("No suitable tours.\nTry to choose other parameters of selection.");
                    suitableTousLabel.Text = "No suitable tours.";
                    requestButton.Visible = false;
                }
                else
                {
                    MessageBox.Show("Нет подходящих туров.\nПопробуйте изменить параметры подбора.");
                    suitableTousLabel.Text = "Нет подходящих туров.";
                    requestButton.Visible = false;
                }
                isSuit = true;
            }
            else
            {
                foreach (var tour in TourSelectionActions._suitableTours)
                {
                    suitableToursDataGridView.Rows.Add(tour.DepartureDate, tour.TourName, tour.Nights, tour.Hotel, tour.Diet, tour.People, tour.Price, tour.Transport, tour.TourId);
                }
                isSuit = false;
            }
        }

        /// <summary>
        /// Find button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void findButton_Click(object sender, EventArgs e)
        {
            TourSelectionActions._requestTourId = -1;

            if (suitableToursDataGridView.Visible == true)
            {
                suitableToursDataGridView.Rows.Clear();
            }
            else
            {
                suitableTousLabel.Visible = true;
                suitableToursDataGridView.Visible = true;
                requestButton.Visible = true;
                suitableToursDataGridView.AllowUserToAddRows = false;
                if (isSuit)
                {
                    suitableToursDataGridView.Location = new Point(34, 400);
                    requestButton.Location = new Point(624, 654);
                }
                else
                {
                    suitableToursDataGridView.Location = new Point(34, 420);
                    requestButton.Location = new Point(624, 674);
                }
                suitableToursDataGridView.Height = 250;
                suitableTousLabel.Text = "SUITABLE TOURS";

                if (selectedLanguage == "ru")
                {
                    suitableTousLabel.Text = "ПОДХОДЯЩИЕ ТУРЫ";

                    suitableToursDataGridView.Columns.Clear();
                    suitableToursDataGridView.Columns.Add("Отъезд", "Отъезд");
                    suitableToursDataGridView.Columns.Add("Тур", "Тур");
                    suitableToursDataGridView.Columns[1].Width = 190;
                    suitableToursDataGridView.Columns.Add("Ночей", "Ночей");
                    suitableToursDataGridView.Columns.Add("Отель", "Отель");
                    suitableToursDataGridView.Columns[3].Width = 310;
                    suitableToursDataGridView.Columns.Add("Питание", "Питание");
                    suitableToursDataGridView.Columns.Add("Человек", "Человек");
                    suitableToursDataGridView.Columns.Add("Стоимость", "Стоимость");
                    suitableToursDataGridView.Columns.Add("Транспорт", "Транспорт");
                    suitableToursDataGridView.Columns[7].Width = 160;
                    suitableToursDataGridView.Columns.Add("TourId", "TourId");
                    suitableToursDataGridView.Columns[8].Visible = false;

                    for (int i = 0; i < suitableToursDataGridView.Columns.Count; i++)
                    {
                        suitableToursDataGridView.Columns[i].ReadOnly = true;
                    }
                }
            }
            
            FillSuitableTours();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserActions._username = "";
            UserActions._userId = -1;
            UserActions._userRole = "";

            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name == "MainForm")
                {
                    var mainForm = Application.OpenForms.OfType<MainForm>().Single();
                    mainForm.ChangeMenu();
                }
                else
                {
                    Application.OpenForms[i].Close();
                }
            }
        }

        private void suitableToursDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in suitableToursDataGridView.SelectedRows)
            {
                TourSelectionActions._requestTourId = Int32.Parse(row.Cells[8].Value.ToString());
            }
        }

        private void requestButton_Click(object sender, EventArgs e)
        {
            if (TourSelectionActions._requestTourId != -1)
            {
                if (UserActions._userId != -1)
                {
                    bool isUser = UserActions.CheckIfTheUserIsInRoleUser();

                    if (isUser)
                    {
                        RequestForm requestForm = new RequestForm();
                        requestForm.Show();
                    }
                    else
                    {
                        if (selectedLanguage == "en")
                        {
                            MessageBox.Show("You are not in role USER, so you can not make requests.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            MessageBox.Show("Пользователь вашего типа не может оставлять заявки на туры.", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
                else
                {
                    if (selectedLanguage == "en")
                    {
                        MessageBox.Show("You are not in system.\nPlease, log in or sign up in system and thent select a tour again.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        MessageBox.Show("Вы не вошли в систему.\nПожалуйста, войдите или зарегистрируйтесь, после чего Вы сможете подобрать тур и оставить заявку.", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                    for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
                    {
                        if (Application.OpenForms[i].Name != "MainForm")
                        {
                            Application.OpenForms[i].Close();
                        }
                    }

                    LoginForm loginForm = new LoginForm();
                    loginForm.Show();
                }
            }
            else
            {
                if (selectedLanguage == "en")
                {
                    MessageBox.Show("You did not select a tour.\nPlease, select a tour, you want to request.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Вы не выбрали тур.\nПожалуйста, выберите тур, на который Вы хотели бы оставить заявку.", "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void profileInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
            PersonalForm personalForm = new PersonalForm();
            personalForm.Show();
        }
    }
}