﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TouristCompany.Common;

namespace TouristCompany.WindowsFormsApplication.Forms
{
    public partial class PersonalForm : Form
    {
        // language of the programm
        public string selectedLanguage = ApplicationParameters.selectedLanguage;

        public PersonalForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Check language of system.
        /// </summary>
        private void CheckLanguage()
        {
            if (selectedLanguage == "ru")
            {
                captionLabel.Text = "ПЕРСОНАЛЬНЫЕ ДАННЫЕ";

                usernameLabel.Text = "Имя пользователя";
                fullNameLabel.Text = "ФИО";
                dateOfBirthLabel.Text = "Дата рождения";
                emailLabel.Text = "Почта";
                phoneLabel.Text = "Телефон";

                editPasswordButton.Text = "Изменить пароль";
                removeProfileButton.Text = "Удалить профиль";

                backToolStripMenuItem.Text = "Назад";
                databaseToolStripMenuItem.Text = "База данных";
                requestsToolStripMenuItem.Text = "Заявки";
                personalToolStripMenuItem.Text = "Профиль";
            }
        }

        /// <summary>
        /// Check user role.
        /// </summary>
        private void CheckUserRole()
        {
            if (UserActions._userRole == "user")
            {
                databaseToolStripMenuItem.Visible = false;
                requestsToolStripMenuItem.Visible = true;
            }
            else if (UserActions._userRole == "agent")
            {
                databaseToolStripMenuItem.Visible = true;
                requestsToolStripMenuItem.Visible = true;
            }
            else
            {
                databaseToolStripMenuItem.Visible = true;
                requestsToolStripMenuItem.Visible = false;
                removeProfileButton.Visible = false;
            }
        }

        /// <summary>
        /// Filling of personal data.
        /// </summary>
        private void FillPersonalData()
        {
            CurrentUser _user = UserActions.GetCurrentUserInfo();

            usernameTextBox.Text = _user.Username;
            fullNameTextBox.Text = _user.FullName;
            dateOfBirthTextBox.Text = _user.DateOfBirth;
            emailTextBox.Text = _user.Email;
            phoneTextBox.Text = _user.PhoneNumber;
        }

        /// <summary>
        /// Edit email.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editEmailButton_Click(object sender, EventArgs e)
        {
            WorkingForm form = new WorkingForm();
            form.WorkWithEmail();
            form.Show();
        }

        /// <summary>
        /// Personal menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void personalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            usernameLabel.Visible = true;
            usernameTextBox.Visible = true;
            fullNameLabel.Visible = true;
            fullNameTextBox.Visible = true;
            dateOfBirthLabel.Visible = true;
            dateOfBirthTextBox.Visible = true;
            emailLabel.Visible = true;
            emailTextBox.Visible = true;
            phoneLabel.Visible = true;
            phoneTextBox.Visible = true;

            editUsernameButton.Visible = true;
            editFullNameButton.Visible = true;
            editDateOfBirthButton.Visible = true;
            editEmailButton.Visible = true;
            editPhoneButton.Visible = true;
            editPasswordButton.Visible = true;
            
            if (UserActions._userRole == "admin")
            {
                removeProfileButton.Visible = false;
            }
            else
            {
                removeProfileButton.Visible = true;
            }
        }

        /// <summary>
        /// Form load.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PersonalForm_Load(object sender, EventArgs e)
        {
            CheckLanguage();
            CheckUserRole();
            FillPersonalData();
        }

        /// <summary>
        /// Back to form, from what this form was called.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// User name changing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editUsernameButton_Click(object sender, EventArgs e)
        {
            WorkingForm form = new WorkingForm();
            form.WorkWithUserName();
            form.Show();
        }

        /// <summary>
        /// Full name changing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editFullNameButton_Click(object sender, EventArgs e)
        {
            WorkingForm form = new WorkingForm();
            form.WorkWithFullName();
            form.Show();
        }

        /// <summary>
        /// Date of birth changing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editDateOfBirthButton_Click(object sender, EventArgs e)
        {
            WorkingForm form = new WorkingForm();
            form.WorkWithDateOfBirth();
            form.Show();
        }

        /// <summary>
        /// Phone changing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editPhoneButton_Click(object sender, EventArgs e)
        {
            WorkingForm form = new WorkingForm();
            form.WorkWithPhone();
            form.Show();
        }

        /// <summary>
        /// Delete user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void removeProfileButton_Click(object sender, EventArgs e)
        {
            string message = "";
            string confirmation = "";
            if (selectedLanguage == "en")
            {
                message = "Are you sure you want to remove your profile?";
                confirmation = "Confirm removing";
            }
            else
            {
                message = "Вы уверены, что хотите удалить свой профиль?";
                confirmation = "Подтвердите удаление";
            }

            var confirmResult = MessageBox.Show(message, confirmation, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

            if (confirmResult == DialogResult.Yes)
            {
                UserActions.DeleteUser();

                UserActions._username = "";
                UserActions._userId = -1;
                UserActions._userRole = "";

                for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
                {
                    if (Application.OpenForms[i].Name == "MainForm")
                    {
                        var mainForm = Application.OpenForms.OfType<MainForm>().Single();
                        mainForm.ChangeMenu();
                    }
                    else
                    {
                        Application.OpenForms[i].Close();
                    }
                }
            }
            else
            {
                // If 'No', do something here.
            }            
        }

        /// <summary>
        /// Password changing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editPasswordButton_Click(object sender, EventArgs e)
        {
            WorkingForm form = new WorkingForm();
            form.WorkWithPassword();
            form.Show();
        }
    }
}
