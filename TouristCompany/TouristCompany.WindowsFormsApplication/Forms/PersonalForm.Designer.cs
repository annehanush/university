﻿namespace TouristCompany.WindowsFormsApplication.Forms
{
    partial class PersonalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PersonalForm));
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.backToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.personalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.requestsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.captionLabel = new System.Windows.Forms.Label();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.usernameTextBox = new System.Windows.Forms.TextBox();
            this.editUsernameButton = new System.Windows.Forms.Button();
            this.editFullNameButton = new System.Windows.Forms.Button();
            this.fullNameLabel = new System.Windows.Forms.Label();
            this.fullNameTextBox = new System.Windows.Forms.TextBox();
            this.editDateOfBirthButton = new System.Windows.Forms.Button();
            this.dateOfBirthLabel = new System.Windows.Forms.Label();
            this.dateOfBirthTextBox = new System.Windows.Forms.TextBox();
            this.editEmailButton = new System.Windows.Forms.Button();
            this.emailLabel = new System.Windows.Forms.Label();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.editPhoneButton = new System.Windows.Forms.Button();
            this.phoneLabel = new System.Windows.Forms.Label();
            this.phoneTextBox = new System.Windows.Forms.TextBox();
            this.editPasswordButton = new System.Windows.Forms.Button();
            this.removeProfileButton = new System.Windows.Forms.Button();
            this.mainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.BackColor = System.Drawing.Color.Black;
            this.mainMenu.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backToolStripMenuItem,
            this.personalToolStripMenuItem,
            this.requestsToolStripMenuItem,
            this.databaseToolStripMenuItem});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(765, 29);
            this.mainMenu.TabIndex = 0;
            this.mainMenu.Text = "menuStrip1";
            // 
            // backToolStripMenuItem
            // 
            this.backToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.backToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.backToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("backToolStripMenuItem.Image")));
            this.backToolStripMenuItem.Name = "backToolStripMenuItem";
            this.backToolStripMenuItem.Size = new System.Drawing.Size(74, 25);
            this.backToolStripMenuItem.Text = "Back";
            this.backToolStripMenuItem.Click += new System.EventHandler(this.backToolStripMenuItem_Click);
            // 
            // personalToolStripMenuItem
            // 
            this.personalToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.personalToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.personalToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.personalToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("personalToolStripMenuItem.Image")));
            this.personalToolStripMenuItem.Name = "personalToolStripMenuItem";
            this.personalToolStripMenuItem.Size = new System.Drawing.Size(97, 25);
            this.personalToolStripMenuItem.Text = "Personal";
            this.personalToolStripMenuItem.Click += new System.EventHandler(this.personalToolStripMenuItem_Click);
            // 
            // requestsToolStripMenuItem
            // 
            this.requestsToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.requestsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.requestsToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.requestsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("requestsToolStripMenuItem.Image")));
            this.requestsToolStripMenuItem.Name = "requestsToolStripMenuItem";
            this.requestsToolStripMenuItem.Size = new System.Drawing.Size(101, 25);
            this.requestsToolStripMenuItem.Text = "Requests";
            // 
            // databaseToolStripMenuItem
            // 
            this.databaseToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.databaseToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.databaseToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.databaseToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("databaseToolStripMenuItem.Image")));
            this.databaseToolStripMenuItem.Name = "databaseToolStripMenuItem";
            this.databaseToolStripMenuItem.Size = new System.Drawing.Size(102, 25);
            this.databaseToolStripMenuItem.Text = "Database";
            // 
            // captionLabel
            // 
            this.captionLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.captionLabel.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.captionLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.captionLabel.Location = new System.Drawing.Point(0, 29);
            this.captionLabel.Name = "captionLabel";
            this.captionLabel.Size = new System.Drawing.Size(765, 82);
            this.captionLabel.TabIndex = 5;
            this.captionLabel.Text = "PERSONAL INFO";
            this.captionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.usernameLabel.Location = new System.Drawing.Point(167, 122);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(75, 20);
            this.usernameLabel.TabIndex = 9;
            this.usernameLabel.Text = "Username";
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.Location = new System.Drawing.Point(338, 122);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.ReadOnly = true;
            this.usernameTextBox.Size = new System.Drawing.Size(220, 25);
            this.usernameTextBox.TabIndex = 8;
            // 
            // editUsernameButton
            // 
            this.editUsernameButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.editUsernameButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("editUsernameButton.BackgroundImage")));
            this.editUsernameButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.editUsernameButton.Location = new System.Drawing.Point(566, 122);
            this.editUsernameButton.Name = "editUsernameButton";
            this.editUsernameButton.Size = new System.Drawing.Size(28, 25);
            this.editUsernameButton.TabIndex = 10;
            this.editUsernameButton.UseVisualStyleBackColor = false;
            this.editUsernameButton.Click += new System.EventHandler(this.editUsernameButton_Click);
            // 
            // editFullNameButton
            // 
            this.editFullNameButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.editFullNameButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("editFullNameButton.BackgroundImage")));
            this.editFullNameButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.editFullNameButton.Location = new System.Drawing.Point(566, 153);
            this.editFullNameButton.Name = "editFullNameButton";
            this.editFullNameButton.Size = new System.Drawing.Size(28, 25);
            this.editFullNameButton.TabIndex = 14;
            this.editFullNameButton.UseVisualStyleBackColor = false;
            this.editFullNameButton.Click += new System.EventHandler(this.editFullNameButton_Click);
            // 
            // fullNameLabel
            // 
            this.fullNameLabel.AutoSize = true;
            this.fullNameLabel.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fullNameLabel.Location = new System.Drawing.Point(167, 153);
            this.fullNameLabel.Name = "fullNameLabel";
            this.fullNameLabel.Size = new System.Drawing.Size(73, 20);
            this.fullNameLabel.TabIndex = 13;
            this.fullNameLabel.Text = "Full name";
            // 
            // fullNameTextBox
            // 
            this.fullNameTextBox.Location = new System.Drawing.Point(338, 152);
            this.fullNameTextBox.Name = "fullNameTextBox";
            this.fullNameTextBox.ReadOnly = true;
            this.fullNameTextBox.Size = new System.Drawing.Size(220, 25);
            this.fullNameTextBox.TabIndex = 12;
            // 
            // editDateOfBirthButton
            // 
            this.editDateOfBirthButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.editDateOfBirthButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("editDateOfBirthButton.BackgroundImage")));
            this.editDateOfBirthButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.editDateOfBirthButton.Location = new System.Drawing.Point(566, 183);
            this.editDateOfBirthButton.Name = "editDateOfBirthButton";
            this.editDateOfBirthButton.Size = new System.Drawing.Size(28, 25);
            this.editDateOfBirthButton.TabIndex = 17;
            this.editDateOfBirthButton.UseVisualStyleBackColor = false;
            this.editDateOfBirthButton.Click += new System.EventHandler(this.editDateOfBirthButton_Click);
            // 
            // dateOfBirthLabel
            // 
            this.dateOfBirthLabel.AutoSize = true;
            this.dateOfBirthLabel.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateOfBirthLabel.Location = new System.Drawing.Point(167, 184);
            this.dateOfBirthLabel.Name = "dateOfBirthLabel";
            this.dateOfBirthLabel.Size = new System.Drawing.Size(94, 20);
            this.dateOfBirthLabel.TabIndex = 16;
            this.dateOfBirthLabel.Text = "Date of birth";
            // 
            // dateOfBirthTextBox
            // 
            this.dateOfBirthTextBox.Location = new System.Drawing.Point(338, 183);
            this.dateOfBirthTextBox.Name = "dateOfBirthTextBox";
            this.dateOfBirthTextBox.ReadOnly = true;
            this.dateOfBirthTextBox.Size = new System.Drawing.Size(220, 25);
            this.dateOfBirthTextBox.TabIndex = 15;
            // 
            // editEmailButton
            // 
            this.editEmailButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.editEmailButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("editEmailButton.BackgroundImage")));
            this.editEmailButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.editEmailButton.Location = new System.Drawing.Point(566, 214);
            this.editEmailButton.Name = "editEmailButton";
            this.editEmailButton.Size = new System.Drawing.Size(28, 25);
            this.editEmailButton.TabIndex = 20;
            this.editEmailButton.UseVisualStyleBackColor = false;
            this.editEmailButton.Click += new System.EventHandler(this.editEmailButton_Click);
            // 
            // emailLabel
            // 
            this.emailLabel.AutoSize = true;
            this.emailLabel.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.emailLabel.Location = new System.Drawing.Point(167, 215);
            this.emailLabel.Name = "emailLabel";
            this.emailLabel.Size = new System.Drawing.Size(46, 20);
            this.emailLabel.TabIndex = 19;
            this.emailLabel.Text = "Email";
            // 
            // emailTextBox
            // 
            this.emailTextBox.Location = new System.Drawing.Point(338, 214);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.ReadOnly = true;
            this.emailTextBox.Size = new System.Drawing.Size(220, 25);
            this.emailTextBox.TabIndex = 18;
            // 
            // editPhoneButton
            // 
            this.editPhoneButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.editPhoneButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("editPhoneButton.BackgroundImage")));
            this.editPhoneButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.editPhoneButton.Location = new System.Drawing.Point(566, 245);
            this.editPhoneButton.Name = "editPhoneButton";
            this.editPhoneButton.Size = new System.Drawing.Size(28, 25);
            this.editPhoneButton.TabIndex = 23;
            this.editPhoneButton.UseVisualStyleBackColor = false;
            this.editPhoneButton.Click += new System.EventHandler(this.editPhoneButton_Click);
            // 
            // phoneLabel
            // 
            this.phoneLabel.AutoSize = true;
            this.phoneLabel.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.phoneLabel.Location = new System.Drawing.Point(167, 246);
            this.phoneLabel.Name = "phoneLabel";
            this.phoneLabel.Size = new System.Drawing.Size(105, 20);
            this.phoneLabel.TabIndex = 22;
            this.phoneLabel.Text = "Phone number";
            // 
            // phoneTextBox
            // 
            this.phoneTextBox.Location = new System.Drawing.Point(338, 245);
            this.phoneTextBox.Name = "phoneTextBox";
            this.phoneTextBox.ReadOnly = true;
            this.phoneTextBox.Size = new System.Drawing.Size(220, 25);
            this.phoneTextBox.TabIndex = 21;
            // 
            // editPasswordButton
            // 
            this.editPasswordButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.editPasswordButton.Image = ((System.Drawing.Image)(resources.GetObject("editPasswordButton.Image")));
            this.editPasswordButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.editPasswordButton.Location = new System.Drawing.Point(458, 287);
            this.editPasswordButton.Name = "editPasswordButton";
            this.editPasswordButton.Size = new System.Drawing.Size(136, 29);
            this.editPasswordButton.TabIndex = 24;
            this.editPasswordButton.Text = "Change password";
            this.editPasswordButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.editPasswordButton.UseVisualStyleBackColor = true;
            this.editPasswordButton.Click += new System.EventHandler(this.editPasswordButton_Click);
            // 
            // removeProfileButton
            // 
            this.removeProfileButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.removeProfileButton.Image = ((System.Drawing.Image)(resources.GetObject("removeProfileButton.Image")));
            this.removeProfileButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.removeProfileButton.Location = new System.Drawing.Point(458, 322);
            this.removeProfileButton.Name = "removeProfileButton";
            this.removeProfileButton.Size = new System.Drawing.Size(136, 29);
            this.removeProfileButton.TabIndex = 25;
            this.removeProfileButton.Text = "Remove profile";
            this.removeProfileButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.removeProfileButton.UseVisualStyleBackColor = true;
            this.removeProfileButton.Click += new System.EventHandler(this.removeProfileButton_Click);
            // 
            // PersonalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 386);
            this.Controls.Add(this.removeProfileButton);
            this.Controls.Add(this.editPasswordButton);
            this.Controls.Add(this.editPhoneButton);
            this.Controls.Add(this.phoneLabel);
            this.Controls.Add(this.phoneTextBox);
            this.Controls.Add(this.editEmailButton);
            this.Controls.Add(this.emailLabel);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(this.editDateOfBirthButton);
            this.Controls.Add(this.dateOfBirthLabel);
            this.Controls.Add(this.dateOfBirthTextBox);
            this.Controls.Add(this.editFullNameButton);
            this.Controls.Add(this.fullNameLabel);
            this.Controls.Add(this.fullNameTextBox);
            this.Controls.Add(this.editUsernameButton);
            this.Controls.Add(this.usernameLabel);
            this.Controls.Add(this.usernameTextBox);
            this.Controls.Add(this.captionLabel);
            this.Controls.Add(this.mainMenu);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mainMenu;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(781, 425);
            this.MinimumSize = new System.Drawing.Size(781, 425);
            this.Name = "PersonalForm";
            this.Text = "Paradise - Personal";
            this.Load += new System.EventHandler(this.PersonalForm_Load);
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem backToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem personalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem requestsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem databaseToolStripMenuItem;
        private System.Windows.Forms.Label captionLabel;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.TextBox usernameTextBox;
        private System.Windows.Forms.Button editUsernameButton;
        private System.Windows.Forms.Button editFullNameButton;
        private System.Windows.Forms.Label fullNameLabel;
        private System.Windows.Forms.TextBox fullNameTextBox;
        private System.Windows.Forms.Button editDateOfBirthButton;
        private System.Windows.Forms.Label dateOfBirthLabel;
        private System.Windows.Forms.TextBox dateOfBirthTextBox;
        private System.Windows.Forms.Button editEmailButton;
        private System.Windows.Forms.Label emailLabel;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.Button editPhoneButton;
        private System.Windows.Forms.Label phoneLabel;
        private System.Windows.Forms.TextBox phoneTextBox;
        private System.Windows.Forms.Button editPasswordButton;
        private System.Windows.Forms.Button removeProfileButton;
    }
}