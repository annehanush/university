﻿namespace TouristCompany.WindowsFormsApplication
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.paradiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.countriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tourSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contactToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fAQToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.signUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profileInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.langToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.welcomeLabel = new System.Windows.Forms.Label();
            this.welcomLabelBigger = new System.Windows.Forms.Label();
            this.mainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.BackColor = System.Drawing.Color.Black;
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.paradiseToolStripMenuItem,
            this.countriesToolStripMenuItem,
            this.tourSelectionToolStripMenuItem,
            this.contactToolStripMenuItem,
            this.fAQToolStripMenuItem,
            this.signUpToolStripMenuItem,
            this.logInToolStripMenuItem,
            this.profileToolStripMenuItem,
            this.langToolStripMenuItem});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(765, 33);
            this.mainMenu.Stretch = false;
            this.mainMenu.TabIndex = 0;
            this.mainMenu.Text = "menuStrip1";
            // 
            // paradiseToolStripMenuItem
            // 
            this.paradiseToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.paradiseToolStripMenuItem.ForeColor = System.Drawing.Color.DarkOrange;
            this.paradiseToolStripMenuItem.Name = "paradiseToolStripMenuItem";
            this.paradiseToolStripMenuItem.Size = new System.Drawing.Size(99, 29);
            this.paradiseToolStripMenuItem.Text = "Paradise";
            // 
            // countriesToolStripMenuItem
            // 
            this.countriesToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.countriesToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.countriesToolStripMenuItem.Margin = new System.Windows.Forms.Padding(120, 0, 0, 0);
            this.countriesToolStripMenuItem.Name = "countriesToolStripMenuItem";
            this.countriesToolStripMenuItem.Size = new System.Drawing.Size(80, 29);
            this.countriesToolStripMenuItem.Text = "Countries";
            this.countriesToolStripMenuItem.Click += new System.EventHandler(this.countriesToolStripMenuItem_Click);
            // 
            // tourSelectionToolStripMenuItem
            // 
            this.tourSelectionToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.tourSelectionToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.tourSelectionToolStripMenuItem.Name = "tourSelectionToolStripMenuItem";
            this.tourSelectionToolStripMenuItem.Size = new System.Drawing.Size(106, 29);
            this.tourSelectionToolStripMenuItem.Text = "Tour Selection";
            this.tourSelectionToolStripMenuItem.Click += new System.EventHandler(this.tourSelectionToolStripMenuItem_Click);
            // 
            // contactToolStripMenuItem
            // 
            this.contactToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.contactToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.contactToolStripMenuItem.Name = "contactToolStripMenuItem";
            this.contactToolStripMenuItem.Size = new System.Drawing.Size(69, 29);
            this.contactToolStripMenuItem.Text = "Contact";
            // 
            // fAQToolStripMenuItem
            // 
            this.fAQToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.fAQToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.fAQToolStripMenuItem.Name = "fAQToolStripMenuItem";
            this.fAQToolStripMenuItem.Size = new System.Drawing.Size(57, 29);
            this.fAQToolStripMenuItem.Text = "F.A.Q.";
            // 
            // signUpToolStripMenuItem
            // 
            this.signUpToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.signUpToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.signUpToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("signUpToolStripMenuItem.Image")));
            this.signUpToolStripMenuItem.Margin = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.signUpToolStripMenuItem.Name = "signUpToolStripMenuItem";
            this.signUpToolStripMenuItem.Size = new System.Drawing.Size(85, 29);
            this.signUpToolStripMenuItem.Text = "Sign Up";
            this.signUpToolStripMenuItem.Click += new System.EventHandler(this.signUpToolStripMenuItem_Click);
            // 
            // logInToolStripMenuItem
            // 
            this.logInToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.logInToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.logInToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("logInToolStripMenuItem.Image")));
            this.logInToolStripMenuItem.Name = "logInToolStripMenuItem";
            this.logInToolStripMenuItem.Size = new System.Drawing.Size(76, 29);
            this.logInToolStripMenuItem.Text = "Log In";
            this.logInToolStripMenuItem.Click += new System.EventHandler(this.logInToolStripMenuItem_Click);
            // 
            // profileToolStripMenuItem
            // 
            this.profileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.profileInfoToolStripMenuItem,
            this.logOutToolStripMenuItem});
            this.profileToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.profileToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.profileToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("profileToolStripMenuItem.Image")));
            this.profileToolStripMenuItem.Name = "profileToolStripMenuItem";
            this.profileToolStripMenuItem.Size = new System.Drawing.Size(75, 29);
            this.profileToolStripMenuItem.Text = "Profile";
            this.profileToolStripMenuItem.Visible = false;
            // 
            // profileInfoToolStripMenuItem
            // 
            this.profileInfoToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.profileInfoToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.profileInfoToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("profileInfoToolStripMenuItem.Image")));
            this.profileInfoToolStripMenuItem.Name = "profileInfoToolStripMenuItem";
            this.profileInfoToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.profileInfoToolStripMenuItem.Text = "Profile info";
            this.profileInfoToolStripMenuItem.Click += new System.EventHandler(this.profileInfoToolStripMenuItem_Click);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.logOutToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.logOutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("logOutToolStripMenuItem.Image")));
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.logOutToolStripMenuItem.Text = "Log out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // langToolStripMenuItem
            // 
            this.langToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.langToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.langToolStripMenuItem.Name = "langToolStripMenuItem";
            this.langToolStripMenuItem.Size = new System.Drawing.Size(34, 29);
            this.langToolStripMenuItem.Text = "ru";
            this.langToolStripMenuItem.Click += new System.EventHandler(this.langToolStripMenuItem_Click);
            // 
            // welcomeLabel
            // 
            this.welcomeLabel.BackColor = System.Drawing.Color.Transparent;
            this.welcomeLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this.welcomeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.welcomeLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 26.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.welcomeLabel.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.welcomeLabel.Location = new System.Drawing.Point(0, 33);
            this.welcomeLabel.Margin = new System.Windows.Forms.Padding(0);
            this.welcomeLabel.Name = "welcomeLabel";
            this.welcomeLabel.Size = new System.Drawing.Size(765, 353);
            this.welcomeLabel.TabIndex = 1;
            this.welcomeLabel.Text = "Welcome To Our Agency!";
            this.welcomeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // welcomLabelBigger
            // 
            this.welcomLabelBigger.AutoSize = true;
            this.welcomLabelBigger.BackColor = System.Drawing.Color.Transparent;
            this.welcomLabelBigger.Font = new System.Drawing.Font("Segoe UI Black", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.welcomLabelBigger.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.welcomLabelBigger.Location = new System.Drawing.Point(119, 232);
            this.welcomLabelBigger.Margin = new System.Windows.Forms.Padding(0);
            this.welcomLabelBigger.Name = "welcomLabelBigger";
            this.welcomLabelBigger.Size = new System.Drawing.Size(526, 59);
            this.welcomLabelBigger.TabIndex = 2;
            this.welcomLabelBigger.Text = "IT\'S NICE TO MEET YOU";
            this.welcomLabelBigger.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(765, 386);
            this.Controls.Add(this.welcomLabelBigger);
            this.Controls.Add(this.welcomeLabel);
            this.Controls.Add(this.mainMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mainMenu;
            this.Name = "MainForm";
            this.Text = "Paradise";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem paradiseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem countriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tourSelectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contactToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fAQToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem signUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logInToolStripMenuItem;
        private System.Windows.Forms.Label welcomeLabel;
        private System.Windows.Forms.Label welcomLabelBigger;
        private System.Windows.Forms.ToolStripMenuItem langToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profileInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
    }
}

