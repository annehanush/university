﻿namespace TouristCompany.WindowsFormsApplication.Forms
{
    partial class TourSelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TourSelectionForm));
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.paradiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.countriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tourSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contactToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fAQToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.signUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profileInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.captionLabel = new System.Windows.Forms.Label();
            this.departureCityComboBox = new System.Windows.Forms.ComboBox();
            this.departureCityLabel = new System.Windows.Forms.Label();
            this.countryComboBox = new System.Windows.Forms.ComboBox();
            this.countryLabel = new System.Windows.Forms.Label();
            this.tourComboBox = new System.Windows.Forms.ComboBox();
            this.tourLabel = new System.Windows.Forms.Label();
            this.line1 = new System.Windows.Forms.Label();
            this.departureFromDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.departureFromLabel = new System.Windows.Forms.Label();
            this.departureToDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.departureToLabel = new System.Windows.Forms.Label();
            this.nightsFromComboBox = new System.Windows.Forms.ComboBox();
            this.nightsFromLabel = new System.Windows.Forms.Label();
            this.nightsToComboBox = new System.Windows.Forms.ComboBox();
            this.nightsToLabel = new System.Windows.Forms.Label();
            this.peopleComboBox = new System.Windows.Forms.ComboBox();
            this.peopleLabel = new System.Windows.Forms.Label();
            this.currencyComboBox = new System.Windows.Forms.ComboBox();
            this.currencyLabel = new System.Windows.Forms.Label();
            this.line2 = new System.Windows.Forms.Label();
            this.resortLabel = new System.Windows.Forms.Label();
            this.resortPanel = new System.Windows.Forms.Panel();
            this.categoryPanel = new System.Windows.Forms.Panel();
            this.categoryLabel = new System.Windows.Forms.Label();
            this.hotelPanel = new System.Windows.Forms.Panel();
            this.hotelLabel = new System.Windows.Forms.Label();
            this.dietPanel = new System.Windows.Forms.Panel();
            this.dietLabel = new System.Windows.Forms.Label();
            this.findButton = new System.Windows.Forms.Button();
            this.TourId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Transport = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.People = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Diet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hotel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nights = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tour = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Arrival = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.suitableToursDataGridView = new System.Windows.Forms.DataGridView();
            this.suitableTousLabel = new System.Windows.Forms.Label();
            this.requestButton = new System.Windows.Forms.Button();
            this.mainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.suitableToursDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.BackColor = System.Drawing.Color.Black;
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.paradiseToolStripMenuItem,
            this.countriesToolStripMenuItem,
            this.tourSelectionToolStripMenuItem,
            this.contactToolStripMenuItem,
            this.fAQToolStripMenuItem,
            this.signUpToolStripMenuItem,
            this.profileToolStripMenuItem,
            this.logInToolStripMenuItem});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Padding = new System.Windows.Forms.Padding(8, 4, 0, 4);
            this.mainMenu.Size = new System.Drawing.Size(748, 37);
            this.mainMenu.Stretch = false;
            this.mainMenu.TabIndex = 2;
            this.mainMenu.Text = "menuStrip1";
            // 
            // paradiseToolStripMenuItem
            // 
            this.paradiseToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.paradiseToolStripMenuItem.ForeColor = System.Drawing.Color.DarkOrange;
            this.paradiseToolStripMenuItem.Name = "paradiseToolStripMenuItem";
            this.paradiseToolStripMenuItem.Size = new System.Drawing.Size(99, 29);
            this.paradiseToolStripMenuItem.Text = "Paradise";
            this.paradiseToolStripMenuItem.Click += new System.EventHandler(this.paradiseToolStripMenuItem_Click);
            // 
            // countriesToolStripMenuItem
            // 
            this.countriesToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.countriesToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.countriesToolStripMenuItem.Margin = new System.Windows.Forms.Padding(130, 0, 0, 0);
            this.countriesToolStripMenuItem.Name = "countriesToolStripMenuItem";
            this.countriesToolStripMenuItem.Size = new System.Drawing.Size(80, 29);
            this.countriesToolStripMenuItem.Text = "Countries";
            this.countriesToolStripMenuItem.Click += new System.EventHandler(this.countriesToolStripMenuItem_Click);
            // 
            // tourSelectionToolStripMenuItem
            // 
            this.tourSelectionToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.tourSelectionToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.tourSelectionToolStripMenuItem.Name = "tourSelectionToolStripMenuItem";
            this.tourSelectionToolStripMenuItem.Size = new System.Drawing.Size(106, 29);
            this.tourSelectionToolStripMenuItem.Text = "Tour Selection";
            this.tourSelectionToolStripMenuItem.Click += new System.EventHandler(this.tourSelectionToolStripMenuItem_Click);
            // 
            // contactToolStripMenuItem
            // 
            this.contactToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.contactToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.contactToolStripMenuItem.Name = "contactToolStripMenuItem";
            this.contactToolStripMenuItem.Size = new System.Drawing.Size(69, 29);
            this.contactToolStripMenuItem.Text = "Contact";
            // 
            // fAQToolStripMenuItem
            // 
            this.fAQToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.fAQToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.fAQToolStripMenuItem.Name = "fAQToolStripMenuItem";
            this.fAQToolStripMenuItem.Size = new System.Drawing.Size(57, 29);
            this.fAQToolStripMenuItem.Text = "F.A.Q.";
            // 
            // signUpToolStripMenuItem
            // 
            this.signUpToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.signUpToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.signUpToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("signUpToolStripMenuItem.Image")));
            this.signUpToolStripMenuItem.Margin = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.signUpToolStripMenuItem.Name = "signUpToolStripMenuItem";
            this.signUpToolStripMenuItem.Size = new System.Drawing.Size(85, 29);
            this.signUpToolStripMenuItem.Text = "Sign Up";
            this.signUpToolStripMenuItem.Click += new System.EventHandler(this.signUpToolStripMenuItem_Click);
            // 
            // profileToolStripMenuItem
            // 
            this.profileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.profileInfoToolStripMenuItem,
            this.logOutToolStripMenuItem});
            this.profileToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.profileToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.profileToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("profileToolStripMenuItem.Image")));
            this.profileToolStripMenuItem.Name = "profileToolStripMenuItem";
            this.profileToolStripMenuItem.Size = new System.Drawing.Size(75, 29);
            this.profileToolStripMenuItem.Text = "Profile";
            this.profileToolStripMenuItem.Visible = false;
            // 
            // profileInfoToolStripMenuItem
            // 
            this.profileInfoToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.profileInfoToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.profileInfoToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("profileInfoToolStripMenuItem.Image")));
            this.profileInfoToolStripMenuItem.Name = "profileInfoToolStripMenuItem";
            this.profileInfoToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.profileInfoToolStripMenuItem.Text = "Profile info";
            this.profileInfoToolStripMenuItem.Click += new System.EventHandler(this.profileInfoToolStripMenuItem_Click);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.logOutToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.logOutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("logOutToolStripMenuItem.Image")));
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(143, 24);
            this.logOutToolStripMenuItem.Text = "Log out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // logInToolStripMenuItem
            // 
            this.logInToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.logInToolStripMenuItem.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.logInToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("logInToolStripMenuItem.Image")));
            this.logInToolStripMenuItem.Name = "logInToolStripMenuItem";
            this.logInToolStripMenuItem.Size = new System.Drawing.Size(76, 29);
            this.logInToolStripMenuItem.Text = "Log In";
            this.logInToolStripMenuItem.Click += new System.EventHandler(this.logInToolStripMenuItem_Click);
            // 
            // captionLabel
            // 
            this.captionLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.captionLabel.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.captionLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.captionLabel.Location = new System.Drawing.Point(0, 37);
            this.captionLabel.Name = "captionLabel";
            this.captionLabel.Size = new System.Drawing.Size(748, 82);
            this.captionLabel.TabIndex = 3;
            this.captionLabel.Text = "TOUR SELECTION";
            this.captionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // departureCityComboBox
            // 
            this.departureCityComboBox.FormattingEnabled = true;
            this.departureCityComboBox.Location = new System.Drawing.Point(107, 132);
            this.departureCityComboBox.Name = "departureCityComboBox";
            this.departureCityComboBox.Size = new System.Drawing.Size(232, 25);
            this.departureCityComboBox.TabIndex = 4;
            this.departureCityComboBox.SelectedValueChanged += new System.EventHandler(this.departureCityComboBox_SelectedValueChanged);
            this.departureCityComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.departureCityComboBox_KeyPress);
            // 
            // departureCityLabel
            // 
            this.departureCityLabel.AutoSize = true;
            this.departureCityLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departureCityLabel.Location = new System.Drawing.Point(31, 132);
            this.departureCityLabel.Name = "departureCityLabel";
            this.departureCityLabel.Size = new System.Drawing.Size(70, 34);
            this.departureCityLabel.TabIndex = 5;
            this.departureCityLabel.Text = "Departure\r\ncity";
            // 
            // countryComboBox
            // 
            this.countryComboBox.FormattingEnabled = true;
            this.countryComboBox.Location = new System.Drawing.Point(107, 179);
            this.countryComboBox.Name = "countryComboBox";
            this.countryComboBox.Size = new System.Drawing.Size(232, 25);
            this.countryComboBox.TabIndex = 6;
            this.countryComboBox.SelectedValueChanged += new System.EventHandler(this.countryComboBox_SelectedValueChanged);
            this.countryComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.countryComboBox_KeyPress);
            // 
            // countryLabel
            // 
            this.countryLabel.AutoSize = true;
            this.countryLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.countryLabel.Location = new System.Drawing.Point(31, 179);
            this.countryLabel.Name = "countryLabel";
            this.countryLabel.Size = new System.Drawing.Size(58, 17);
            this.countryLabel.TabIndex = 7;
            this.countryLabel.Text = "Country";
            // 
            // tourComboBox
            // 
            this.tourComboBox.FormattingEnabled = true;
            this.tourComboBox.Location = new System.Drawing.Point(496, 132);
            this.tourComboBox.Name = "tourComboBox";
            this.tourComboBox.Size = new System.Drawing.Size(232, 25);
            this.tourComboBox.TabIndex = 8;
            this.tourComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tourComboBox_KeyPress);
            // 
            // tourLabel
            // 
            this.tourLabel.AutoSize = true;
            this.tourLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tourLabel.Location = new System.Drawing.Point(420, 132);
            this.tourLabel.Name = "tourLabel";
            this.tourLabel.Size = new System.Drawing.Size(36, 17);
            this.tourLabel.TabIndex = 9;
            this.tourLabel.Text = "Tour";
            // 
            // line1
            // 
            this.line1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.line1.Location = new System.Drawing.Point(34, 218);
            this.line1.Name = "line1";
            this.line1.Size = new System.Drawing.Size(694, 2);
            this.line1.TabIndex = 10;
            // 
            // departureFromDateTimePicker
            // 
            this.departureFromDateTimePicker.Location = new System.Drawing.Point(107, 237);
            this.departureFromDateTimePicker.Name = "departureFromDateTimePicker";
            this.departureFromDateTimePicker.Size = new System.Drawing.Size(160, 25);
            this.departureFromDateTimePicker.TabIndex = 11;
            this.departureFromDateTimePicker.ValueChanged += new System.EventHandler(this.departureFromDateTimePicker_ValueChanged);
            // 
            // departureFromLabel
            // 
            this.departureFromLabel.AutoSize = true;
            this.departureFromLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departureFromLabel.Location = new System.Drawing.Point(31, 237);
            this.departureFromLabel.Name = "departureFromLabel";
            this.departureFromLabel.Size = new System.Drawing.Size(70, 34);
            this.departureFromLabel.TabIndex = 12;
            this.departureFromLabel.Text = "Departure\r\nfrom";
            // 
            // departureToDateTimePicker
            // 
            this.departureToDateTimePicker.Location = new System.Drawing.Point(107, 285);
            this.departureToDateTimePicker.Name = "departureToDateTimePicker";
            this.departureToDateTimePicker.Size = new System.Drawing.Size(160, 25);
            this.departureToDateTimePicker.TabIndex = 13;
            this.departureToDateTimePicker.ValueChanged += new System.EventHandler(this.departureToDateTimePicker_ValueChanged);
            // 
            // departureToLabel
            // 
            this.departureToLabel.AutoSize = true;
            this.departureToLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departureToLabel.Location = new System.Drawing.Point(31, 285);
            this.departureToLabel.Name = "departureToLabel";
            this.departureToLabel.Size = new System.Drawing.Size(21, 17);
            this.departureToLabel.TabIndex = 14;
            this.departureToLabel.Text = "to";
            // 
            // nightsFromComboBox
            // 
            this.nightsFromComboBox.FormattingEnabled = true;
            this.nightsFromComboBox.Location = new System.Drawing.Point(360, 237);
            this.nightsFromComboBox.Name = "nightsFromComboBox";
            this.nightsFromComboBox.Size = new System.Drawing.Size(86, 25);
            this.nightsFromComboBox.TabIndex = 15;
            this.nightsFromComboBox.SelectedValueChanged += new System.EventHandler(this.nightsFromComboBox_SelectedValueChanged);
            this.nightsFromComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nightsFromComboBox_KeyPress);
            // 
            // nightsFromLabel
            // 
            this.nightsFromLabel.AutoSize = true;
            this.nightsFromLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nightsFromLabel.Location = new System.Drawing.Point(305, 237);
            this.nightsFromLabel.Name = "nightsFromLabel";
            this.nightsFromLabel.Size = new System.Drawing.Size(49, 34);
            this.nightsFromLabel.TabIndex = 16;
            this.nightsFromLabel.Text = "Nights\r\nfrom";
            // 
            // nightsToComboBox
            // 
            this.nightsToComboBox.FormattingEnabled = true;
            this.nightsToComboBox.Location = new System.Drawing.Point(360, 285);
            this.nightsToComboBox.Name = "nightsToComboBox";
            this.nightsToComboBox.Size = new System.Drawing.Size(86, 25);
            this.nightsToComboBox.TabIndex = 17;
            this.nightsToComboBox.SelectedValueChanged += new System.EventHandler(this.nightsToComboBox_SelectedValueChanged);
            this.nightsToComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nightsToComboBox_KeyPress);
            // 
            // nightsToLabel
            // 
            this.nightsToLabel.AutoSize = true;
            this.nightsToLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nightsToLabel.Location = new System.Drawing.Point(305, 285);
            this.nightsToLabel.Name = "nightsToLabel";
            this.nightsToLabel.Size = new System.Drawing.Size(21, 17);
            this.nightsToLabel.TabIndex = 18;
            this.nightsToLabel.Text = "to";
            // 
            // peopleComboBox
            // 
            this.peopleComboBox.FormattingEnabled = true;
            this.peopleComboBox.Location = new System.Drawing.Point(561, 285);
            this.peopleComboBox.Name = "peopleComboBox";
            this.peopleComboBox.Size = new System.Drawing.Size(167, 25);
            this.peopleComboBox.TabIndex = 19;
            this.peopleComboBox.SelectedValueChanged += new System.EventHandler(this.peopleComboBox_SelectedValueChanged);
            this.peopleComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.peopleComboBox_KeyPress);
            // 
            // peopleLabel
            // 
            this.peopleLabel.AutoSize = true;
            this.peopleLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.peopleLabel.Location = new System.Drawing.Point(493, 285);
            this.peopleLabel.Name = "peopleLabel";
            this.peopleLabel.Size = new System.Drawing.Size(50, 17);
            this.peopleLabel.TabIndex = 20;
            this.peopleLabel.Text = "People";
            // 
            // currencyComboBox
            // 
            this.currencyComboBox.FormattingEnabled = true;
            this.currencyComboBox.Location = new System.Drawing.Point(561, 237);
            this.currencyComboBox.Name = "currencyComboBox";
            this.currencyComboBox.Size = new System.Drawing.Size(167, 25);
            this.currencyComboBox.TabIndex = 21;
            this.currencyComboBox.SelectedValueChanged += new System.EventHandler(this.currencyComboBox_SelectedValueChanged);
            this.currencyComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.currencyComboBox_KeyPress);
            // 
            // currencyLabel
            // 
            this.currencyLabel.AutoSize = true;
            this.currencyLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.currencyLabel.Location = new System.Drawing.Point(493, 237);
            this.currencyLabel.Name = "currencyLabel";
            this.currencyLabel.Size = new System.Drawing.Size(62, 17);
            this.currencyLabel.TabIndex = 22;
            this.currencyLabel.Text = "Currency";
            // 
            // line2
            // 
            this.line2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.line2.Location = new System.Drawing.Point(35, 325);
            this.line2.Name = "line2";
            this.line2.Size = new System.Drawing.Size(694, 2);
            this.line2.TabIndex = 23;
            // 
            // resortLabel
            // 
            this.resortLabel.AutoSize = true;
            this.resortLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.resortLabel.Location = new System.Drawing.Point(32, 342);
            this.resortLabel.Name = "resortLabel";
            this.resortLabel.Size = new System.Drawing.Size(53, 17);
            this.resortLabel.TabIndex = 24;
            this.resortLabel.Text = "Resorts";
            // 
            // resortPanel
            // 
            this.resortPanel.AutoScroll = true;
            this.resortPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.resortPanel.Location = new System.Drawing.Point(35, 366);
            this.resortPanel.MaximumSize = new System.Drawing.Size(167, 172);
            this.resortPanel.Name = "resortPanel";
            this.resortPanel.Size = new System.Drawing.Size(167, 172);
            this.resortPanel.TabIndex = 25;
            // 
            // categoryPanel
            // 
            this.categoryPanel.AutoScroll = true;
            this.categoryPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.categoryPanel.Location = new System.Drawing.Point(219, 366);
            this.categoryPanel.Name = "categoryPanel";
            this.categoryPanel.Size = new System.Drawing.Size(86, 172);
            this.categoryPanel.TabIndex = 26;
            // 
            // categoryLabel
            // 
            this.categoryLabel.AutoSize = true;
            this.categoryLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.categoryLabel.Location = new System.Drawing.Point(216, 342);
            this.categoryLabel.Name = "categoryLabel";
            this.categoryLabel.Size = new System.Drawing.Size(73, 17);
            this.categoryLabel.TabIndex = 27;
            this.categoryLabel.Text = "Categories";
            // 
            // hotelPanel
            // 
            this.hotelPanel.AutoScroll = true;
            this.hotelPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hotelPanel.Location = new System.Drawing.Point(320, 366);
            this.hotelPanel.Name = "hotelPanel";
            this.hotelPanel.Size = new System.Drawing.Size(235, 172);
            this.hotelPanel.TabIndex = 26;
            // 
            // hotelLabel
            // 
            this.hotelLabel.AutoSize = true;
            this.hotelLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hotelLabel.Location = new System.Drawing.Point(317, 342);
            this.hotelLabel.Name = "hotelLabel";
            this.hotelLabel.Size = new System.Drawing.Size(48, 17);
            this.hotelLabel.TabIndex = 28;
            this.hotelLabel.Text = "Hotels";
            // 
            // dietPanel
            // 
            this.dietPanel.AutoScroll = true;
            this.dietPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dietPanel.Location = new System.Drawing.Point(572, 366);
            this.dietPanel.Name = "dietPanel";
            this.dietPanel.Size = new System.Drawing.Size(156, 172);
            this.dietPanel.TabIndex = 27;
            // 
            // dietLabel
            // 
            this.dietLabel.AutoSize = true;
            this.dietLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dietLabel.Location = new System.Drawing.Point(569, 342);
            this.dietLabel.Name = "dietLabel";
            this.dietLabel.Size = new System.Drawing.Size(40, 17);
            this.dietLabel.TabIndex = 29;
            this.dietLabel.Text = "Diets";
            // 
            // findButton
            // 
            this.findButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.findButton.Location = new System.Drawing.Point(624, 547);
            this.findButton.Name = "findButton";
            this.findButton.Size = new System.Drawing.Size(104, 30);
            this.findButton.TabIndex = 30;
            this.findButton.Text = "Find";
            this.findButton.UseVisualStyleBackColor = true;
            this.findButton.Click += new System.EventHandler(this.findButton_Click);
            // 
            // TourId
            // 
            this.TourId.HeaderText = "TourId";
            this.TourId.Name = "TourId";
            this.TourId.ReadOnly = true;
            this.TourId.Visible = false;
            // 
            // Transport
            // 
            this.Transport.HeaderText = "Transport";
            this.Transport.Name = "Transport";
            this.Transport.ReadOnly = true;
            this.Transport.Width = 160;
            // 
            // Price
            // 
            this.Price.HeaderText = "Price";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            // 
            // People
            // 
            this.People.HeaderText = "People";
            this.People.Name = "People";
            this.People.ReadOnly = true;
            // 
            // Diet
            // 
            this.Diet.HeaderText = "Diet";
            this.Diet.Name = "Diet";
            this.Diet.ReadOnly = true;
            // 
            // Hotel
            // 
            this.Hotel.HeaderText = "Hotel";
            this.Hotel.Name = "Hotel";
            this.Hotel.ReadOnly = true;
            this.Hotel.Width = 310;
            // 
            // Nights
            // 
            this.Nights.HeaderText = "Nights";
            this.Nights.Name = "Nights";
            this.Nights.ReadOnly = true;
            // 
            // Tour
            // 
            this.Tour.HeaderText = "Tour";
            this.Tour.Name = "Tour";
            this.Tour.ReadOnly = true;
            this.Tour.Width = 190;
            // 
            // Arrival
            // 
            this.Arrival.HeaderText = "Arrival";
            this.Arrival.Name = "Arrival";
            this.Arrival.ReadOnly = true;
            // 
            // suitableToursDataGridView
            // 
            this.suitableToursDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.suitableToursDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Arrival,
            this.Tour,
            this.Nights,
            this.Hotel,
            this.Diet,
            this.People,
            this.Price,
            this.Transport,
            this.TourId});
            this.suitableToursDataGridView.Location = new System.Drawing.Point(34, 615);
            this.suitableToursDataGridView.MultiSelect = false;
            this.suitableToursDataGridView.Name = "suitableToursDataGridView";
            this.suitableToursDataGridView.Size = new System.Drawing.Size(694, 250);
            this.suitableToursDataGridView.TabIndex = 31;
            this.suitableToursDataGridView.Visible = false;
            this.suitableToursDataGridView.SelectionChanged += new System.EventHandler(this.suitableToursDataGridView_SelectionChanged);
            // 
            // suitableTousLabel
            // 
            this.suitableTousLabel.AutoSize = true;
            this.suitableTousLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.suitableTousLabel.Location = new System.Drawing.Point(317, 592);
            this.suitableTousLabel.Name = "suitableTousLabel";
            this.suitableTousLabel.Size = new System.Drawing.Size(112, 17);
            this.suitableTousLabel.TabIndex = 32;
            this.suitableTousLabel.Text = "SUITABLE TOURS";
            this.suitableTousLabel.Visible = false;
            // 
            // requestButton
            // 
            this.requestButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.requestButton.Location = new System.Drawing.Point(624, 874);
            this.requestButton.Name = "requestButton";
            this.requestButton.Size = new System.Drawing.Size(104, 30);
            this.requestButton.TabIndex = 33;
            this.requestButton.Text = "Request";
            this.requestButton.UseVisualStyleBackColor = true;
            this.requestButton.Visible = false;
            this.requestButton.Click += new System.EventHandler(this.requestButton_Click);
            // 
            // TourSelectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(765, 386);
            this.Controls.Add(this.requestButton);
            this.Controls.Add(this.suitableTousLabel);
            this.Controls.Add(this.suitableToursDataGridView);
            this.Controls.Add(this.findButton);
            this.Controls.Add(this.dietLabel);
            this.Controls.Add(this.dietPanel);
            this.Controls.Add(this.hotelLabel);
            this.Controls.Add(this.hotelPanel);
            this.Controls.Add(this.categoryLabel);
            this.Controls.Add(this.categoryPanel);
            this.Controls.Add(this.resortPanel);
            this.Controls.Add(this.resortLabel);
            this.Controls.Add(this.line2);
            this.Controls.Add(this.currencyLabel);
            this.Controls.Add(this.currencyComboBox);
            this.Controls.Add(this.peopleLabel);
            this.Controls.Add(this.peopleComboBox);
            this.Controls.Add(this.nightsToLabel);
            this.Controls.Add(this.nightsToComboBox);
            this.Controls.Add(this.nightsFromLabel);
            this.Controls.Add(this.nightsFromComboBox);
            this.Controls.Add(this.departureToLabel);
            this.Controls.Add(this.departureToDateTimePicker);
            this.Controls.Add(this.departureFromLabel);
            this.Controls.Add(this.departureFromDateTimePicker);
            this.Controls.Add(this.line1);
            this.Controls.Add(this.tourLabel);
            this.Controls.Add(this.tourComboBox);
            this.Controls.Add(this.countryLabel);
            this.Controls.Add(this.countryComboBox);
            this.Controls.Add(this.departureCityLabel);
            this.Controls.Add(this.departureCityComboBox);
            this.Controls.Add(this.captionLabel);
            this.Controls.Add(this.mainMenu);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(781, 425);
            this.MinimumSize = new System.Drawing.Size(781, 425);
            this.Name = "TourSelectionForm";
            this.Text = "Paradise - tour selection";
            this.Load += new System.EventHandler(this.TourSelectionForm_Load);
            this.Resize += new System.EventHandler(this.TourSelectionForm_Resize);
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.suitableToursDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem paradiseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem countriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tourSelectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contactToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fAQToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem signUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logInToolStripMenuItem;
        private System.Windows.Forms.Label captionLabel;
        private System.Windows.Forms.ComboBox departureCityComboBox;
        private System.Windows.Forms.Label departureCityLabel;
        private System.Windows.Forms.ComboBox countryComboBox;
        private System.Windows.Forms.Label countryLabel;
        private System.Windows.Forms.ComboBox tourComboBox;
        private System.Windows.Forms.Label tourLabel;
        private System.Windows.Forms.Label line1;
        private System.Windows.Forms.DateTimePicker departureFromDateTimePicker;
        private System.Windows.Forms.Label departureFromLabel;
        private System.Windows.Forms.DateTimePicker departureToDateTimePicker;
        private System.Windows.Forms.Label departureToLabel;
        private System.Windows.Forms.ComboBox nightsFromComboBox;
        private System.Windows.Forms.Label nightsFromLabel;
        private System.Windows.Forms.ComboBox nightsToComboBox;
        private System.Windows.Forms.Label nightsToLabel;
        private System.Windows.Forms.ComboBox peopleComboBox;
        private System.Windows.Forms.Label peopleLabel;
        private System.Windows.Forms.ComboBox currencyComboBox;
        private System.Windows.Forms.Label currencyLabel;
        private System.Windows.Forms.Label line2;
        private System.Windows.Forms.Label resortLabel;
        private System.Windows.Forms.Panel resortPanel;
        private System.Windows.Forms.Panel categoryPanel;
        private System.Windows.Forms.Label categoryLabel;
        private System.Windows.Forms.Panel hotelPanel;
        private System.Windows.Forms.Label hotelLabel;
        private System.Windows.Forms.Panel dietPanel;
        private System.Windows.Forms.Label dietLabel;
        private System.Windows.Forms.Button findButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn TourId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Transport;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn People;
        private System.Windows.Forms.DataGridViewTextBoxColumn Diet;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hotel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nights;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tour;
        private System.Windows.Forms.DataGridViewTextBoxColumn Arrival;
        private System.Windows.Forms.DataGridView suitableToursDataGridView;
        private System.Windows.Forms.Label suitableTousLabel;
        private System.Windows.Forms.Button requestButton;
        private System.Windows.Forms.ToolStripMenuItem profileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profileInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
    }
}