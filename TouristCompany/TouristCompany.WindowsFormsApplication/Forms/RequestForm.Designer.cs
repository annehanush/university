﻿namespace TouristCompany.WindowsFormsApplication.Forms
{
    partial class RequestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RequestForm));
            this.captionLabel = new System.Windows.Forms.Label();
            this.captionUserInfoLabel = new System.Windows.Forms.Label();
            this.usernameTextBox = new System.Windows.Forms.TextBox();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.emailLabel = new System.Windows.Forms.Label();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.tourInfoLabel = new System.Windows.Forms.Label();
            this.tourLabel = new System.Windows.Forms.Label();
            this.tourTextBox = new System.Windows.Forms.TextBox();
            this.departureDateLabel = new System.Windows.Forms.Label();
            this.departureDateTextBox = new System.Windows.Forms.TextBox();
            this.hotelLabel = new System.Windows.Forms.Label();
            this.hotelTextBox = new System.Windows.Forms.TextBox();
            this.resortLabel = new System.Windows.Forms.Label();
            this.resortTextBox = new System.Windows.Forms.TextBox();
            this.transportLabel = new System.Windows.Forms.Label();
            this.transportTextBox = new System.Windows.Forms.TextBox();
            this.peopleAmountLabel = new System.Windows.Forms.Label();
            this.peopleAmountTextBox = new System.Windows.Forms.TextBox();
            this.totalPriceLabel = new System.Windows.Forms.Label();
            this.totalPriceTextBox = new System.Windows.Forms.TextBox();
            this.additionalLabel = new System.Windows.Forms.Label();
            this.servicesLabel = new System.Windows.Forms.Label();
            this.servicesPanel = new System.Windows.Forms.Panel();
            this.finalLabel = new System.Windows.Forms.Label();
            this.nightsLabel = new System.Windows.Forms.Label();
            this.nightsTextBox = new System.Windows.Forms.TextBox();
            this.priceLabel = new System.Windows.Forms.Label();
            this.priceTextBox = new System.Windows.Forms.TextBox();
            this.submitButton = new System.Windows.Forms.Button();
            this.orderPicture2 = new System.Windows.Forms.PictureBox();
            this.orderPicture3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.orderPicture2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderPicture3)).BeginInit();
            this.SuspendLayout();
            // 
            // captionLabel
            // 
            this.captionLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.captionLabel.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.captionLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.captionLabel.Location = new System.Drawing.Point(0, 0);
            this.captionLabel.Name = "captionLabel";
            this.captionLabel.Size = new System.Drawing.Size(748, 82);
            this.captionLabel.TabIndex = 4;
            this.captionLabel.Text = "MAKE A REQUEST TO ORDER";
            this.captionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // captionUserInfoLabel
            // 
            this.captionUserInfoLabel.AutoSize = true;
            this.captionUserInfoLabel.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.captionUserInfoLabel.Location = new System.Drawing.Point(169, 82);
            this.captionUserInfoLabel.Name = "captionUserInfoLabel";
            this.captionUserInfoLabel.Size = new System.Drawing.Size(77, 21);
            this.captionUserInfoLabel.TabIndex = 5;
            this.captionUserInfoLabel.Text = "User info";
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.Location = new System.Drawing.Point(399, 113);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.ReadOnly = true;
            this.usernameTextBox.Size = new System.Drawing.Size(185, 25);
            this.usernameTextBox.TabIndex = 6;
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.usernameLabel.Location = new System.Drawing.Point(228, 114);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(75, 20);
            this.usernameLabel.TabIndex = 7;
            this.usernameLabel.Text = "Username";
            // 
            // emailLabel
            // 
            this.emailLabel.AutoSize = true;
            this.emailLabel.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.emailLabel.Location = new System.Drawing.Point(228, 145);
            this.emailLabel.Name = "emailLabel";
            this.emailLabel.Size = new System.Drawing.Size(46, 20);
            this.emailLabel.TabIndex = 9;
            this.emailLabel.Text = "Email";
            // 
            // emailTextBox
            // 
            this.emailTextBox.Location = new System.Drawing.Point(399, 144);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.ReadOnly = true;
            this.emailTextBox.Size = new System.Drawing.Size(185, 25);
            this.emailTextBox.TabIndex = 8;
            // 
            // tourInfoLabel
            // 
            this.tourInfoLabel.AutoSize = true;
            this.tourInfoLabel.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tourInfoLabel.Location = new System.Drawing.Point(169, 187);
            this.tourInfoLabel.Name = "tourInfoLabel";
            this.tourInfoLabel.Size = new System.Drawing.Size(78, 21);
            this.tourInfoLabel.TabIndex = 10;
            this.tourInfoLabel.Text = "Tour info";
            // 
            // tourLabel
            // 
            this.tourLabel.AutoSize = true;
            this.tourLabel.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tourLabel.Location = new System.Drawing.Point(228, 221);
            this.tourLabel.Name = "tourLabel";
            this.tourLabel.Size = new System.Drawing.Size(38, 20);
            this.tourLabel.TabIndex = 12;
            this.tourLabel.Text = "Tour";
            // 
            // tourTextBox
            // 
            this.tourTextBox.Location = new System.Drawing.Point(399, 220);
            this.tourTextBox.Name = "tourTextBox";
            this.tourTextBox.ReadOnly = true;
            this.tourTextBox.Size = new System.Drawing.Size(249, 25);
            this.tourTextBox.TabIndex = 11;
            // 
            // departureDateLabel
            // 
            this.departureDateLabel.AutoSize = true;
            this.departureDateLabel.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.departureDateLabel.Location = new System.Drawing.Point(228, 252);
            this.departureDateLabel.Name = "departureDateLabel";
            this.departureDateLabel.Size = new System.Drawing.Size(76, 20);
            this.departureDateLabel.TabIndex = 14;
            this.departureDateLabel.Text = "Departure";
            // 
            // departureDateTextBox
            // 
            this.departureDateTextBox.Location = new System.Drawing.Point(399, 251);
            this.departureDateTextBox.Name = "departureDateTextBox";
            this.departureDateTextBox.ReadOnly = true;
            this.departureDateTextBox.Size = new System.Drawing.Size(249, 25);
            this.departureDateTextBox.TabIndex = 13;
            // 
            // hotelLabel
            // 
            this.hotelLabel.AutoSize = true;
            this.hotelLabel.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hotelLabel.Location = new System.Drawing.Point(228, 314);
            this.hotelLabel.Name = "hotelLabel";
            this.hotelLabel.Size = new System.Drawing.Size(46, 20);
            this.hotelLabel.TabIndex = 16;
            this.hotelLabel.Text = "Hotel";
            // 
            // hotelTextBox
            // 
            this.hotelTextBox.Location = new System.Drawing.Point(399, 313);
            this.hotelTextBox.Name = "hotelTextBox";
            this.hotelTextBox.ReadOnly = true;
            this.hotelTextBox.Size = new System.Drawing.Size(249, 25);
            this.hotelTextBox.TabIndex = 15;
            // 
            // resortLabel
            // 
            this.resortLabel.AutoSize = true;
            this.resortLabel.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.resortLabel.Location = new System.Drawing.Point(228, 283);
            this.resortLabel.Name = "resortLabel";
            this.resortLabel.Size = new System.Drawing.Size(51, 20);
            this.resortLabel.TabIndex = 18;
            this.resortLabel.Text = "Resort";
            // 
            // resortTextBox
            // 
            this.resortTextBox.Location = new System.Drawing.Point(399, 282);
            this.resortTextBox.Name = "resortTextBox";
            this.resortTextBox.ReadOnly = true;
            this.resortTextBox.Size = new System.Drawing.Size(249, 25);
            this.resortTextBox.TabIndex = 17;
            // 
            // transportLabel
            // 
            this.transportLabel.AutoSize = true;
            this.transportLabel.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.transportLabel.Location = new System.Drawing.Point(228, 345);
            this.transportLabel.Name = "transportLabel";
            this.transportLabel.Size = new System.Drawing.Size(71, 20);
            this.transportLabel.TabIndex = 20;
            this.transportLabel.Text = "Transport";
            // 
            // transportTextBox
            // 
            this.transportTextBox.Location = new System.Drawing.Point(399, 344);
            this.transportTextBox.Name = "transportTextBox";
            this.transportTextBox.ReadOnly = true;
            this.transportTextBox.Size = new System.Drawing.Size(249, 25);
            this.transportTextBox.TabIndex = 19;
            // 
            // peopleAmountLabel
            // 
            this.peopleAmountLabel.AutoSize = true;
            this.peopleAmountLabel.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.peopleAmountLabel.Location = new System.Drawing.Point(228, 376);
            this.peopleAmountLabel.Name = "peopleAmountLabel";
            this.peopleAmountLabel.Size = new System.Drawing.Size(109, 20);
            this.peopleAmountLabel.TabIndex = 22;
            this.peopleAmountLabel.Text = "People amount";
            // 
            // peopleAmountTextBox
            // 
            this.peopleAmountTextBox.Location = new System.Drawing.Point(399, 375);
            this.peopleAmountTextBox.Name = "peopleAmountTextBox";
            this.peopleAmountTextBox.ReadOnly = true;
            this.peopleAmountTextBox.Size = new System.Drawing.Size(249, 25);
            this.peopleAmountTextBox.TabIndex = 21;
            // 
            // totalPriceLabel
            // 
            this.totalPriceLabel.AutoSize = true;
            this.totalPriceLabel.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.totalPriceLabel.Location = new System.Drawing.Point(228, 678);
            this.totalPriceLabel.Name = "totalPriceLabel";
            this.totalPriceLabel.Size = new System.Drawing.Size(79, 20);
            this.totalPriceLabel.TabIndex = 24;
            this.totalPriceLabel.Text = "Total price";
            // 
            // totalPriceTextBox
            // 
            this.totalPriceTextBox.Location = new System.Drawing.Point(399, 677);
            this.totalPriceTextBox.Name = "totalPriceTextBox";
            this.totalPriceTextBox.ReadOnly = true;
            this.totalPriceTextBox.Size = new System.Drawing.Size(249, 25);
            this.totalPriceTextBox.TabIndex = 23;
            // 
            // additionalLabel
            // 
            this.additionalLabel.AutoSize = true;
            this.additionalLabel.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.additionalLabel.Location = new System.Drawing.Point(169, 477);
            this.additionalLabel.Name = "additionalLabel";
            this.additionalLabel.Size = new System.Drawing.Size(88, 21);
            this.additionalLabel.TabIndex = 27;
            this.additionalLabel.Text = "Additional";
            // 
            // servicesLabel
            // 
            this.servicesLabel.AutoSize = true;
            this.servicesLabel.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.servicesLabel.Location = new System.Drawing.Point(228, 508);
            this.servicesLabel.Name = "servicesLabel";
            this.servicesLabel.Size = new System.Drawing.Size(62, 20);
            this.servicesLabel.TabIndex = 29;
            this.servicesLabel.Text = "Services";
            // 
            // servicesPanel
            // 
            this.servicesPanel.AutoScroll = true;
            this.servicesPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.servicesPanel.Location = new System.Drawing.Point(399, 517);
            this.servicesPanel.MaximumSize = new System.Drawing.Size(249, 120);
            this.servicesPanel.Name = "servicesPanel";
            this.servicesPanel.Size = new System.Drawing.Size(249, 120);
            this.servicesPanel.TabIndex = 30;
            // 
            // finalLabel
            // 
            this.finalLabel.AutoSize = true;
            this.finalLabel.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.finalLabel.Location = new System.Drawing.Point(169, 642);
            this.finalLabel.Name = "finalLabel";
            this.finalLabel.Size = new System.Drawing.Size(47, 21);
            this.finalLabel.TabIndex = 31;
            this.finalLabel.Text = "Final";
            // 
            // nightsLabel
            // 
            this.nightsLabel.AutoSize = true;
            this.nightsLabel.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nightsLabel.Location = new System.Drawing.Point(228, 407);
            this.nightsLabel.Name = "nightsLabel";
            this.nightsLabel.Size = new System.Drawing.Size(52, 20);
            this.nightsLabel.TabIndex = 33;
            this.nightsLabel.Text = "Nights";
            // 
            // nightsTextBox
            // 
            this.nightsTextBox.Location = new System.Drawing.Point(399, 406);
            this.nightsTextBox.Name = "nightsTextBox";
            this.nightsTextBox.ReadOnly = true;
            this.nightsTextBox.Size = new System.Drawing.Size(249, 25);
            this.nightsTextBox.TabIndex = 32;
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.priceLabel.Location = new System.Drawing.Point(228, 438);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(75, 20);
            this.priceLabel.TabIndex = 35;
            this.priceLabel.Text = "Tour price";
            // 
            // priceTextBox
            // 
            this.priceTextBox.Location = new System.Drawing.Point(399, 437);
            this.priceTextBox.Name = "priceTextBox";
            this.priceTextBox.ReadOnly = true;
            this.priceTextBox.Size = new System.Drawing.Size(249, 25);
            this.priceTextBox.TabIndex = 34;
            // 
            // submitButton
            // 
            this.submitButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.submitButton.Location = new System.Drawing.Point(544, 718);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(104, 30);
            this.submitButton.TabIndex = 36;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // orderPicture2
            // 
            this.orderPicture2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.orderPicture2.Image = ((System.Drawing.Image)(resources.GetObject("orderPicture2.Image")));
            this.orderPicture2.Location = new System.Drawing.Point(601, 44);
            this.orderPicture2.Name = "orderPicture2";
            this.orderPicture2.Size = new System.Drawing.Size(135, 164);
            this.orderPicture2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.orderPicture2.TabIndex = 38;
            this.orderPicture2.TabStop = false;
            // 
            // orderPicture3
            // 
            this.orderPicture3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.orderPicture3.Image = ((System.Drawing.Image)(resources.GetObject("orderPicture3.Image")));
            this.orderPicture3.Location = new System.Drawing.Point(504, 718);
            this.orderPicture3.Name = "orderPicture3";
            this.orderPicture3.Size = new System.Drawing.Size(34, 30);
            this.orderPicture3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.orderPicture3.TabIndex = 39;
            this.orderPicture3.TabStop = false;
            // 
            // RequestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(765, 386);
            this.Controls.Add(this.orderPicture3);
            this.Controls.Add(this.orderPicture2);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.priceLabel);
            this.Controls.Add(this.priceTextBox);
            this.Controls.Add(this.nightsLabel);
            this.Controls.Add(this.nightsTextBox);
            this.Controls.Add(this.finalLabel);
            this.Controls.Add(this.servicesPanel);
            this.Controls.Add(this.servicesLabel);
            this.Controls.Add(this.additionalLabel);
            this.Controls.Add(this.totalPriceLabel);
            this.Controls.Add(this.totalPriceTextBox);
            this.Controls.Add(this.peopleAmountLabel);
            this.Controls.Add(this.peopleAmountTextBox);
            this.Controls.Add(this.transportLabel);
            this.Controls.Add(this.transportTextBox);
            this.Controls.Add(this.resortLabel);
            this.Controls.Add(this.resortTextBox);
            this.Controls.Add(this.hotelLabel);
            this.Controls.Add(this.hotelTextBox);
            this.Controls.Add(this.departureDateLabel);
            this.Controls.Add(this.departureDateTextBox);
            this.Controls.Add(this.tourLabel);
            this.Controls.Add(this.tourTextBox);
            this.Controls.Add(this.tourInfoLabel);
            this.Controls.Add(this.emailLabel);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(this.usernameLabel);
            this.Controls.Add(this.usernameTextBox);
            this.Controls.Add(this.captionUserInfoLabel);
            this.Controls.Add(this.captionLabel);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(781, 425);
            this.MinimumSize = new System.Drawing.Size(781, 425);
            this.Name = "RequestForm";
            this.Text = "Paradise - request";
            this.Load += new System.EventHandler(this.RequestForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.orderPicture2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderPicture3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label captionLabel;
        private System.Windows.Forms.Label captionUserInfoLabel;
        private System.Windows.Forms.TextBox usernameTextBox;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Label emailLabel;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.Label tourInfoLabel;
        private System.Windows.Forms.Label tourLabel;
        private System.Windows.Forms.TextBox tourTextBox;
        private System.Windows.Forms.Label departureDateLabel;
        private System.Windows.Forms.TextBox departureDateTextBox;
        private System.Windows.Forms.Label hotelLabel;
        private System.Windows.Forms.TextBox hotelTextBox;
        private System.Windows.Forms.Label resortLabel;
        private System.Windows.Forms.TextBox resortTextBox;
        private System.Windows.Forms.Label transportLabel;
        private System.Windows.Forms.TextBox transportTextBox;
        private System.Windows.Forms.Label peopleAmountLabel;
        private System.Windows.Forms.TextBox peopleAmountTextBox;
        private System.Windows.Forms.Label totalPriceLabel;
        private System.Windows.Forms.TextBox totalPriceTextBox;
        private System.Windows.Forms.Label additionalLabel;
        private System.Windows.Forms.Label servicesLabel;
        private System.Windows.Forms.Panel servicesPanel;
        private System.Windows.Forms.Label finalLabel;
        private System.Windows.Forms.Label nightsLabel;
        private System.Windows.Forms.TextBox nightsTextBox;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.TextBox priceTextBox;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.PictureBox orderPicture2;
        private System.Windows.Forms.PictureBox orderPicture3;
    }
}