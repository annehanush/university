﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TouristCompany.Common;

namespace TouristCompany.WindowsFormsApplication.Forms
{
    public partial class WorkingForm : Form
    {
        // language of the programm
        public string selectedLanguage = ApplicationParameters.selectedLanguage;

        public WorkingForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Info, what we gonna work with username.
        /// </summary>
        public void WorkWithUserName()
        {
            if (selectedLanguage == "en")
            {
                captionLabel.Text = "Username";
                infoLabel.Text = "Enter new value here...";
            }
            else
            {
                captionLabel.Text = "Имя пользователя";
                infoLabel.Text = "Введите новое значение...";
            }
        }

        /// <summary>
        /// Info, what we gonna work with full name.
        /// </summary>
        public void WorkWithFullName()
        {
            if (selectedLanguage == "en")
            {
                captionLabel.Text = "Full name";
                infoLabel.Text = "Enter new value here...";
            }
            else
            {
                captionLabel.Text = "ФИО";
                infoLabel.Text = "Введите новое значение...";
            }
        }

        /// <summary>
        /// Info, what we gonna work with date of birth.
        /// </summary>
        public void WorkWithDateOfBirth()
        {
            if (selectedLanguage == "en")
            {
                captionLabel.Text = "Date of birth";
                infoLabel.Text = "Enter new value here...";
            }
            else
            {
                captionLabel.Text = "Дата рождения";
                infoLabel.Text = "Введите новое значение...";
            }
        }

        /// <summary>
        /// Info, what we gonna work with email.
        /// </summary>
        public void WorkWithEmail()
        {
            if (selectedLanguage == "en")
            {
                captionLabel.Text = "Email";
                infoLabel.Text = "Enter new value here...";
            }
            else
            {
                captionLabel.Text = "Почта";
                infoLabel.Text = "Введите новое значение...";
            }
        }

        /// <summary>
        /// Info, what we gonna work with phone number.
        /// </summary>
        public void WorkWithPhone()
        {
            if (selectedLanguage == "en")
            {
                captionLabel.Text = "Phone number";
                infoLabel.Text = "Enter new value here...";
            }
            else
            {
                captionLabel.Text = "Номер телефона";
                infoLabel.Text = "Введите новое значение...";
            }
        }

        /// <summary>
        /// Info, what we gonna work with password.
        /// </summary>
        public void WorkWithPassword()
        {
            newValueTextBox.PasswordChar = '*';

            if (selectedLanguage == "en")
            {
                captionLabel.Text = "Password";
                infoLabel.Text = "Enter new value here...";
            }
            else
            {
                captionLabel.Text = "Пароль";
                infoLabel.Text = "Введите новое значение...";
            }
        }

        /// <summary>
        /// Change user name.
        /// </summary>
        private void ChangeUserName()
        {
            if (newValueTextBox.Text != "")
            {
                bool isCorrect = UserActions.UpdateUsername(newValueTextBox.Text);

                if (isCorrect)
                {
                    if (selectedLanguage == "en")
                    {
                        MessageBox.Show("Username was successfully changed!", "Changing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Имя пользователя было успешно изменено!", "Изменения", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
                    {
                        if (Application.OpenForms[i].Name != "MainForm")
                            Application.OpenForms[i].Close();
                    }
                    PersonalForm personalForm = new PersonalForm();
                    personalForm.Show();
                }
                else
                {
                    if (selectedLanguage == "en")
                    {
                        MessageBox.Show("Such username is not free! Try to choose anothre one.", "Changing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Введенное имя пользователя уже занято! Попробуйте выбрать другое.", "Изменения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
            else
            {
                if (selectedLanguage == "en")
                {
                    MessageBox.Show("You must enter new username!", "Changing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Введите новое имя пользователя!", "Изменения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Change full name.
        /// </summary>
        private void ChangeFullName()
        {
            UserActions.UpdateFullName(newValueTextBox.Text);

            if (selectedLanguage == "en")
            {
                MessageBox.Show("Full name was successfully changed!", "Changing", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("ФИО было успешно изменено!", "Изменения", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
            PersonalForm personalForm = new PersonalForm();
            personalForm.Show();
        }

        /// <summary>
        /// Change date of birth.
        /// </summary>
        private void ChangeDateOfBirth()
        {
            if (newValueTextBox.Text == "")
            {
                UserActions.UpdateDateOfBirthToNull();

                if (selectedLanguage == "en")
                {
                    MessageBox.Show("Date of birth was successfully changed!", "Changing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Дата рождения была успешно изменена!", "Изменения", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
                {
                    if (Application.OpenForms[i].Name != "MainForm")
                        Application.OpenForms[i].Close();
                }
                PersonalForm personalForm = new PersonalForm();
                personalForm.Show();
            }
            else
            {
                try
                {
                    DateTime date = DateTime.Parse(newValueTextBox.Text);
                    UserActions.UpdateDateOfBirth(date);

                    if (selectedLanguage == "en")
                    {
                        MessageBox.Show("Date of birth was successfully changed!", "Changing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Дата рождения была успешно изменена!", "Изменения", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
                    {
                        if (Application.OpenForms[i].Name != "MainForm")
                            Application.OpenForms[i].Close();
                    }
                    PersonalForm personalForm = new PersonalForm();
                    personalForm.Show();
                }
                catch (FormatException)
                {
                    if (selectedLanguage == "en")
                    {
                        MessageBox.Show("Incorrect input for date!\nEnter -- day.month.year --", "Changing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Введена некорректная дата!\nВводите -- день.месяц.год --", "Изменения", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }


        /// <summary>
        /// Change email.
        /// </summary>
        private void ChangeEmail()
        {
            if (newValueTextBox.Text != "")
            {
                UserActions.UpdateEmail(newValueTextBox.Text);

                if (selectedLanguage == "en")
                    {
                    MessageBox.Show("Email was successfully changed!", "Changing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                    else
                    {
                    MessageBox.Show("Адрес почты был успешно изменен!", "Изменения", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
                {
                    if (Application.OpenForms[i].Name != "MainForm")
                        Application.OpenForms[i].Close();
                }
                PersonalForm personalForm = new PersonalForm();
                personalForm.Show();

            }
            else
            {
                if (selectedLanguage == "en")
                {
                    MessageBox.Show("You must enter new email!", "Changing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Введите новый адрес почты!", "Изменения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Change phone number.
        /// </summary>
        private void ChangePhoneNumber()
        {
            UserActions.UpdatePhoneNumber(newValueTextBox.Text);

            if (selectedLanguage == "en")
            {
                MessageBox.Show("Phone number was successfully changed!", "Changing", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Номер телефона был успешно изменен!", "Изменения", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
            PersonalForm personalForm = new PersonalForm();
            personalForm.Show();
        }

        /// <summary>
        /// Change password.
        /// </summary>
        private void ChangePassword()
        {
            if (newValueTextBox.Text != "")
            {
                UserActions.UpdatePassword(newValueTextBox.Text);

                if (selectedLanguage == "en")
                {
                    MessageBox.Show("Password was successfully changed!", "Changing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Пароль был успешно изменен!", "Изменения", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
                {
                    if (Application.OpenForms[i].Name != "MainForm")
                        Application.OpenForms[i].Close();
                }
                PersonalForm personalForm = new PersonalForm();
                personalForm.Show();
            }
            else
            {
                if (selectedLanguage == "en")
                {
                    MessageBox.Show("You must enter new password!", "Changing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Введите новый пароль!", "Изменения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Change personal information.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void changeButton_Click(object sender, EventArgs e)
        {
            if (captionLabel.Text == "Username" || captionLabel.Text == "Имя пользователя")
            {
                ChangeUserName();
            }
            else if (captionLabel.Text == "Full name" || captionLabel.Text == "ФИО")
            {
                ChangeFullName();
            }
            else if (captionLabel.Text == "Date of birth" || captionLabel.Text == "Дата рождения")
            {
                ChangeDateOfBirth();
            }
            else if (captionLabel.Text == "Email" || captionLabel.Text == "Почта")
            {
                ChangeEmail();
            }
            else if (captionLabel.Text == "Phone number" || captionLabel.Text == "Номер телефона")
            {
                ChangePhoneNumber();
            }
            else if (captionLabel.Text == "Password" || captionLabel.Text == "Пароль")
            {
                ChangePassword();
            }
        }
    }
}
