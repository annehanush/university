﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TouristCompany.Common;
using TouristCompany.Data.Entities;

namespace TouristCompany.WindowsFormsApplication.Forms
{
    public partial class CountriesForm : Form
    {
        public string selectedLanguage = "en";

        // Exemplar of context class
        TouristCompanyDbContext db = new TouristCompanyDbContext();
        static List<Control> cards = new List<Control>();
        static List<Button> buttons = new List<Button>();
        static bool isBig = false;

        public CountriesForm()
        {
            InitializeComponent();
            selectedLanguage = ApplicationParameters.selectedLanguage;
        }

        public void ChangeMenu()
        {
            if (UserActions._username != "" && UserActions._userId != -1)
            {
                signUpToolStripMenuItem.Visible = false;
                logInToolStripMenuItem.Visible = false;
                profileToolStripMenuItem.Visible = true;
                profileToolStripMenuItem.Margin = new Padding(100, 0, 0, 0);
            }

        }

        /// <summary>
        /// Check of what language was selected.
        /// </summary>
        private void CheckLanguage()
        {
            if (selectedLanguage == "ru")
            {
                countriesToolStripMenuItem.Text = "Страны";
                tourSelectionToolStripMenuItem.Text = "Подбор тура";
                contactToolStripMenuItem.Text = "Контакты";
                fAQToolStripMenuItem.Text = "Ч.З.В";

                profileToolStripMenuItem.Text = "Профиль";
                profileInfoToolStripMenuItem.Text = "О профиле";
                logOutToolStripMenuItem.Text = "Выйти";

                signUpToolStripMenuItem.Text = "Регистрация";
                if (this.Width > 781)
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 11);
                }
                else
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 8);
                }
                logInToolStripMenuItem.Text = "Вход";

                foreach (var item in buttons)
                {
                    item.Text = "УЗНАТЬ БОЛЬШЕ";
                }
            }
        }

        /// <summary>
        /// Two types of resizing (from small to big and vice versa).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CountriesForm_Resize(object sender, EventArgs e)
        {
            if (this.Width > 781)
            {
                countriesToolStripMenuItem.Font = new Font("Segoe UI", 13);
                tourSelectionToolStripMenuItem.Font = new Font("Segoe UI", 13);
                contactToolStripMenuItem.Font = new Font("Segoe UI", 13);
                fAQToolStripMenuItem.Font = new Font("Segoe UI", 13);
                if (selectedLanguage == "ru")
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 11);
                }
                else
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 13);
                }
                logInToolStripMenuItem.Font = new Font("Segoe UI", 13);
                profileToolStripMenuItem.Font = new Font("Segoe UI", 13);

                paradiseToolStripMenuItem.Font = new Font("Segoe UI", 22, FontStyle.Bold);
                countriesToolStripMenuItem.Margin = new Padding(510, 0, 0, 0);
                signUpToolStripMenuItem.Margin = new Padding(95, 0, 0, 0);

                if (profileToolStripMenuItem.Visible)
                {
                    profileToolStripMenuItem.Margin = new Padding(160, 0, 0, 0);
                }

                if (!isBig)
                {
                    foreach (var card in cards)
                    {
                        Controls.Remove(card);
                    }
                    cards.Clear();

                    BigCountriesCards();
                }
            }
            else
            {
                countriesToolStripMenuItem.Font = new Font("Segoe UI", 10);
                tourSelectionToolStripMenuItem.Font = new Font("Segoe UI", 10);
                contactToolStripMenuItem.Font = new Font("Segoe UI", 10);
                fAQToolStripMenuItem.Font = new Font("Segoe UI", 10);
                if (selectedLanguage == "ru")
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 8);
                }
                else
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 10);
                }
                logInToolStripMenuItem.Font = new Font("Segoe UI", 10);
                profileToolStripMenuItem.Font = new Font("Segoe UI", 10);

                if (profileToolStripMenuItem.Visible)
                {
                    profileToolStripMenuItem.Margin = new Padding(100, 0, 0, 0);
                }

                paradiseToolStripMenuItem.Font = new Font("Segoe UI", 14, FontStyle.Bold);
                countriesToolStripMenuItem.Margin = new Padding(130, 0, 0, 0);
                signUpToolStripMenuItem.Margin = new Padding(35, 0, 0, 0);

                if (isBig)
                {
                    foreach (var card in cards)
                    {
                        Controls.Remove(card);
                    }
                    cards.Clear();

                    SmallCountriesCards();
                }
            }
        }

        /// <summary>
        /// Form load action. Countries cards (small) are loading to the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CountriesForm_Load(object sender, EventArgs e)
        {
            SmallCountriesCards();
            CheckLanguage();
            ChangeMenu();
        }

        /// <summary>
        /// Action for loading small countries cards to the app form.
        /// </summary>
        private void SmallCountriesCards()
        {
            int countriesAmount = db.Countries.Count();
            int ost = countriesAmount % 3;
            if (ost == 2)
            {
                ost = 1;
            }
            int stringsAmount = countriesAmount / 3 + ost;
            IEnumerable<Country> _countries = db.Countries;
            List<Country> _consistsCountries = new List<Country>();

            if (countriesAmount > 3)
            {
                int count = 0, x = 77, y = 77;

                for (int i = 0; i < stringsAmount; i++)
                {
                    foreach (var country in _countries)
                    {
                        if (count < 3)
                        {
                            if (!_consistsCountries.Contains(country))
                            {
                                string path = @"E:\Study\Database - Course (6 semester)\images\countries\" + country.Name + ".jpg";

                                PictureBox picture = new PictureBox();
                                picture.Size = new Size(166, 198);
                                picture.Location = new Point(x, y);
                                picture.Image = Image.FromFile(path);

                                Label pictureLabelName = new Label();
                                pictureLabelName.Text = country.Name.ToUpper();
                                pictureLabelName.Font = new Font("Segoe UI", 14, FontStyle.Bold);
                                pictureLabelName.BackColor = System.Drawing.Color.Transparent;
                                pictureLabelName.Location = new Point(37, 85);
                                pictureLabelName.TextAlign = ContentAlignment.MiddleCenter;

                                Label pictureLabelFrom = new Label();
                                pictureLabelFrom.Text = "FROM";
                                pictureLabelFrom.Font = new Font("Segoe UI", 8, FontStyle.Bold);
                                pictureLabelFrom.BackColor = System.Drawing.Color.Transparent;
                                pictureLabelFrom.Location = new Point(37, 112);
                                pictureLabelFrom.TextAlign = ContentAlignment.MiddleCenter;

                                Label pictureLabelPrice = new Label();
                                pictureLabelPrice.Text = "1000" + " $";
                                pictureLabelPrice.Font = new Font("Segoe UI", 14, FontStyle.Bold);
                                pictureLabelPrice.BackColor = System.Drawing.Color.Transparent;
                                pictureLabelPrice.Location = new Point(37, 128);
                                pictureLabelPrice.TextAlign = ContentAlignment.MiddleCenter;

                                Button pictureButton = new Button();
                                pictureButton.Text = "LEARN MORE";
                                pictureButton.BackColor = System.Drawing.Color.WhiteSmoke;
                                pictureButton.ForeColor = System.Drawing.Color.IndianRed;
                                pictureButton.Font = new Font("Segoe UI", 9);
                                pictureButton.Location = new Point(30, 160);
                                pictureButton.Size = new Size(111, 29);
                                pictureButton.TextAlign = ContentAlignment.MiddleCenter;

                                buttons.Add(pictureButton);


                                cards.Add(picture);
                                Controls.Add(picture);
                                picture.Controls.Add(pictureLabelName);
                                picture.Controls.Add(pictureLabelFrom);
                                picture.Controls.Add(pictureLabelPrice);
                                picture.Controls.Add(pictureButton);
                                _consistsCountries.Add(country);
                                x = x + 222;
                                count++;
                            }
                        }
                        else if (count == 3)
                        {
                            count = 0;
                            y += 223;
                            x = 77;
                            break;
                        }
                    }
                }
            }
            else
            {
                int x = 77;
                foreach (var country in _countries)
                {
                    PictureBox picture = new PictureBox();
                    picture.Size = new Size(166, 198);
                    picture.Location = new Point(x, 77);
                    picture.Image = Image.FromFile(@"e:\WEB\try\img\ban_img1.jpg");

                    cards.Add(picture);
                    Controls.Add(picture);
                    x = x + 222;
                }
            }
            isBig = false;
        }

        /// <summary>
        /// Action for loading big countries cards to the app form.
        /// </summary>
        private void BigCountriesCards()
        {
            int countriesAmount = db.Countries.Count();
            IEnumerable<Country> _countries = db.Countries;
            int ost = countriesAmount % 3;
            if (ost == 2)
            {
                ost = 1;
            }
            int stringsAmount = countriesAmount / 3 + ost;
            List<Country> _consistsCountries = new List<Country>();

            if (countriesAmount > 3)
            {

                int count = 0, x = 150, y = 150;

                for (int i = 0; i < stringsAmount; i++)
                {
                    foreach (var country in _countries)
                    {
                        if (count < 3)
                        {
                            if (!_consistsCountries.Contains(country))
                            {
                                string path = @"E:\Study\Database - Course (6 semester)\images\countries\" + country.Name + ".jpg";

                                PictureBox picture = new PictureBox();
                                picture.Size = new Size(270, 330);
                                picture.Location = new Point(x, y);
                                picture.Image = Image.FromFile(path);

                                Label pictureLabel = new Label();
                                pictureLabel.Text = country.Name.ToUpper();
                                pictureLabel.Font = new Font("Segoe UI", 14, FontStyle.Bold);
                                pictureLabel.BackColor = System.Drawing.Color.Transparent;
                                pictureLabel.Location = new Point(85, 260);
                                pictureLabel.TextAlign = ContentAlignment.MiddleCenter;

                                cards.Add(picture);
                                Controls.Add(picture);
                                picture.Controls.Add(pictureLabel);
                                _consistsCountries.Add(country);
                                x = x + 400;
                                count++;
                            }
                        }
                        else if (count == 3)
                        {
                            count = 0;
                            y += 401;
                            x = 150;
                            break;
                        }
                    }
                }
            }
            else
            {
                int x = 150;
                foreach (var country in _countries)
                {
                    PictureBox picture = new PictureBox();
                    picture.Size = new Size(270, 330);
                    picture.Location = new Point(x, 150);
                    picture.Image = Image.FromFile(@"e:\WEB\try\img\ban_img1.jpg");

                    cards.Add(picture);
                    Controls.Add(picture);
                    x = x + 400;
                }
            }

            isBig = true;
        }

        private void signUpToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            bool opened = false;
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name == "SignUpForm")
                {
                    opened = true;
                    if (selectedLanguage == "en")
                    {
                        MessageBox.Show("Sign up window is already opened.");
                    }
                    else
                    {
                        MessageBox.Show("Окно регистрации уже открыто.");
                    }
                }
            }

            if (!opened)
            {
                SignUpForm _signUpForm = new SignUpForm();
                _signUpForm.Show();
            }
        }

        private void logInToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            bool opened = false;
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name == "LoginForm")
                {
                    opened = true;
                    if (selectedLanguage == "en")
                    {
                        MessageBox.Show("Login window is already opened.");
                    }
                    else
                    {
                        MessageBox.Show("Окно входа уже открыто.");
                    }
                }
            }

            if (!opened)
            {
                LoginForm _loginForm = new LoginForm();
                _loginForm.Show();
            }
        }

        private void fAQToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void countriesToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
            CountriesForm _countriesForm = new CountriesForm();
            _countriesForm.Show();
        }

        private void tourSelectionToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
            TourSelectionForm _tourSelectionForm = new TourSelectionForm();
            _tourSelectionForm.Show();
        }

        private void paradiseToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserActions._username = "";
            UserActions._userId = -1;
            UserActions._userRole = "";

            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name == "MainForm")
                {
                    var mainForm = Application.OpenForms.OfType<MainForm>().Single();
                    mainForm.ChangeMenu();
                }
                else
                {
                    Application.OpenForms[i].Close();
                }
            }
        }

        private void profileInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
            PersonalForm personalForm = new PersonalForm();
            personalForm.Show();
        }
    }
}
