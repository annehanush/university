﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TouristCompany.Common;

namespace TouristCompany.WindowsFormsApplication.Forms
{
    public partial class RequestForm : Form
    {
        // language of the programm
        public string selectedLanguage = ApplicationParameters.selectedLanguage;

        public RequestForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Check of the language of the programm.
        /// </summary>
        private void CheckLanguage()
        {
            if (selectedLanguage == "ru")
            {
                captionLabel.Text = "ОФОРМЛЕНИЕ ЗАЯВКИ НА ЗАКАЗ ТУРА";

                captionUserInfoLabel.Text = "Данные пользователя";
                tourInfoLabel.Text = "Данные о туре";
                additionalLabel.Text = "Допольнительно";
                finalLabel.Text = "Итог";

                usernameLabel.Text = "Имя пользователя";
                emailLabel.Text = "Почта";
                tourLabel.Text = "Тур";
                departureDateLabel.Text = "Отправление";
                resortLabel.Text = "Курорт";
                hotelLabel.Text = "Отель";
                transportLabel.Text = "Транспорт";
                peopleAmountLabel.Text = "Количество человек";
                nightsLabel.Text = "Ночей в туре";
                priceLabel.Text = "Стоимость";
                servicesLabel.Text = "Услуги";
                totalPriceLabel.Text = "Общая стоимость";

                submitButton.Text = "Отправить";
            }
        }

        /// <summary>
        /// Fill services panel.
        /// </summary>
        private void FillServices()
        {
            RequestActions.FillServices();
            int count = 0;
            foreach (var _service in RequestActions._services)
            {
                CheckBox service = new CheckBox();
                service.CheckedChanged += service_CheckedChanged;
                service.Text = _service;
                service.Location = new Point(4, 23 * count + 2);
                service.Size = new Size(180, 21);

                servicesPanel.Controls.Add(service);
                count++;
            }
        }

        /// <summary>
        /// Fill all info about tour.
        /// </summary>
        private void FillTourInfo()
        {
            RequestActions.FillTourInfo();

            tourTextBox.Text = RequestActions._tourInfo.Tour;
            departureDateTextBox.Text = RequestActions._tourInfo.DepartureDate;
            resortTextBox.Text = RequestActions._tourInfo.Resort;
            hotelTextBox.Text = RequestActions._tourInfo.Hotel;
            transportTextBox.Text = RequestActions._tourInfo.Transport;
            peopleAmountTextBox.Text = RequestActions._tourInfo.People.ToString(); ;
            nightsTextBox.Text = RequestActions._tourInfo.Nights.ToString();
            if (TourSelectionActions._selectedCurrency == "USD")
            {
                priceTextBox.Text = RequestActions._tourInfo.Price + " USD";
            }
            else
            {
                priceTextBox.Text = (Decimal.Parse(RequestActions._tourInfo.Price) * 2).ToString() + " BYN";
            }

            // fill default total cost
            if (TourSelectionActions._selectedCurrency == "USD")
            {
                totalPriceTextBox.Text = RequestActions._totalCost + " USD";
            }
            else
            {
                totalPriceTextBox.Text = (RequestActions._totalCost * 2).ToString() + " BYN";
            }
        }

        /// <summary>
        /// Change total cost (when choose/not choose service)
        /// </summary>
        private void ChangeTotalCost()
        {
            RequestActions.TotalCostChange();

            if (TourSelectionActions._selectedCurrency == "USD")
            {
                totalPriceTextBox.Text = RequestActions._totalCost + " USD";
            }
            else
            {
                totalPriceTextBox.Text = (RequestActions._totalCost * 2).ToString() + " BYN";
            }
        }

        /// <summary>
        /// Form load.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RequestForm_Load(object sender, EventArgs e)
        {
            CheckLanguage();

            usernameTextBox.Text = UserActions._username;

            RequestActions.GetUserEmail();
            emailTextBox.Text = RequestActions._userEmail;

            FillTourInfo();
            FillServices();
        }

        /// <summary>
        /// Service selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void service_CheckedChanged(object sender, EventArgs e)
        {
            foreach (CheckBox service in servicesPanel.Controls)
            {
                if (service.Checked)
                {
                    if (!RequestActions._selectedServices.Contains(service.Text))
                    {
                        RequestActions._selectedServices.Add(service.Text);

                        ChangeTotalCost();
                    }
                }
                else
                {
                    if (RequestActions._selectedServices.Contains(service.Text))
                    {
                        RequestActions._selectedServices.Remove(service.Text);

                        ChangeTotalCost();
                    }
                }
            }
        }

        /// <summary>
        /// Request making.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void submitButton_Click(object sender, EventArgs e)
        {
            RequestActions.MakeRequest();
            if (selectedLanguage == "en")
            {
                MessageBox.Show("Tour request has been sent.\nTour agent will connect you soon.", "Request making", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Ваша заявка была отправлена.\nПредставители фирмы свяжутся с Вами в ближайшее время.", "Оформление заявки", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
