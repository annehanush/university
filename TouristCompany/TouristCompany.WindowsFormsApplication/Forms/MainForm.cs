﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TouristCompany.Common;
using TouristCompany.WindowsFormsApplication.Forms;

namespace TouristCompany.WindowsFormsApplication
{
    public partial class MainForm : Form
    {
        public string selectedLanguage = ApplicationParameters.selectedLanguage;

        public MainForm()
        {
            InitializeComponent();
        }

        public void ChangeMenu()
        {
            if (UserActions._username != "" && UserActions._userId != -1)
            {
                signUpToolStripMenuItem.Visible = false;
                logInToolStripMenuItem.Visible = false;
                profileToolStripMenuItem.Visible = true;
                profileToolStripMenuItem.Margin = new Padding(100, 0, 0, 0);
            }
            else
            {
                signUpToolStripMenuItem.Visible = true;
                logInToolStripMenuItem.Visible = true;
                profileToolStripMenuItem.Visible = false;
            }
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (this.Width > 781)
            {
                countriesToolStripMenuItem.Font = new Font("Segoe UI", 13);
                tourSelectionToolStripMenuItem.Font = new Font("Segoe UI", 13);
                contactToolStripMenuItem.Font = new Font("Segoe UI", 13);
                fAQToolStripMenuItem.Font = new Font("Segoe UI", 13);
                if (selectedLanguage == "ru")
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 11);
                }
                else
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 13);
                }
                logInToolStripMenuItem.Font = new Font("Segoe UI", 13);
                langToolStripMenuItem.Font = new Font("Segoe UI", 13);
                profileToolStripMenuItem.Font = new Font("Segoe UI", 13);

                paradiseToolStripMenuItem.Font = new Font("Segoe UI", 22, FontStyle.Bold);
                countriesToolStripMenuItem.Margin = new Padding(490, 0, 0, 0);
                signUpToolStripMenuItem.Margin = new Padding(85, 0, 0, 0);

                if (profileToolStripMenuItem.Visible)
                {
                    profileToolStripMenuItem.Margin = new Padding(165, 0, 0, 0);
                }

                welcomeLabel.Font = new Font("Segoe UI", 36, FontStyle.Italic);
                welcomeLabel.Dock = DockStyle.None;
                welcomeLabel.Top = 265;
                welcomeLabel.Left = 310;
                welcomLabelBigger.Font = new Font("Segoe UI", 46, FontStyle.Bold);
                welcomLabelBigger.Top = 470;
                welcomLabelBigger.Left = 320;
            }
            else
            {
                countriesToolStripMenuItem.Font = new Font("Segoe UI", 10);
                tourSelectionToolStripMenuItem.Font = new Font("Segoe UI", 10);
                contactToolStripMenuItem.Font = new Font("Segoe UI", 10);
                fAQToolStripMenuItem.Font = new Font("Segoe UI", 10);
                if (selectedLanguage == "ru")
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 8);
                }
                else
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 10);
                }
                logInToolStripMenuItem.Font = new Font("Segoe UI", 10);
                langToolStripMenuItem.Font = new Font("Segoe UI", 10);
                profileToolStripMenuItem.Font = new Font("Segoe UI", 10);

                paradiseToolStripMenuItem.Font = new Font("Segoe UI", 14, FontStyle.Bold);
                countriesToolStripMenuItem.Margin = new Padding(120, 0, 0, 0);
                signUpToolStripMenuItem.Margin = new Padding(25, 0, 0, 0);

                if (profileToolStripMenuItem.Visible)
                {
                    profileToolStripMenuItem.Margin = new Padding(100, 0, 0, 0);
                }

                welcomeLabel.Font = new Font("Segoe UI", 26, FontStyle.Italic);
                welcomeLabel.Dock = DockStyle.Fill;
                welcomLabelBigger.Font = new Font("Segoe UI", 32, FontStyle.Bold);
                welcomLabelBigger.Top = 232;
                welcomLabelBigger.Left = 130;
            }
        }

        private void countriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
            CountriesForm _countriesForm = new CountriesForm();
            _countriesForm.Show();
        }

        private void tourSelectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
            TourSelectionForm _tourSelectionForm = new TourSelectionForm();
            _tourSelectionForm.Show();
        }

        private void logInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool opened = false;
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name == "LoginForm")
                {
                    opened = true;
                    if (selectedLanguage == "en")
                    {
                        MessageBox.Show("Login window is already opened.");
                    }
                    else
                    {
                        MessageBox.Show("Окно входа уже открыто.");
                    }
                }
            }

            if (!opened)
            {
                LoginForm _loginForm = new LoginForm();
                _loginForm.Show();
            }
        }

        private void signUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool opened = false;
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name == "SignUpForm")
                {
                    opened = true;
                    if (selectedLanguage == "en")
                    {
                        MessageBox.Show("Sign up window is already opened.");
                    }
                    else
                    {
                        MessageBox.Show("Окно регистрации уже открыто.");
                    }
                }
            }

            if (!opened)
            {
                SignUpForm _signUpForm = new SignUpForm();
                _signUpForm.Show();
            }
        }

        private void langToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selectedLanguage == "en")
            {
                
                ApplicationParameters.selectedLanguage = "ru";
                selectedLanguage = ApplicationParameters.selectedLanguage;

                langToolStripMenuItem.Text = "en";

                countriesToolStripMenuItem.Text = "Страны";
                tourSelectionToolStripMenuItem.Text = "Подбор тура";
                contactToolStripMenuItem.Text = "Контакты";
                fAQToolStripMenuItem.Text = "Ч.З.В";

                signUpToolStripMenuItem.Text = "Регистрация";
                if (this.Width > 781)
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 11);
                }
                else
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 8);
                }
                logInToolStripMenuItem.Text = "Вход";
                profileToolStripMenuItem.Text = "Профиль";
                profileInfoToolStripMenuItem.Text = "О профиле";
                logOutToolStripMenuItem.Text = "Выйти";

                welcomeLabel.Text = "   Добро пожаловать!";
                welcomLabelBigger.Text = "МЫ РАДЫ ВИДЕТЬ ВАС";
            }
            else
            {
                ApplicationParameters.selectedLanguage = "en";
                selectedLanguage = ApplicationParameters.selectedLanguage;

                langToolStripMenuItem.Text = "ru";

                countriesToolStripMenuItem.Text = "Countries";
                tourSelectionToolStripMenuItem.Text = "Tour selection";
                contactToolStripMenuItem.Text = "Contact";
                fAQToolStripMenuItem.Text = "F.A.Q.";

                signUpToolStripMenuItem.Text = "Sign Up";
                if (this.Width > 781)
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 13);
                }
                else
                {
                    signUpToolStripMenuItem.Font = new Font("Segoe UI", 10);
                }
                logInToolStripMenuItem.Text = "Log In";
                profileToolStripMenuItem.Text = "Profile";
                profileInfoToolStripMenuItem.Text = "Profile info";
                logOutToolStripMenuItem.Text = "Log out";

                welcomeLabel.Text = "Welcome To Our Agency!";
                welcomLabelBigger.Text = "IT'S NICE TO MEET YOU";
            }

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserActions._username = "";
            UserActions._userId = -1;
            UserActions._userRole = "";

            signUpToolStripMenuItem.Visible = true;
            logInToolStripMenuItem.Visible = true;
            profileToolStripMenuItem.Visible = false;
        }

        private void profileInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
            PersonalForm personalForm = new PersonalForm();
            personalForm.Show();
        }
    }
}
