﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TouristCompany.Common;

namespace TouristCompany.WindowsFormsApplication.Forms
{
    public partial class SignUpForm : Form
    {
        public string selectedLanguage = ApplicationParameters.selectedLanguage;

        public SignUpForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Check language.
        /// </summary>
        private void CheckLanguage()
        {
            if (selectedLanguage == "ru")
            {
                countriesToolStripMenuItem.Text = "Страны";
                tourSelectionToolStripMenuItem.Text = "Подбор тура";
                contactToolStripMenuItem.Text = "Контакты";
                fAQToolStripMenuItem.Text = "Ч.З.В";

                captionLabel.Text = "Регистрация";
                usernameLabel.Text = "Имя пользователя *";
                usernameLabel.Location = new Point(304, 136);
                fullnameLabel.Text = "ФИО";
                fullnameLabel.Location = new Point(340, 193);
                emailLabel.Text = "Почта *";
                phoneLabel.Text = "Телефон";
                phoneLabel.Location = new Point(330 ,302);
                passwordLabel.Text = "Пароль *";
                passwordLabel.Location = new Point(333, 355);

                signUpButton.Text = "Зарегистрироваться";
                signUpButton.Size = new Size(145 ,27);
                signUpButton.Location = new Point(285, 418);
            }
        }

        /// <summary>
        /// Form resize.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SignUpForm_Resize(object sender, EventArgs e)
        {
            if (this.Width > 781)
            {
                countriesToolStripMenuItem.Font = new Font("Segoe UI", 13);
                tourSelectionToolStripMenuItem.Font = new Font("Segoe UI", 13);
                contactToolStripMenuItem.Font = new Font("Segoe UI", 13);
                fAQToolStripMenuItem.Font = new Font("Segoe UI", 13);

                paradiseToolStripMenuItem.Font = new Font("Segoe UI", 22, FontStyle.Bold);
                countriesToolStripMenuItem.Margin = new Padding(790, 0, 0, 0);

                if (selectedLanguage == "ru")
                {
                    signUpButton.Size = new Size(145, 27);
                }
                else
                {
                    signUpButton.Size = new Size(82, 27);
                }
            }
            else
            {
                countriesToolStripMenuItem.Font = new Font("Segoe UI", 10);
                tourSelectionToolStripMenuItem.Font = new Font("Segoe UI", 10);
                contactToolStripMenuItem.Font = new Font("Segoe UI", 10);
                fAQToolStripMenuItem.Font = new Font("Segoe UI", 10);

                paradiseToolStripMenuItem.Font = new Font("Segoe UI", 14, FontStyle.Bold);
                countriesToolStripMenuItem.Margin = new Padding(330, 0, 0, 0);

                if (selectedLanguage == "ru")
                {
                    signUpButton.Size = new Size(145, 27);
                    signUpButton.Location = new Point(285, 418);
                }
                else
                {
                    signUpButton.Size = new Size(82, 27);
                    signUpButton.Location = new Point(320, 418);
                }
            }
        }

        /// <summary>
        /// Countries menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void countriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
            CountriesForm _countriesForm = new CountriesForm();
            _countriesForm.Show();
        }

        /// <summary>
        /// Tour selection menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tourSelectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
            TourSelectionForm _tourSelectionForm = new TourSelectionForm();
            _tourSelectionForm.Show();
        }

        /// <summary>
        /// Paradise menu item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void paradiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "MainForm")
                    Application.OpenForms[i].Close();
            }
        }

        /// <summary>
        /// Sign up form load.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SignUpForm_Load(object sender, EventArgs e)
        {
            CheckLanguage();
            this.Size = new Size(781, 425);
            if (selectedLanguage == "ru")
            {
                signUpButton.Location = new Point(285, 418);
            }
            else
            {
                signUpButton.Location = new Point(320, 418);
            }

            // clear all static var-s that are fulling while registration
            UserActions.ClearSignUpData();

            passwordTextBox.PasswordChar = '*';
        }

        /// <summary>
        /// Sign up button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void signUpButton_Click(object sender, EventArgs e)
        {
            if (usernameTextBox.Text != "")
            {
                bool isFree = UserActions.CheckUsernameExistance(usernameTextBox.Text);
                if (isFree)
                {
                    UserActions._enteredUsername = usernameTextBox.Text;
                }
                else
                {
                    if (selectedLanguage == "en")
                    {
                        MessageBox.Show("You must choose another username!\nThere is a user with such username in system.", "Sign Up process", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Выберите другое имя пользователя! Пользователь с таким именем уже существует.", "Регистрация", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                if (selectedLanguage == "en")
                {
                    MessageBox.Show("You must enter username!", "Sign Up process", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Введите имя пользователя! Оно необходимо для регистрации.", "Регистрация", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if (fullnameTextBox.Text != "")
            {
                UserActions._enteredFullName = fullnameTextBox.Text;
            }

            if (emailTextBox.Text != "")
            {
                UserActions._enteredEmail = emailTextBox.Text;
            }
            else
            {
                if (selectedLanguage == "en")
                {
                    MessageBox.Show("You must enter email!", "Sign Up process", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Введите почту! Она необходима для регистрации.", "Регистрация", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if (phoneTextBox.Text != "")
            {
                UserActions._enteredPhone = phoneTextBox.Text;
            }

            if (passwordTextBox.Text != "")
            {
                if (passwordTextBox.Text.Length < 6)
                {
                    if (selectedLanguage == "en")
                    {
                        MessageBox.Show("Password length must be at least 6 characters.", "Sign Up process", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Длина пароля должна быть минимум 6 символов.", "Регистрация", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    UserActions._enteredPassword = passwordTextBox.Text;
                }
            }
            else
            {
                if (selectedLanguage == "en")
                {
                    MessageBox.Show("You must enter password!", "Sign Up process", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Введите пароль! Он необходим для регистрации.", "Регистрация", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }


            // Check if all the parameters were entered correctly.
            bool isReady = UserActions.CheckEnteredParameters();
            if (isReady)
            {
                UserActions.Register();

                if (selectedLanguage == "en")
                {
                    MessageBox.Show("You were successfully registered in system.\nYou will be redirected to Log In page.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Вы были зарегистрированы в системе.\nВы будете перенаправлены на страницу Входа.", "Успешно", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                LoginForm _loginForm = new LoginForm();
                _loginForm.Show();

                for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
                {
                    if (Application.OpenForms[i].Name != "MainForm" && Application.OpenForms[i].Name != "LoginForm")
                    {
                        Application.OpenForms[i].Close();
                    }
                }
            }
        }
    }
}
