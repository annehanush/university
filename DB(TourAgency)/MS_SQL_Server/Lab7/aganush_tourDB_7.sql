ALTER TABLE ���� DROP CONSTRAINT FK_���������_����
ALTER TABLE ������� DROP CONSTRAINT FK_������_�������
ALTER TABLE ������ DROP CONSTRAINT FK_����������_������
ALTER TABLE ������ DROP CONSTRAINT FK_����_������
ALTER TABLE ����� DROP CONSTRAINT FK_�������_�����
ALTER TABLE ���� DROP CONSTRAINT FK_�����_����
ALTER TABLE ������ DROP CONSTRAINT FK_�������_������
ALTER TABLE ������������� DROP CONSTRAINT FK_������_�������������
ALTER TABLE ������������� DROP CONSTRAINT FK_�������������_��������������������
ALTER TABLE �������������������� DROP CONSTRAINT FK_��������������������_����������

GO

-- ������������ ������ �5
-- ��������� ������ � ������� SQL-��������
-- � ������ ������ ���� ����������� ����� SQL-�������� ��� �������� ���� ������ ������������� ���� ������. 
-- ����� � Create Table ������ ���� ���������� �������� ����� �� ���������, ����������� ������������ � ����������� ����������� 
-- (� Create Table ��� ���� ������ ������ ���� ���������� ���� �� �� ������ ����������� ������� ����).


DROP TABLE ���������
GO

CREATE TABLE ��������� 
(
  ��� INT PRIMARY KEY IDENTITY, 
  ������������ VARCHAR(80) NOT NULL,
  ����� VARCHAR(80) DEFAULT '������'
)
GO

DROP TABLE ����
GO

CREATE TABLE ���� 
(
  ��� INT PRIMARY KEY IDENTITY,
  �������� INT NOT NULL,
  ������� VARCHAR(80) NOT NULL,
  ������������� INT NOT NULL,
  ��������� MONEY CHECK (��������� > 0) NOT NULL,
  ����������������� DATE NOT NULL,
  �������������������� DATE NOT NULL
)
GO

DROP TABLE ����������
GO

CREATE TABLE ���������� 
(
  ��� INT PRIMARY KEY IDENTITY,
  ��� VARCHAR(80) NOT NULL,
  ������������ DATE,
  ��������� VARCHAR(50) NOT NULL,
  ������� VARCHAR(15) NOT NULL
)
GO

DROP TABLE ������
GO

CREATE TABLE ������ 
(
  ��� INT PRIMARY KEY IDENTITY,
  ������������� INT NOT NULL,
  ���������� INT NOT NULL,
  �������������� DATE NOT NULL,
  ������� INT NOT NULL,
  ����������������� INT NOT NULL,
  ��������������� DATE NOT NULL,
  �������������� INT NOT NULL,
  ��������� VARCHAR(30)
)
GO

DROP TABLE ������
GO

CREATE TABLE ������ 
(
  ��� INT PRIMARY KEY IDENTITY,
  ������������ VARCHAR(80) UNIQUE NOT NULL,
  ������������� MONEY,
  ������ VARCHAR(50),
  ���� VARCHAR(80) NOT NULL
)
GO

DROP TABLE �������
GO

CREATE TABLE ������� 
(
  ��� INT PRIMARY KEY IDENTITY,
  ��������� INT NOT NULL,
  ������������ VARCHAR(80) NOT NULL,
  �������� VARCHAR(80) NOT NULL,
  �������� VARCHAR(200)
)
GO

DROP TABLE �����
GO

CREATE TABLE ����� 
(
  ��� INT PRIMARY KEY IDENTITY,
  ���������� INT NOT NULL,
  ������� INT NOT NULL,
  ������������ VARCHAR(80) NOT NULL,
  ���������� VARCHAR(80),
  �������� VARCHAR(200)
)
GO

DROP TABLE �������
GO

CREATE TABLE ������� 
(
  ��� INT PRIMARY KEY IDENTITY,
  ��� VARCHAR(80) NOT NULL,
  ������������� VARCHAR(80) NOT NULL,
  ����� VARCHAR(80),
  ������� VARCHAR(15) NOT NULL
)
GO

DROP TABLE �������������
GO

CREATE TABLE ������������� 
(
  ��� INT PRIMARY KEY IDENTITY,
  ������������ INT NOT NULL,
  ��������� INT NOT NULL,
  ��������� MONEY NOT NULL
)
GO

DROP TABLE ��������������������
GO

CREATE TABLE �������������������� 
(
  ��� INT PRIMARY KEY IDENTITY,
  ������������ VARCHAR(80) NOT NULL,
  ������������� INT NOT NULL
)
GO

DROP TABLE ����������
GO

CREATE TABLE ���������� 
(
  ��� INT PRIMARY KEY IDENTITY,
  ������������ VARCHAR(80) NOT NULL,
  ������� VARCHAR(15) NOT NULL,
  ����� VARCHAR(80),
  ������� VARCHAR(80)
)
GO


--������������ ������ �7
--������������ ������ ����� ��������� � ������� SQL ��������
--� ������ ������ ���� ����������� ����� SQL-�������� ��� ����������� ������ ����� ��������� (Alter Table + CONSTRAINT). 
--����� � Alter Table ������ ���� ���������� ����������� ������������ � ����������� ����������� (� Alter Table ��� ���� ������ ������ ���� ���������� ���� �� �� ������ ����������� ������� ����).


ALTER TABLE ���� 
  ADD CONSTRAINT FK_���������_����
  FOREIGN KEY (�������������) REFERENCES ��������� (���)

GO

ALTER TABLE ������� 
  ADD CONSTRAINT FK_������_�������
  FOREIGN KEY (���������) REFERENCES ������ (���)

GO

ALTER TABLE ������ 
  ADD CONSTRAINT FK_����������_������
  FOREIGN KEY (�������������) REFERENCES ���������� (���)

GO

ALTER TABLE ������ 
  ADD CONSTRAINT FK_����_������
  FOREIGN KEY (�������) REFERENCES ���� (���)

GO

ALTER TABLE ����� 
  ADD CONSTRAINT FK_�������_�����
  FOREIGN KEY (����������) REFERENCES ������� (���)

GO

ALTER TABLE ���� 
  ADD CONSTRAINT FK_�����_����
  FOREIGN KEY (��������) REFERENCES ����� (���)

GO

ALTER TABLE ������ 
  ADD CONSTRAINT FK_�������_������
  FOREIGN KEY (����������) REFERENCES ������� (���)

GO

ALTER TABLE ������������� 
  ADD CONSTRAINT FK_������_�������������
  FOREIGN KEY (���������) REFERENCES ������ (���)

GO

ALTER TABLE ������������� 
  ADD CONSTRAINT FK_�������������_��������������������
  FOREIGN KEY (������������) REFERENCES �������������������� (���)

GO

ALTER TABLE �������������������� 
  ADD CONSTRAINT FK_��������������������_����������
  FOREIGN KEY (�������������) REFERENCES ���������� (���)

GO

ALTER TABLE ������������� 
  ADD CHECK (��������� >= 0)

GO

ALTER TABLE ���������� 
  ADD UNIQUE (�������)

GO