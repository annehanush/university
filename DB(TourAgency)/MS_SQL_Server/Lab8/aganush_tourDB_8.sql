ALTER TABLE ���� DROP CONSTRAINT FK_���������_����
ALTER TABLE ������� DROP CONSTRAINT FK_������_�������
ALTER TABLE ������ DROP CONSTRAINT FK_����������_������
ALTER TABLE ������ DROP CONSTRAINT FK_����_������
ALTER TABLE ����� DROP CONSTRAINT FK_�������_�����
ALTER TABLE ���� DROP CONSTRAINT FK_�����_����
ALTER TABLE ������ DROP CONSTRAINT FK_�������_������
ALTER TABLE ������������� DROP CONSTRAINT FK_������_�������������
ALTER TABLE ������������� DROP CONSTRAINT FK_�������������_��������������������
ALTER TABLE �������������������� DROP CONSTRAINT FK_��������������������_����������

GO

-- ������������ ������ �5
-- ��������� ������ � ������� SQL-��������
-- � ������ ������ ���� ����������� ����� SQL-�������� ��� �������� ���� ������ ������������� ���� ������. 
-- ����� � Create Table ������ ���� ���������� �������� ����� �� ���������, ����������� ������������ � ����������� ����������� 
-- (� Create Table ��� ���� ������ ������ ���� ���������� ���� �� �� ������ ����������� ������� ����).

DROP TABLE ���������
GO

CREATE TABLE ��������� 
(
  ��� INT PRIMARY KEY IDENTITY, 
  ������������ VARCHAR(80) NOT NULL,
  ����� VARCHAR(80) DEFAULT '������'
)
GO

DROP TABLE ����
GO

CREATE TABLE ���� 
(
  ��� INT PRIMARY KEY IDENTITY,
  �������� INT NOT NULL,
  ������� VARCHAR(80) NOT NULL,
  ������������� INT NOT NULL,
  ��������� MONEY CHECK (��������� > 0) NOT NULL,
  ����������������� DATE NOT NULL,
  �������������������� DATE NOT NULL
)
GO

DROP TABLE ����������
GO

CREATE TABLE ���������� 
(
  ��� INT PRIMARY KEY IDENTITY,
  ��� VARCHAR(80) NOT NULL,
  ������������ DATE,
  ��������� VARCHAR(50) NOT NULL,
  ������� VARCHAR(15) NOT NULL
)
GO

DROP TABLE ������
GO

CREATE TABLE ������ 
(
  ��� INT PRIMARY KEY IDENTITY,
  ������������� INT NOT NULL,
  ���������� INT NOT NULL,
  �������������� DATE NOT NULL,
  ������� INT NOT NULL,
  ����������������� INT NOT NULL,
  ��������������� DATE NOT NULL,
  �������������� INT NOT NULL,
  ��������� VARCHAR(30)
)
GO

DROP TABLE ������
GO

CREATE TABLE ������ 
(
  ��� INT PRIMARY KEY IDENTITY,
  ������������ VARCHAR(80) UNIQUE NOT NULL,
  ������������� MONEY,
  ������ VARCHAR(50),
  ���� VARCHAR(80) NOT NULL
)
GO

DROP TABLE �������
GO

CREATE TABLE ������� 
(
  ��� INT PRIMARY KEY IDENTITY,
  ��������� INT NOT NULL,
  ������������ VARCHAR(80) NOT NULL,
  �������� VARCHAR(80) NOT NULL,
  �������� VARCHAR(1000)
)
GO

DROP TABLE �����
GO

CREATE TABLE ����� 
(
  ��� INT PRIMARY KEY IDENTITY,
  ���������� INT NOT NULL,
  ������� INT NOT NULL,
  ������������ VARCHAR(80) NOT NULL,
  ���������� VARCHAR(80),
  �������� VARCHAR(1000)
)
GO

DROP TABLE �������
GO

CREATE TABLE ������� 
(
  ��� INT PRIMARY KEY IDENTITY,
  ��� VARCHAR(80) NOT NULL,
  ������������� VARCHAR(80) NOT NULL,
  ����� VARCHAR(80),
  ������� VARCHAR(15) NOT NULL
)
GO

DROP TABLE �������������
GO

CREATE TABLE ������������� 
(
  ��� INT PRIMARY KEY IDENTITY,
  ������������ INT NOT NULL,
  ��������� INT NOT NULL,
  ��������� MONEY NOT NULL
)
GO

DROP TABLE ��������������������
GO

CREATE TABLE �������������������� 
(
  ��� INT PRIMARY KEY IDENTITY,
  ������������ VARCHAR(80) NOT NULL,
  ������������� INT NOT NULL
)
GO

DROP TABLE ����������
GO

CREATE TABLE ���������� 
(
  ��� INT PRIMARY KEY IDENTITY,
  ������������ VARCHAR(80) NOT NULL,
  ������� VARCHAR(15) NOT NULL,
  ����� VARCHAR(80),
  ������� VARCHAR(80)
)
GO


--������������ ������ �7
--������������ ������ ����� ��������� � ������� SQL ��������
--� ������ ������ ���� ����������� ����� SQL-�������� ��� ����������� ������ ����� ��������� (Alter Table + CONSTRAINT). 
--����� � Alter Table ������ ���� ���������� ����������� ������������ � ����������� ����������� (� Alter Table ��� ���� ������ ������ ���� ���������� ���� �� �� ������ ����������� ������� ����).


ALTER TABLE ���� 
  ADD CONSTRAINT FK_���������_����
  FOREIGN KEY (�������������) REFERENCES ��������� (���)

GO

ALTER TABLE ������� 
  ADD CONSTRAINT FK_������_�������
  FOREIGN KEY (���������) REFERENCES ������ (���)

GO

ALTER TABLE ������ 
  ADD CONSTRAINT FK_����������_������
  FOREIGN KEY (�������������) REFERENCES ���������� (���)

GO

ALTER TABLE ������ 
  ADD CONSTRAINT FK_����_������
  FOREIGN KEY (�������) REFERENCES ���� (���)

GO

ALTER TABLE ����� 
  ADD CONSTRAINT FK_�������_�����
  FOREIGN KEY (����������) REFERENCES ������� (���)

GO

ALTER TABLE ���� 
  ADD CONSTRAINT FK_�����_����
  FOREIGN KEY (��������) REFERENCES ����� (���)

GO

ALTER TABLE ������ 
  ADD CONSTRAINT FK_�������_������
  FOREIGN KEY (����������) REFERENCES ������� (���)

GO

ALTER TABLE ������������� 
  ADD CONSTRAINT FK_������_�������������
  FOREIGN KEY (���������) REFERENCES ������ (���)

GO

ALTER TABLE ������������� 
  ADD CONSTRAINT FK_�������������_��������������������
  FOREIGN KEY (������������) REFERENCES �������������������� (���)

GO

ALTER TABLE �������������������� 
  ADD CONSTRAINT FK_��������������������_����������
  FOREIGN KEY (�������������) REFERENCES ���������� (���)

GO

ALTER TABLE ������������� 
  ADD CHECK (��������� >= 0)

GO

ALTER TABLE ���������� 
  ADD UNIQUE (�������)

GO

--������������ ������ �8
--������������� SQL-�������� ��� ����������� �������
--� ������ ������ ���� ����������� ����� SQL-�������� ��� ��������� ������ � ���� ������ � INSERT INTO�VALUES, ����������� ������ � �����-���� ����� ������� � UPDATE, �������� ������ �� �����-���� ������� � DELETE. � ���������� ���������� ������ � ������ ������� ������ ���� �������� �� ����� 2-� �������, � ������ �������� ������� ������ ���� �������� ������ �������, ��� � ����� �� � ������������ ������.

INSERT INTO ��������� VALUES ('������', '������')
INSERT INTO ��������� VALUES ('�����', '�������-������')

GO

INSERT INTO ������ VALUES ('�������', 150, '����', '��������')
INSERT INTO ������ VALUES ('������', 0, '����', '��������')

GO

INSERT INTO ���������� VALUES ('������� ����� �����������', '1990-09-30', '��������', '+375297654321')
INSERT INTO ���������� VALUES ('����� ����� ����������', '1992-06-13', '��������', '+375291256789')

GO

INSERT INTO ������� VALUES (1, '�����-������', '���������', '�����-������ (Costa Dorada, Costa Daurada) ����������� ����� ���������. ��� ��������� ������� �� ������� ����� (Cunit) �� ����� ������� � �������� ��������. ������� ����������� ������� � ����-�������� ���������� ����� +25..+28�C, ���� +21..+23�C. ���� ������ ������������ ������� �����-����� � ����� "�� ���", ����� �� ���')
INSERT INTO ������� VALUES (2, '������', '����� ������', '������ � ����� ����������� ������ ������, ������� ������ ����� ���� ������ � ��������� ���������� �����, ������ ������, ���������� � �������� �� ���������� ��������. � 14 ������ ����������� ����������� ������� � �����, � ����������� ����� �������� � ����� ��� ������. ����, � ������� �� ����������� ������ �������� ��������, ���� � �������� �� �������� �������� �������� �� ������, � ����� �������� ��������, ���������� � ���������.')
INSERT INTO ������� VALUES (2, '��������', '������', '�������� � ���� �� ������ �������� ������, �� ��������� ����� ������������ � ������. ���� � ���, ��� ��� �� ��� ����� �������� ���������� ���������� ����� ������� �� �������� � ����������, �������, ��� ��������, ���� � ������������ ������. ������� ����� � ������ ��������� �� �������� ����� ��������� ����������� ������ ����� �������. ��-������, ����� ��������� ������: ���� � ����-������� ��� ������������� ����. ��-������, ��������� ���� � ��������� ������ ���������� � �������� �����, ������������ �� ��������� ���� �������� � ������������, ���� ��� ����� ������� �� ������, ���� ������ ������� � ������� ������ �������� ���, ��� �� ������� ������ ������� � � ��� ����� �����. �� � ���� � ���� ��� �������. �� � �-�������, �� ���������� ����������� ��������� ������ ������������� ������ ������ � � �����-����� �� ������� ������ ����� ������. � ��� ����� ���������� �������� ������, � ������� ����� �� ������� ����������� ���� � ����������� ������.')

GO

INSERT INTO ����� VALUES (1, 4, 'AURORA ORIENTAL BAY MARSA ALAM', 'AI', '����� ������� � 2010 ����. ���������� 64000 ���������� ������. ����������� �������� ���� � 250 � �� �����, ����� �������, ���� �������� ���� � ����.')
INSERT INTO ����� VALUES (2, 3, 'ANANAS HOTEL', 'BB', '���������� ��������� �����. ������� ����������� ���� � ��������.')
INSERT INTO ����� VALUES (3, 2, 'DOUBLETREE BY HILTON', 'FB', '����� ������� �� ������ 8-�������� ������� ������������ �������� ���� � 600 � �� �����.')
INSERT INTO ����� VALUES (3, 3, 'GRAND HOTEL BRISTOL RESORT & SPA', 'OB', '����� ������� � 2010 ����. ���������� 64000 ���������� ������. ����������� �������� ���� � 250 � �� �����, ����� �������, ���� �������� ���� � ����.')

GO

INSERT INTO ���� VALUES (3, '����� �� ����', 1, 2600, '2016-06-14', '2016-09-23')
INSERT INTO ���� VALUES (2, '��� �� ����������� �������', 2, 3250, '2016-07-27', '2016-10-13')
INSERT INTO ���� VALUES (1, '����� �� ����', 2, 3600, '2016-08-03', '2016-09-03')
INSERT INTO ���� VALUES (4, '����� �� ����', 1, 3132, '2016-05-25', '2016-09-30')
INSERT INTO ���� VALUES (1, '��� �� ����������� �������', 2, 2700, '2016-01-13', '2016-09-22')

GO

INSERT INTO ������� VALUES ('���������� ����� ���������', 'RU263521', '����������� 9', '+375446789542')
INSERT INTO ������� VALUES ('��������� ������ ����������', 'HB256001', '�������� 62', '+375296785432')
INSERT INTO ������� VALUES ('��������� ������� ����������', 'HM251487', '�������� 20', '+375293450985')
INSERT INTO ������� VALUES ('�������� ����� ��������', 'HG263154', '������ 10', '+375294578232')

GO

INSERT INTO ������ VALUES (1, 3, '2016-08-07', 5, 2, '2016-08-17', 10, '������')
INSERT INTO ������ VALUES (2, 4, '2016-07-21', 4, 3, '2016-05-11', 7, '�� ��������')
INSERT INTO ������ VALUES (2, 1, '2016-04-13', 2, 1, '2016-09-02', 7, '������')
INSERT INTO ������ VALUES (1, 2, '2016-09-02', 1, 4, '2016-09-25', 14, '�� ��������')
INSERT INTO ������ VALUES (2, 2, '2016-09-11', 3, 4, '2016-10-12', 11, '������')
INSERT INTO ������ VALUES (1, 3, '2016-09-17', 5, 3, '2016-10-13', 3, '�� ��������')

GO

INSERT INTO ���������� VALUES ('Coral Travel', '+26035965214', '����, ��������� 10', 'coraltravel.ua')
INSERT INTO ���������� VALUES ('AnexTour', '+37036215485', '����, ����������� �����, 201', 'anextour.com.ua')
INSERT INTO ���������� VALUES ('OdeonTours', '+37025632145', '������, ��������� 18', 'odeon.ru')
INSERT INTO ���������� VALUES ('Tez-Tour', '+375448962351', '�����, ��������� 16', 'tez-tour.com')

GO


INSERT INTO �������������������� VALUES ('�������', 2)
INSERT INTO �������������������� VALUES ('������ �� ������', 3)
INSERT INTO �������������������� VALUES ('�������� �� ����', 1)
INSERT INTO �������������������� VALUES ('�������� ������', 4)
INSERT INTO �������������������� VALUES ('���� ��������', 1)
INSERT INTO �������������������� VALUES ('����� �� ������', 4)

GO

INSERT INTO ������������� VALUES (1, 2, 200)
INSERT INTO ������������� VALUES (2, 2, 180)
INSERT INTO ������������� VALUES (2, 3, 65)
INSERT INTO ������������� VALUES (3, 5, 84)
INSERT INTO ������������� VALUES (5, 4, 160)
INSERT INTO ������������� VALUES (4, 2, 70)
INSERT INTO ������������� VALUES (5, 1, 140)

GO


DELETE 
FROM ��������������������
WHERE ������������ = '����� �� ������'

GO

UPDATE �������������������� SET ������������� = 2
WHERE ������������ = '���� ��������'

GO